import React from 'react';
import Root from './root';
import { BTProvider } from './services/use-bluetooth-context';
import { OBD2DeviceProvider } from './services/use-obd2-device-context';

export default HelpOnRoad = () => (
    <BTProvider>
        <OBD2DeviceProvider>
            <Root/>
        </OBD2DeviceProvider>
    </BTProvider>
);