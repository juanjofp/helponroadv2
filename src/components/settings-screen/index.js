import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconButtonView from '../widgets/icon-button-view';
import styles from './styles';
import { COLORS } from '../../theme';
import HeaderSection from '../widgets/header-section';
import {useOBD2Device} from '../../services/use-obd2-device-context';

export const SettingsScreen = ({navigation}) => {
    const device = useOBD2Device();
    console.log('SettingsScreen props', navigation, device);
    return (
        <View style={styles.container}>
            <View style={styles.deviceSelectedView}>
                <HeaderSection title='Último dispositivo conectado'/>
                <View style={styles.deviceSelectedInfoView}>
                    <Text style={styles.itemName}>{device.name}</Text>
                    <Text style={styles.itemAddress}>{device.address}</Text>
                </View>
            </View>
            <View style={styles.deviceSelectedView}>
            <HeaderSection title='Términos de Servicio'/>
                <Text>TOS</Text>
            </View>
        </View>
    );
};

SettingsScreen.navigationOptions = {
    title: 'Ajustes'
};

export default SettingsScreen;
