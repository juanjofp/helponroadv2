import React from 'react';
import {
    View,
    Text,
    TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

export const IconButtonView = ({pressColor, color, icon, text, onPress = () => {}}) => {
    return (
        <TouchableHighlight style={styles.iconView} underlayColor={pressColor} onPress={onPress}>
            <React.Fragment>
                <View style={[styles.onoffView, {borderColor: color}]}>
                    <Icon name={icon} size={56} color={color} />
                </View>
                <Text style={[styles.iconText, {color}]}>{text}</Text>
            </React.Fragment>
        </TouchableHighlight>
    );
};

export default IconButtonView;