import {StyleSheet} from 'react-native';
import { COLORS } from '../../../theme';
export default StyleSheet.create({
    iconView: {
        alignItems: 'center',
        padding: 2,
        borderRadius: 12
    },
    iconText: {
        color: COLORS.TEXT,
        fontSize: 14,
        fontWeight: '700'
    },
    onoffView: {
        margin: 16,
        padding: 4,
        borderWidth: 8,
        borderRadius: 39,
        width: 78,
        height: 78,
        alignItems: 'center'
    }
});