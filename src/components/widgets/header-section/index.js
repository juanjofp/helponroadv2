import React from 'react';
import { COLORS } from '../../../theme';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

export const HeaderSection = ({title}) => {
    return (
        <View style={styles.headerView}>
            <Text style={styles.headerText}>{title}</Text>
        </View>
    );
};

export default HeaderSection;

const styles = StyleSheet.create({
    headerView: {
        backgroundColor: COLORS.SCREEN_BACKGROUND,
        margin: 4,
        padding: 4
    },
    headerText: {
        color: COLORS.TEXT,
        fontSize: 16
    }
});