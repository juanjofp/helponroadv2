import React from 'react';
import {
    View,
    Text
} from 'react-native';
import styles from './styles';
import HeaderSection from '../header-section';

export const SensorView = ({title = '', sensors=[]}) => {

    const sensorsData = sensors.map((sensor, idx) => (
        <View style={styles.chartItem} key={idx}>
            <Text style={styles.chartItemType}>{sensor.type}</Text>
            <Text style={styles.chartItemValue}>{sensor.value}</Text>
            <Text style={styles.chartItemName}>{sensor.name}</Text>
        </View>
    ));
    return (
        <View style={styles.charts}>
            <HeaderSection title={title}/>
            <View style={styles.chartView}>
                {sensorsData}
            </View>
        </View>
    );
};

export default SensorView;
