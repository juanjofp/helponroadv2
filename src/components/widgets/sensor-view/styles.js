import {StyleSheet} from 'react-native';
import { COLORS } from '../../../theme';

export default StyleSheet.create({
    charts: {
        backgroundColor: COLORS.DRAWER_BACKGROUND,
        margin: 4
    },
    chartView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        padding: 2
    },
    chartItem: {
        borderColor: COLORS.TEXT,
        backgroundColor: COLORS.TEXT,
        borderWidth: 1,
        borderRadius: 12,
        padding: 8,
        margin: 4,
        width: 90,
        justifyContent: 'center'
    },
    chartItemValue: {
        color: COLORS.ACTIVE,
        fontSize: 26,
        fontWeight: '900',
        textAlign: 'center'
    },
    chartItemName: {
        color: COLORS.TEXT_INVERT,
        fontSize: 12,
        fontWeight: '500',
        textAlign: 'center'
    },
    chartItemType: {
        color: '#CCC',
        fontSize: 10,
        fontWeight: '100',
        textAlign: 'center'
    }
});