import React from 'react';
import {
    View,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

export const IconStatusView = ({color, icon, text}) => {
    return (
        <View style={styles.iconView}>
            <View style={[styles.onoffView, {borderColor: color}]}>
                <Icon name={icon} size={56} color={color} />
            </View>
            <Text style={[styles.iconText, {color}]}>{text}</Text>
        </View>
    );
};

export default IconStatusView;