import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
} from 'react-native';
import LOGO from '../images/logo.png';
import LOGONAME from '../images/logo-name.png';
import LOGOS from '../images/logos.png';
import { DrawerItems } from 'react-navigation-drawer';
import styles from './styles';
import { Switch } from 'react-native-gesture-handler';
import { COLORS } from '../../theme';

let ecallChecked = true;

const CustomDrawerContentComponent = (props) => {
    const [ecall, setEcall] = useState(true);
    return (
        <View style={styles.container}>
            <View
                style={styles.user}>
                <Image
                    style={ styles.userViewAvatar }
                    source={LOGO}
                />
                <Image
                    source={LOGONAME}
                    resizeMode='contain'
                    style={styles.userViewName}/>
            </View>
            <DrawerItems {...props} />
            <View
                style={styles.menu}>
                <TouchableOpacity
                    onPress={
                        ()=>{
                            //console.log('Logos de I+D');
                        }
                    }>
                    <View style={styles.ecallView}>
                        <Text style={styles.ecallText}>E-Call {ecall ? 'activado' : 'desactivado'}</Text>
                        <Switch
                            style={styles.ecallSwitch}
                            value={ecall}
                            thumbColor={COLORS.ACTIVE}
                            trackColor={{
                                false: 'red',
                                true: 'green'
                            }}
                            onValueChange={value => {
                                //console.log('Switch', ecall, value);
                                setEcall(value);
                                }}/>
                    </View>
                </TouchableOpacity>    
                <TouchableOpacity
                    onPress={
                        ()=>{
                            //console.log('Logos de I+D');
                        }
                    }>
                    <Image
                        source={LOGOS}
                        resizeMode='contain'
                        style={styles.userViewName}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};
export default CustomDrawerContentComponent;