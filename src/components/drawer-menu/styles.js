import { StyleSheet } from 'react-native';
import {
    COLORS
} from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    user: {
        //paddingLeft: 16,
        borderBottomColor: COLORS.ACTIVE,
        borderBottomWidth: 2,
        alignItems: 'center'
    },
    userViewAvatar: {
        height: 64,
        width: 64,
        borderRadius: 36,
        margin: 6
    },
    userViewName: {
        width: 150,
        height: 50
    },
    menu: {
        padding: 16,
        borderTopColor: COLORS.ACTIVE,
        borderTopWidth: 2
    },
    ecallView: {
        flexDirection: 'row',
        padding: 8
    },
    ecallSwitch: {
        flex: 1,
        alignItems: 'flex-end'
    },
    ecallText: {
        color: COLORS.TEXT,
        fontSize: 16
    }
});
