import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconButtonView from '../widgets/icon-button-view';
import styles from './styles';
import { COLORS } from '../../theme';
import HeaderSection from '../widgets/header-section';
import { FlatList } from 'react-native-gesture-handler';

const fakeNotification = [
    {name: 'Revisar presión neumaticos', severity: 0, when: 'Antes de 50 Km', id: 1},
    {name: 'Revisar filtro de polen', severity: 1, when: 'Antes de 150 Km', id: 2},
    {name: 'Cambiar pastillas de freno', severity: 2, when: 'Inmediatamente', id: 3},
    {name: 'Cambiar limpiaparabrisas', severity: 0, when: 'Inmediatamente', id: 4}
];

const severityIcons = ['ios-information-circle', 'ios-warning', 'ios-alert'];
const severityColor = ['blue', 'orange', 'red'];

const renderNotificationCallback = ({item, index}) => {
    const icon = severityIcons[item.severity];
    const color = severityColor[item.severity];
    return (
        <View
            style={styles.notifyItemView}>
            <View style={styles.itemInfo}>
                <Text style={styles.itemName}>{item.name}</Text>
                <Text style={styles.itemWhen}>{item.when}</Text>
            </View>
            <Icon name={icon} size={36} color={color} />
        </View>
    );
};

export const MaintenanceScreen = ({navigation}) => {
    //console.log('MaintenanceScreen props', navigation);
    return (
        <View style={styles.container}>
            <View style={styles.sectionsView}>
                <View style={styles.iconButtonsView}>
                    <IconButtonView
                        onPress={()=> navigation.navigate('Garage')}
                        pressColor={COLORS.ACTIVE}
                        color={COLORS.TEXT}
                        text={'Talleres'}
                        icon={'ios-build'}/>
                </View>
                <View style={styles.iconButtonsView}>
                    <IconButtonView
                        onPress={()=> navigation.navigate('Garage')}
                        pressColor={COLORS.ACTIVE}
                        color={COLORS.TEXT}
                        text={'Reparaciones'}
                        icon={'ios-construct'}/>
                </View>
                <View style={styles.iconButtonsView}>
                    <IconButtonView
                        onPress={()=> navigation.navigate('Garage')}
                        pressColor={COLORS.ACTIVE}
                        color={COLORS.TEXT}
                        text={'Alertas'}
                        icon={'ios-notifications-outline'}/>
                </View>
            </View>
            <HeaderSection title='Avisos de mantenimiento'/>
            <FlatList
                data={fakeNotification}
                renderItem={renderNotificationCallback}
                keyExtractor={(item) => item.id + ''}
            />
        </View>
    );
};

MaintenanceScreen.navigationOptions = {
    title: 'Mantenimiento'
};

export default MaintenanceScreen;
