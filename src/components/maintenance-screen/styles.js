import {StyleSheet} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    sectionsView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 8
    },
    iconButtonsView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: COLORS.SCREEN_BACKGROUND,
        margin: 4,
        borderRadius: 12,
        paddingBottom: 4
    },
    notifyItemView: {
        flexDirection: 'row',
        margin: 8,
        padding: 10,
        alignItems: 'center',
        borderRadius: 12,
        borderWidth: 1,
        borderColor: COLORS.ACTIVE
    },
    itemInfo: {
        flex: 1,
    },
    itemName: {
        color: COLORS.TEXT_INVERT,
        fontSize: 18,
        fontWeight: '700'
    },
    itemWhen: {
        color: COLORS.TEXT_SECONDARY,
        fontSize: 14,
        fontWeight: '700'
    }
});