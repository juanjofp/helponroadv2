import React from 'react';
import {
    View,
    Text,
    FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { COLORS } from '../../theme';

const fakeGarages = [
    {
        id: 1,
        name: 'Talleres PITSTOP Murcia',
        address: 'Av. Ciclista Mariano Rojas, 15',
        phone: '968 07 04 16',
        location: {latitude: 38.028717, longitude: -1.168496}
    },
    {
        id: 2,
        name: 'Talleres Ataz e Hijos S.L.',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.029308, longitude: -1.164683}
    },
    {
        id: 3,
        name: 'Talleres El Automóvil de Murcia S. L.',
        address: 'Av. Ciclista Mariano Rojas, 15',
        phone: '968 07 04 16',
        location: {latitude: 38.031750, longitude: -1.164061}
    },
    {
        id: 4,
        name: 'Feu Vert',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.023763, longitude: -1.166385}
    },
    {
        id: 5,
        name: 'Talleres Paco El Cherro',
        address: 'Av. Ciclista Mariano Rojas, 15',
        phone: '968 07 04 16',
        location: {latitude: 38.024650, longitude: -1.163949}
    },
    {
        id: 6,
        name: 'Euromaster',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.020928, longitude: -1.163648}
    },
    {
        id: 7,
        name: 'Talleres Midas',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.022565, longitude: -1.171569}
    }
];

const renderGarageCallback = ({item, index}) => {
    const icon = 'ios-construct';
    const color = COLORS.ACTIVE;
    return (
        <View
            style={styles.garageItemView}>
            <Icon name={icon} size={36} color={color} />
            <View style={styles.itemInfo}>
                <Text style={styles.itemName}>{item.name}</Text>
                <Text style={styles.itemAddress}>{item.address}</Text>
            </View>
        </View>
    );
};

export const GarageListScreen = (props) => {
    //console.log('GarageListScreen props', props);
    return (
        <View style={styles.container}>
            <FlatList
                data={fakeGarages}
                renderItem={renderGarageCallback}
                keyExtractor={(item) => item.id + ''}/>
        </View>
    );
};

export default GarageListScreen;
