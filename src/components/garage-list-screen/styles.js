import {StyleSheet} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    garageItemView: {
        flexDirection: 'row',
        margin: 8,
        padding: 10,
        alignItems: 'center',
        borderRadius: 12,
        borderWidth: 1,
        borderColor: COLORS.ACTIVE
    },
    itemInfo: {
        flex: 1,
        marginLeft: 12
    },
    itemName: {
        color: COLORS.TEXT_INVERT,
        fontSize: 18,
        fontWeight: '700'
    },
    itemAddress: {
        color: COLORS.TEXT_SECONDARY,
        fontSize: 14,
        fontWeight: '700'
    }
});