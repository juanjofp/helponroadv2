import React, {useState} from 'react';
import {
    View,
    Picker,
    Button
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { COLORS } from '../../theme';
import HeaderSection from '../widgets/header-section';
import { TextInput } from 'react-native-gesture-handler';
import LoadingView from '../loading-view';

export const FormDiagnosisScreen = ({navigation}) => {
    //console.log('FormDiagnosisScreen props', navigation, navigation.isFirstRouteInParent());
    const [commChannel, setCommChanel] = useState('whatsapp');
    const [isSending, setSending] = useState(false);
    return (
        <View style={styles.container}>
            <HeaderSection title='Canal de comunicación'/>
            <Picker
                selectedValue={commChannel}
                style={{height: 50, width: 300}}
                onValueChange={(itemValue, itemIndex) => setCommChanel(itemValue)}
            >
                <Picker.Item label='WhatsApp' value='whatsapp' />
                <Picker.Item label='Teléfono' value='phone' />
                <Picker.Item label='E-mail' value='email' />
            </Picker>
            <HeaderSection title='Comentarios'/>
            <View style={styles.commentsView}>
                <Icon name='ios-chatbubbles' color={COLORS.ACTIVE} size={36}/>
                <TextInput
                    placeholder={'Escribe los comentarios que creas oportunos'}
                    style={styles.commentsText}
                    autoCapitalize='none'
                    autoCorrect={false}
                    multiline={true}
                    numberOfLines={6}
                    onChangeText={(txt) => console.log('Comentarios', txt)}
                />
            </View>
            <View style={styles.buttonsView}>
                {
                    !navigation.isFirstRouteInParent() &&
                    <View style={styles.buttonView}>
                        <Button
                            title={'Cancelar'}
                            color={COLORS.ACTIVE}
                            disabled={isSending}
                            onPress={() => navigation.goBack()}/>
                    </View>
                }
                <View style={styles.buttonView}>
                    <Button
                        disabled={isSending}
                        title={'Enviar'}
                        color={COLORS.ACTIVE}
                        onPress={() => {
                            // Fake sent to server....
                            setSending(true);
                            setTimeout(
                                () => {
                                    setSending(false);
                                    navigation.goBack();
                                },
                                3000
                            );
                        }}/>
                </View>
            </View>
            <LoadingView show ={isSending}/>
        </View>
    );
};

export default FormDiagnosisScreen;
