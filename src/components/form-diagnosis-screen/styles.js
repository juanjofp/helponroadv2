import {StyleSheet} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    commentsView: {
        flexDirection: 'row',
        margin: 4,
        padding: 4
    },
    commentsText: {
        borderLeftColor: COLORS.ACTIVE,
        borderLeftWidth: 2,
        height: 180,
        marginLeft: 8,
        textAlignVertical: 'top',
        paddingLeft: 6
    },
    buttonsView: {
        flexDirection: 'row',
        margin: 16,
        justifyContent: 'space-around'
    },
    buttonView: {
        flex: 1,
        margin: 12
    },
    buttonText: {
        fontWeight: '700',
        fontSize: 18,
        color: 'white',
        flex: 1,
        textAlign: 'center'
    }
});