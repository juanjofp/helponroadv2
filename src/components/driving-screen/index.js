import React, {useState} from 'react';
import {Text, View, ScrollView} from 'react-native';
import ReactSpeedometer from 'react-native-speedometer';
import HeaderSection from '../widgets/header-section';
import styles from './styles';
import { COLORS } from '../../theme';
import SensorView from '../widgets/sensor-view';
import { useOBD2Device } from '../../services/use-obd2-device-context';
import { useDrivingData } from '../../services/use-driving-data';
import { NavigationEvents } from 'react-navigation';

const labelsRPM = [
    {
        name: 1,
        labelColor: 'rgba(0,0,0,0)',
        activeBarColor: 'green'
    },
    {
        name: 2,
        labelColor: 'rgba(0,0,0,0)',
        activeBarColor: 'green'
    },
    {
        name: 3,
        labelColor: 'rgba(0,0,0,0)',
        activeBarColor: 'orange'
    },
    {
        name: 4,
        labelColor: 'rgba(0,0,0,0)',
        activeBarColor: 'red'
    }
];

const labelsSpeed = [
    {
        name: 1,
        labelColor: 'rgba(0,0,0,0)',
        activeBarColor: 'green'
    },
    {
        name: 2,
        labelColor: 'rgba(0,0,0,0)',
        activeBarColor: 'green'
    },
    {
        name: 3,
        labelColor: 'rgba(0,0,0,0)',
        activeBarColor: 'red'
    }
];

const proccessValue = (key, data) => {
    const value = data[key];
    if(value && value.cmdResult && !value.cmdResult.includes('NODATA')) {
        return value.cmdResult;
    }
    return '--';
};

const proccessIntValue = (value) => {
    if(value && value !== '--') {
        return parseInt(value);
    }
    return 0;
};

const NoDeviceConnected = ({device}) => {
    //console.log('No Device', device)
    if(device) return null;
    return (
        <View style={styles.charts}>
            <HeaderSection title='Mensajes'/>
            <Text  style={styles.message}>Para obtener datos necesitas conectarte a un dispositivo OBD2</Text>
        </View>
    );
};

export const DrivingScreen = (props) => {
    const device = useOBD2Device();
    const [isVisible, setVisible] = useState(false);
    const data = useDrivingData(device, isVisible);

    
    const velocity = proccessIntValue(proccessValue('010D', data));
    const rpm = proccessIntValue(proccessValue('010C', data));
    //console.log('DrivingScreen props', props, device, velocity, rpm, data);
    return (
        <ScrollView style={styles.container}>
            <NavigationEvents
                onDidFocus={payload => {
                    //console.log('OBD2 subs DrivingScreen did focus',payload);
                    setVisible(true);
                }}
                onDidBlur={payload => {
                    //console.log('OBD2 subs DrivingScreen did blur',payload);
                    setVisible(false);
                }}
            />
            <NoDeviceConnected device={device}/>
            <View style={styles.charts}>
                <HeaderSection title='Velocímetro'/>
                <View style={styles.chart}>
                    <ReactSpeedometer
                        minValue={0}
                        maxValue={200}
                        value={velocity}
                        size={300}
                        easeDuration={0}
                        labels={labelsSpeed}
                        labelStyle={styles.hidelabel}
                    />
                    <Text style={styles.label}>{velocity} Kms/h</Text>
                </View>
            </View>
            <View style={styles.charts}>
            <HeaderSection title='Cuenta revoluciones'/>
                <View style={styles.chart}>
                    <ReactSpeedometer
                        minValue={0}
                        maxValue={6000}
                        value={rpm}
                        size={300}
                        easeDuration={0}
                        labels={labelsRPM}
                        labelStyle={styles.hidelabel}
                    />
                    <Text style={styles.label}>{rpm} RPM</Text>
                </View>
            </View>
            <SensorView
                title={'VIN: ' + proccessValue('0902', data)}
                sensors={[
                    {name: 'Aceite', type: 'Temperatura', value: proccessValue('0146', data)},
                    {name: 'Motor', type: 'Temperatura', value: proccessValue('0105', data)},
                    {name: 'Motor', type: 'Carga', value: proccessValue('0104', data)},
                    {name: 'Combus.', type: 'Litros', value: proccessValue('012F', data)}
                ]}
                />
        </ScrollView>
    );
};

DrivingScreen.navigationOptions = {
    title: 'Panel de condución'
};

export default DrivingScreen;
