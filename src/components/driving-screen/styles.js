import {StyleSheet} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    charts: {
        backgroundColor: COLORS.DRAWER_BACKGROUND,
        margin: 4
    },
    chart: {
        alignItems: 'center'
    },
    label: {
        textAlign: 'center',
        fontSize: 24,
        fontWeight: '700',
        color: COLORS.TEXT
    },
    hidelabel: {
        color: COLORS.DRAWER_BACKGROUND
    },
    chartView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingBottom: 8
    },
    chartItem: {
        borderColor: COLORS.TEXT,
        backgroundColor: COLORS.TEXT,
        borderWidth: 1,
        borderRadius: 12,
        padding: 8,
        width: 80,
        justifyContent: 'center'
    },
    chartItemValue: {
        color: COLORS.ACTIVE,
        fontSize: 26,
        fontWeight: '900',
        textAlign: 'center'
    },
    chartItemName: {
        color: COLORS.TEXT_INVERT,
        fontSize: 14,
        fontWeight: '500',
        textAlign: 'center'
    },
    chartItemType: {
        color: '#CCC',
        fontSize: 10,
        fontWeight: '100',
        textAlign: 'center'
    },
    message: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '700',
        padding: 12,
        color: COLORS.TEXT
    },
});