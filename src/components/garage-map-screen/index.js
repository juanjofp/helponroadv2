import React from 'react';
import {View} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import styles from './styles';
import { COLORS } from '../../theme';

const fakeGarages = [
    {
        id: 1,
        name: 'Talleres PITSTOP Murcia',
        address: 'Av. Ciclista Mariano Rojas, 15',
        phone: '968 07 04 16',
        location: {latitude: 38.028717, longitude: -1.168496}
    },
    {
        id: 2,
        name: 'Talleres Ataz e Hijos S.L.',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.029308, longitude: -1.164683}
    },
    {
        id: 3,
        name: 'Talleres El Automóvil de Murcia S. L.',
        address: 'Av. Ciclista Mariano Rojas, 15',
        phone: '968 07 04 16',
        location: {latitude: 38.031750, longitude: -1.164061}
    },
    {
        id: 4,
        name: 'Feu Vert',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.023763, longitude: -1.166385}
    },
    {
        id: 5,
        name: 'Talleres Paco El Cherro',
        address: 'Av. Ciclista Mariano Rojas, 15',
        phone: '968 07 04 16',
        location: {latitude: 38.024650, longitude: -1.163949}
    },
    {
        id: 6,
        name: 'Euromaster',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.020928, longitude: -1.163648}
    },
    {
        id: 7,
        name: 'Talleres Midas',
        address: 'Calle Cisne, 13, 30009 Murcia',
        phone: ' 968 29 14 52',
        location: {latitude: 38.022565, longitude: -1.171569}
    }
];

export const GarageMapScreen = (props) => {
    //console.log('GarageMapScreen props', props);
    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                    latitude: 38.022565,
                    longitude: -1.165920,
                    latitudeDelta: 0.03,
                    longitudeDelta: 0.028,
                }}
                >
                {
                    fakeGarages.map(
                        (garage) => (
                            <Marker
                                key={garage.id}
                                title={garage.name}
                                description={garage.address}
                                pinColor='gold'
                                coordinate={garage.location}
                            />
                        )
                    )
                }
            </MapView>
        </View>
    );
};

export default GarageMapScreen;
