import React from 'react';
import {
    View,
    ActivityIndicator,
    StyleSheet
} from 'react-native';
import { COLORS } from '../theme';

export const LoadingView = ({show}) => {
    if(!show) return null;
    return (
        <View style={styles.loadingView}>
            <ActivityIndicator color={COLORS.ACTIVE} size='large'/>
        </View>
    );
};

const styles = StyleSheet.create({
    loadingView: {
        justifyContent: 'center',
    }
});

export default LoadingView;