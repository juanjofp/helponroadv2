import React, {useState} from 'react';
import {Text, View, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { COLORS } from '../../theme';
import styles from './styles';
import { NavigationEvents } from 'react-navigation';
import { useDTCData } from '../../services/use-dtc-data';
import { useOBD2Device } from '../../services/use-obd2-device-context';
import LoadingView from '../loading-view';
import HeaderSection from '../widgets/header-section/index';


export const ActionView = ({show, title, action}) => {
    if(!show) return false;
    return (
        <View style={styles.actionButtonView}>
            <Icon.Button
                name={'ios-pulse'}
                backgroundColor={COLORS.ACTIVE}
                onPress={action}>
                <Text style={styles.actionButtonText}>{title}</Text>
            </Icon.Button>
        </View>
    );
};

const messages = [
`Puede realizar un diagnostico del estado del motor de su coche para comprobar posibles averias.
Tras la comprobación le mostraremos la información recopilada.`,
`¡Felicidades!, No se ha detectado ninguna avería en su motor.
¡Puede continuar la conducción con tranquilidad!.`,
`Se han detectado avisos de posibles fallos en su motor, le sugerimos realizar diagnostico más completo del estado su motor.
Durante el diagnostico se le pedirá que mantenga las revoluciones del motor dentro de un rango concreto.
Una vez finalizada la diagnosis se enviará a nuestros profesionales para que la evaluen y puedan asesorarle de forma adecuada.`,
`Estamos realizando el chequeo de su motor, esto nos llevará unos pocos segundos.
¡Gracias por su paciencia!.`
];
const showMessage = (isChecking, finished, errors) => {
    if(isChecking) return messages[3];
    if(finished) {
        if(errors) return messages[2];
        return messages[1];
    }
    return messages[0];
}
const severityIcons = ['', 'ios-information-circle', 'ios-warning', 'ios-alert'];
const severityColor = ['', COLORS.DEVICE_ON, COLORS.DEVICE_CONNECTING, '#FF7F7F'];

const renderNotificationCallback = (item, index) => {
    const icon = severityIcons[item.severity];
    const color = severityColor[item.severity];
    return (
        <View
            key={item.code}
            style={styles.notifyItemView}>
            <View style={styles.itemInfo}>
                <Text style={styles.itemName}>{item.description}</Text>
                <Text style={styles.itemWhen}>{item.code}</Text>
            </View>
            <Icon name={icon} size={36} color={color} />
        </View>
    );
};
export const DTCS = ({data}) => {
    if(!data || !Array.isArray(data) || data.length === 0) {
        return null;
    }

    return (
        <View>
            <HeaderSection title='Fallos detectados'/>
            {
                data.map(renderNotificationCallback)
            }
        </View>
    );
};

export const DiagnosisScreen = ({navigation}) => {
    const device = useOBD2Device();
    const [isChecking, setChecking] = useState(false);
    const {data, reset} = useDTCData(device, () => setChecking(false), isChecking);
    console.log('DiagnosisScreen', data.updates);
    return (
        <ScrollView style={styles.container}>
            <NavigationEvents
                onDidFocus={payload => {
                    //console.log('OBD2 subs DiagnosisScreen did focus',payload);
                    //setChecking(true);
                }}
                onDidBlur={payload => {
                    console.log('OBD2 subs DiagnosisScreen did blur',payload);
                    setChecking(false);
                    reset();
                }}
            />
            <View style={styles.bannerView}>
                <Icon name={'ios-car'} size={116} color={COLORS.ACTIVE} />
                <Text style={styles.bannerTitle}>
                    Sano como una Manzana Golden!
                </Text>
                <Text style={styles.bannerDetails}>
                {showMessage(isChecking, data.completed, data.hasErrors)}
                </Text>
            </View>
            <ActionView
                show={!isChecking && !data.hasErrors}
                title='Comenzar Diagnóstico'
                action={() => setChecking(true)}
            />
            <ActionView
                show={!isChecking && data.completed && data.hasErrors}
                title='Diagnóstico Avanzado'
                action={() => {
                    navigation.navigate('AdvanceIssue');
                    reset();
                }}
            />
            <LoadingView show={isChecking}/>
            <DTCS data={data.dtcs}/>
        </ScrollView>
    );
};

DiagnosisScreen.navigationOptions = {
    title: 'Diagnóstico'
};

export default DiagnosisScreen;
