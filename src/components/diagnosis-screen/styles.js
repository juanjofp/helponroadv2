import {StyleSheet} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    bannerView: {
        margin: 4,
        backgroundColor: COLORS.DRAWER_BACKGROUND,
        padding: 8,
        alignItems: 'center'
    },
    bannerTitle: {
        fontWeight: '700',
        fontSize: 28,
        color: COLORS.TEXT,
        textAlign: 'center'
    },
    bannerDetails: {
        fontWeight: '200',
        fontSize: 16,
        color: COLORS.TEXT,
        padding: 16
    },
    actionButtonView: {
        margin: 4,
        padding: 4
    },
    actionButtonText: {
        fontWeight: '700',
        fontSize: 18,
        color: 'white',
        flex: 1,
        textAlign: 'center'
    },
    notifyItemView: {
        flexDirection: 'row',
        margin: 8,
        padding: 10,
        alignItems: 'center',
        borderRadius: 12,
        borderWidth: 1,
        borderColor: COLORS.ACTIVE
    },
    itemInfo: {
        flex: 1,
    },
    itemName: {
        color: COLORS.TEXT_INVERT,
        fontSize: 18,
        fontWeight: '700'
    },
    itemWhen: {
        color: COLORS.TEXT_SECONDARY,
        fontSize: 14,
        fontWeight: '700'
    }
});