import React, {useEffect} from 'react';
import {
  ActivityIndicator,
  StatusBar,
  View,
  Image,
  Text
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LOGO from '../images/logo.png';
import LOGONAME from '../images/logo-name.png';
import styles from './styles';
import { COLORS } from '../../theme';

const LogoView = (props) => {
    return(
        <View style={styles.userView}>
            <Image
                source={LOGO}
                resizeMode='contain'
                style={styles.userViewAvatar}/>
            <Image
                source={LOGONAME}
                resizeMode='contain'
                style={styles.userViewName}/>
        </View>
    );
};

const AuthLoadingScreen = ({navigation}) => {
    useEffect(() => {
        setTimeout(
            async () => {
                const userToken = await AsyncStorage.getItem('userToken');
                navigation.navigate(userToken ? 'App' : 'Auth');
            },
            2000
        );
    }, []);

    // Render any loading content that you like here
    return (
    <View style={styles.container}>
        <LogoView/>
        <View style={styles.loaderView}>
            <ActivityIndicator color={COLORS.ACTIVE} size='large'/>
        </View>
        <StatusBar barStyle="default" />
    </View>
    );
}

export default AuthLoadingScreen;