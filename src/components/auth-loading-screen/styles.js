import {
    StyleSheet
} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BACKGROUND,
        color: COLORS.TEXT,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.BACKGROUND
    },
    userView: {
        padding: 8,
        flexDirection: 'column',
        alignItems: 'center'
    },
    userViewAvatar: {
        height: 100,
        width: 100
    },
    userViewName: {
        margin: 6,
        width: 300
    },
    loaderView: {
    }
});