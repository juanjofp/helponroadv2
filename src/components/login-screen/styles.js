import {
    StyleSheet,
    Dimensions
} from 'react-native';
import { COLORS } from '../../theme';

let winSize = Dimensions.get('window');
//console.log('Size', winSize);


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BACKGROUND,
        color: COLORS.TEXT
    },
    userView: {
        marginTop: 4,
        padding: 8,
        flexDirection: 'column',
        alignItems: 'center',
    },
    userViewAvatar: {
        height: 100,
        width: 100,
        borderRadius: 50
    },
    userViewName: {
        margin: 6,
        width: 300
    },
    inputView: {
        margin: 8,
        padding: 10,
        backgroundColor: '#CDCDCD'
    },
    inputUsernameView: {
        height: 80
    },
    errorView: {
        alignContent: 'center',
        justifyContent: 'center',
        zIndex: 99,
        backgroundColor: '#f95a25',
        padding: 8,
    },
    errorViewMessage: {
        color: 'white',
        fontSize: 18,
        alignSelf: 'center'
    },
    loginLocalButtonView: {
        marginTop: 8,
    },
    loadingView: {
        justifyContent: 'center',
    },
    buttons: {
        marginLeft: 26,
        marginRight: 26,
        marginTop: 12,
        marginBottom: 12
    },
    buttonText: {
        flex: 1,
        fontWeight: '700',
        fontSize: 18,
        color: 'white',
        flex: 1,
        textAlign: 'center'
    }
}); 