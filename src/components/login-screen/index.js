import React, {useState, useCallback, useReducer, useEffect} from 'react';
import {
    View,
    Text,
    Image,
    ActivityIndicator,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Fumi } from 'react-native-textinput-effects';
import AsyncStorage from '@react-native-community/async-storage';
import LOGO from '../images/logo.png';
import LOGONAME from '../images/logo-name.png';
import styles from './styles';
import { COLORS } from '../../theme';

const LogoView = (props) => {
    return(
        <View style={styles.userView}>
            <Image
                source={LOGO}
                style={styles.userViewAvatar}/>
            <Image
                source={LOGONAME}
                resizeMode='contain'
                style={styles.userViewName}/>
        </View>
    );
};

const ErrorView = ({hasError, message, timeout, action}) => {
    useEffect(
        () => {
            let timeId;
            if(hasError) {
                timeId = setTimeout(action, timeout);
            }
            return () => {
                clearTimeout(timeId);
            };
        },
        [hasError, timeout, action]
    );
    if(!hasError) {
        return null;
    }

    return (
        <View style={styles.errorView}>
            <Text style={styles.errorViewMessage}>{message}</Text>
        </View>
    );
};

const InputView = ({
    hasError,
    errorText,
    clearError,
    isLoading,
    show,
    login
}) => {
    const [user, setUser] = useState('');
    const [passwd, setPasswd] = useState('');

    const runLogin = useCallback(
      () => {
        login(user, passwd);
      },
      [user, passwd, login],
    )

    if(!show) return null;
    return (
        <React.Fragment>
            <View
                style={styles.inputView}>
                <View
                    style={styles.inputUsernameView}>
                    <Fumi
                        label={'Usuario'}
                        iconClass={Icon}
                        iconName={'user'}
                        iconColor={COLORS.ACTIVE}
                        iconSize={20}
                        style={{height: 60}}
                        autoCapitalize='none'
                        autoCorrect={false}
                        onChangeText={setUser}
                    />
                </View>
                <View
                    style={styles.inputUsernameView}>
                    <Fumi
                        label={'Contraseña'}
                        iconClass={Icon}
                        iconName={'key'}
                        iconColor={COLORS.ACTIVE}
                        iconSize={20}
                        style={{height: 60}}
                        secureTextEntry={true}
                        autoCapitalize='none'
                        autoCorrect={false}
                        onChangeText={setPasswd}
                    />
                </View>
                <View
                    style={styles.loginLocalButtonView}>
                    {
                        hasError ?
                            <ErrorView
                                hasError={hasError}
                                message={errorText}
                                timeout={2500}
                                action={clearError}/>
                        :
                            (
                                <Icon.Button
                                    name={'user'}
                                    backgroundColor={COLORS.ACTIVE}
                                    disabled={isLoading || hasError}
                                    onPress={runLogin}>
                                    <Text style={styles.buttonText}>ACCEDER</Text>
                                </Icon.Button>
                            )
                    }
                </View>
            </View>
        </React.Fragment>
    );
};

export const LoadingView = ({show}) => {
    if(!show) return null;
    return (
        <View style={styles.loadingView}>
            <ActivityIndicator color={COLORS.ACTIVE} size='large'/>
        </View>
    );
};

const initialState = {
    isLoading: false,
    hasErrors: false,
    errorText: null,
    isLogged: false
};
const CASE_LOADING = 'LOADING';
const CASE_LOGGED = 'LOGGED';
const CASE_ERROR = 'ERROR';
const CASE_CLEAR_ERROR = 'CLEAR_ERROR';
const reducer = (state, action) => {
    switch(action) {
        case CASE_LOADING:
            return {
                ...state,
                isLoading: true,
                isLogged: false,
            };
        case CASE_LOGGED:
            return {
                ...state,
                isLoading: false,
                isLogged: true
            };
        case CASE_ERROR:
            return {
                isLogged: false,
                isLoading: false,
                hasErrors: true,
                errorText: 'Error de authenticación!'
            };
        case CASE_CLEAR_ERROR:
            return initialState;
    }
    return state;
};

export const LoginScreen = ({navigation}) => {
    const [state, dispatch] = useReducer(reducer, initialState)
    const login = useCallback(
        (user, passwd) => {
            dispatch(CASE_LOADING);
            const ACTION =  (
                !user || !passwd || !user.includes('answare') || passwd.length < 6
            ) ? CASE_ERROR : CASE_LOGGED;
            setTimeout(
                () => {
                    dispatch(ACTION);
                },
                1000
            );
        },
        []
    );

    useEffect(
        () => {
            AsyncStorage.setItem('userToken', 'abc');
            state.isLogged && navigation.navigate('App');
        },
        [state.isLogged]
    );

    return (
        <ScrollView
            style={styles.container}>
            <View
                style={styles.userView}>
                <LogoView/>
            </View>
            <InputView
                show={!state.isLogged}
                login={login}
                hasError={state.hasErrors}
                errorText={state.errorText}
                clearError={() => dispatch(CASE_CLEAR_ERROR)}
                isLoading={state.isLoading}/>
            <LoadingView show={state.isLoading}/>
        </ScrollView>
    );
};

LoginScreen.navigationOptions = {
    header: null
};

export default LoginScreen;
