import React, {useState} from 'react';
import {Text, View, ProgressBarAndroid} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ReactSpeedometer from 'react-native-speedometer';
import { COLORS } from '../../theme';
import styles from './styles';
import HeaderSection from '../widgets/header-section';
import { NavigationEvents } from 'react-navigation';
import { useDiagnosisData } from '../../services/use-diagnosis-data';
import { useOBD2Device } from '../../services/use-obd2-device-context';

const MESSAGES_DIAGNOSIS = [
    'Debes detener el coche para poder realizar el diagnostico',
    'Usa el acelerador para mantener las revoluciones dentro del rango indicado!',
    'ACELERA para volver al rango de RPMs indicado!',
    'DECELERA para volver al rango de RPMs indicado! ',
    'MANTEN el acelerador!',
    'Chequeo terminado correctamente!'
];
const MESSAGES_COLOR = [
    COLORS.DEVICE_CONNECTING,
    COLORS.DEVICE_CONNECTING,
    '#FF7F7F',
    '#FF7F7F',
    COLORS.DEVICE_ON,
    COLORS.DEVICE_ON
];

const proccessValue = (key, data) => {
    const value = data[key];
    if (value && value.cmdResult && !value.cmdResult.includes('NODATA')) {
        return value.cmdResult;
    }
    return '--';
};

const proccessIntValue = (value) => {
    if (value && value !== '--') {
        return parseInt(value, 10);
    }
    return 0;
};

const DiagnosisProccess = ({velocity = 10, rpms = 0, rpmsRange, completed = 0, finished = false, issue = 0, totalIssues = 1}) => {
    if (!Array.isArray(rpmsRange) || rpmsRange.length !== 2) return null;
    // Calculate values for chart
    const vMax = rpmsRange[1] + (rpmsRange[1] - rpmsRange[0]);
    const vMin = rpmsRange[0] - (rpmsRange[1] - rpmsRange[0]);
    const avoidNulls = !rpms ? vMin : rpms;
    const avoidNegatives = (avoidNulls < vMin) ? vMin : avoidNulls;
    const avoidOverflow = (avoidNegatives > vMax) ? vMax : avoidNegatives;

    const statusCode = finished ? 5 :
                        (velocity > 0) ? 0 :
                            (!rpms) ? 1 :
                                (rpms < rpmsRange[0]) ? 2 :
                                    (rpms > rpmsRange[1]) ? 3 : 4;
    const message = MESSAGES_DIAGNOSIS[statusCode];
    const messageColor = {color: MESSAGES_COLOR[statusCode]};

    const progressIssues = [];
    for (let section = 0; section < totalIssues; section++) {
        progressIssues.push(
            <View 
                style={{flex: 1, backgroundColor: section === issue ? COLORS.ACTIVE : section < issue ? COLORS.DEVICE_ON : COLORS.BACKGROUND}}
                key={section + '_'}
                >
                <Text style={{textAlign: 'center', color: 'white'}}>{section + 1}</Text>
            </View>
        );
    }
    //console.log('Diagnosis', rpms, rpmsRange, vMax, vMin, avoidOverflow, statusCode, issue, totalIssues, progressIssues);
    return (
        <View style={styles.diagnosisBoardView}>
            <View style={styles.chart}>
                <HeaderSection title='Cuenta revoluciones'/>
                <View style={styles.rangeView}>
                    <Text style={styles.rangeMinText}>{rpmsRange[0]}</Text>
                    <Text style={styles.rpmText}>{rpms}</Text>
                    <Text style={styles.rangeMaxText}>{rpmsRange[1]}</Text>
                </View>
                <ReactSpeedometer
                    minValue={vMin}
                    maxValue={vMax}
                    value={avoidOverflow}
                    size={300}
                    easeDuration={0}
                    labelStyle={styles.hidelabel}
                    labels={[
                        {
                            name: 'ACELERA!',
                            labelColor: 'rgba(0,0,0,0)',
                            activeBarColor: '#FF7F7F',
                        },
                        {
                            name: 'MANTEN LA VELOCIDAD',
                            labelColor: 'rgba(0,0,0,0)',
                            activeBarColor: COLORS.DEVICE_ON,
                        },
                        {
                            name: 'DECELERA!',
                            labelColor: 'rgba(0,0,0,0)',
                            activeBarColor: '#FF7F7F',
                        }
                        ]}
                />
            </View>
            <View style={styles.percentCompletedView}>
                <HeaderSection title='Lectura del motor'/>
                <ProgressBarAndroid
                    styleAttr="Horizontal"
                    style={styles.percentCompletedBar}
                    indeterminate={false}
                    progress={completed / 100}
                    color={COLORS.ACTIVE}
                    />
                <Text style={styles.percentCompletedText}>{JSON.stringify(completed)}%</Text>
            </View>
            <View style={styles.diagnosisMessageView}>
                <HeaderSection title='Instrucciones'/>
                <Text style={[styles.diagnosisMessageText, messageColor]}>
                    {message}
                </Text>
                <HeaderSection title='Repeticiones'/>
                <View style={{flexDirection: 'row'}}>
                    {progressIssues}
                </View>
            </View>             
        </View>
    );
};

const ActionButton = ({navigate, show}) => {
    if(!show) return null;
    return (
        <View style={styles.actionButtonView}>
            <Icon.Button
                name={'ios-pulse'}
                backgroundColor={COLORS.ACTIVE}
                onPress={() => navigate('FormDiagnosis')}>
                <Text style={styles.actionButtonText}>Enviar Diagnóstico</Text>
            </Icon.Button>
        </View>
    );
};

export const AdvancedIssueScreen = ({navigation}) => {
    const device = useOBD2Device();
    const [isVisible, setVisible] = useState(false);
    const data = useDiagnosisData(device, isVisible);
    const velocity = proccessIntValue(proccessValue('010D', data.pids));
    const rpm = proccessIntValue(proccessValue('010C', data.pids));
    return (
        <View style={styles.container}>
            <NavigationEvents
                onDidFocus={payload => {
                    //console.log('OBD2 subs AdvancedIssueScreen did focus',payload);
                    setVisible(true);
                }}
                onDidBlur={payload => {
                    //console.log('OBD2 subs AdvancedIssueScreen did blur',payload);
                    setVisible(false);
                }}
            />
            <View style={styles.bannerView}>
                <DiagnosisProccess
                    velocity={velocity}
                    rpms={rpm}
                    rpmsRange={data.rpms[data.issue] || data.rpms[0]}
                    issue={data.issue}
                    totalIssues={data.rpms.length}
                    completed={data.completed}
                    finished={data.finished}/>
            </View>
            <ActionButton
                navigate={navigation.replace}
                show={data.finished}
            />
        </View>
    );
};

AdvancedIssueScreen.navigationOptions = {
    title: 'Diagnóstico'
};

export default AdvancedIssueScreen;
