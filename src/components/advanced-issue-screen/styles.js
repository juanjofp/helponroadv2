import {StyleSheet} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    diagnosisBoardView: {
    },
    chart: {
        margin: 4,
        backgroundColor: COLORS.DRAWER_BACKGROUND
    },
    rangeView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'flex-end'
    },
    rangeMinText: {
        color: COLORS.TEXT,
        fontSize: 14,
        fontWeight: '300'
    },
    rangeMaxText: {
        color: COLORS.TEXT,
        fontSize: 14,
        fontWeight: '300'
    },
    rpmText: {
        color: COLORS.ACTIVE,
        fontSize: 24,
        fontWeight: '700'
    },
    hidelabel: {
        color: 'rgba(0,0,0,0)'
    },
    percentCompletedView: {
        margin: 4,
        backgroundColor: COLORS.DRAWER_BACKGROUND
    },
    percentCompletedBar: {
       marginLeft: 4,
       marginRight: 4 
    },
    percentCompletedText: {
        color: COLORS.TEXT,
        padding: 4,
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'center'
    },
    diagnosisMessageView: {
        margin: 4,
        backgroundColor: COLORS.DRAWER_BACKGROUND
    },
    diagnosisMessageText: {
        padding: 6,
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '500'
    },
    actionButtonView: {
        margin: 4,
        padding: 4
    },
    actionButtonText: {
        fontWeight: '700',
        fontSize: 18,
        color: 'white',
        flex: 1,
        textAlign: 'center'
    },
});