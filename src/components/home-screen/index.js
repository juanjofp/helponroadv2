import React, { useState } from 'react';
import {
    Text,
    View,
    FlatList,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { NavigationEvents } from 'react-navigation';
import HeaderSection from '../widgets/header-section';
import IconStatusView from '../widgets/icon-status-view/index';
import SensorView from '../widgets/sensor-view/index';
import { COLORS } from '../../theme';
import styles from './styles';

import { useBTStatus } from '../../services/use-bluetooth-context';
import { useFindBTDevices } from '../../services/use-find-btdevices';
import { useConnectDevice } from '../../services/use-connect-device';

export const DeviceActionButton = ({
    isBluetoothEnabled = false,
    isOBD2Enabled = false,
    isLoading = false,
    enableBluetooth,
    findDevices,
    selectDevice
}) => {
    if (!isBluetoothEnabled) {
        return (
            <View style={styles.actionButtonView}>
                <Icon.Button
                    name={'ios-bluetooth'}
                    backgroundColor={COLORS.ACTIVE}
                    disabled={ isLoading }
                    onPress={enableBluetooth}>
                    <Text style={styles.actionButtonText}>Activar Bluetooth</Text>
                </Icon.Button>
            </View>
        );
    }

    if (isOBD2Enabled) {
        return (
            <View style={styles.actionButtonView}>
                <Icon.Button
                    name={'ios-car'}
                    backgroundColor={COLORS.ACTIVE}
                    disabled={ isLoading }
                    onPress={() => selectDevice(null)}>
                    <Text style={styles.actionButtonText}>Desconectar OBD2</Text>
                </Icon.Button>
            </View>
        );
    }

    return (
        <View style={styles.actionButtonView}>
            <Icon.Button
                name={'ios-refresh'}
                backgroundColor={COLORS.ACTIVE}
                disabled={ isLoading }
                onPress={findDevices}>
                <Text style={styles.actionButtonText}>Buscar dispositivos</Text>
            </Icon.Button>
        </View>
    );
};

const DeviceItem = ({selectDevice, item, index}) => {
    return (
        <TouchableOpacity
            style={[styles.deviceItemView, {backgroundColor: (index % 2 === 0) ? '#CCC' : 'white'}]}
            onPress={() => selectDevice(item)}>
            <Text style={styles.itemName}>{item.name}</Text>
            <Text style={styles.itemAddress}>{item.address}</Text>
        </TouchableOpacity>
    );
};

const renderDeviceItem = (selectDevice, props) => <DeviceItem selectDevice={selectDevice} {...props}/>;

const NoDevicesMessages = {
    NOBT: {
        icon: 'ios-bluetooth',
        message: 'Necesitas activar el Bluetooth para poder buscar dispositivos cercanos'
    },
    BT: {
        icon: 'ios-refresh',
        message: 'Usa el botón buscar dispositivos cercanos para encontrar nuevos dispositivos bluetooth'
    }
};
const DeviceList = ({devices, selectDevice, isBluetoothEnabled = false, isLoading, isDeviceConnected}) => {

    if (isDeviceConnected) {
        return null;
    }

    if (isLoading) {
        return (
            <View style={styles.noDeviceListView}>
                <ActivityIndicator color={COLORS.ACTIVE} size='large'/>
                <Text style={styles.noDeviceListText}>Buscando dispositivos</Text>
            </View>
        );
    }

    if (!Array.isArray(devices) || devices.length <= 0) {
        const state = isBluetoothEnabled ? 'BT' : 'NOBT';
        return (
            <View style={styles.noDeviceListView}>
                <Icon name={NoDevicesMessages[state].icon} size={56} color={COLORS.ACTIVE} />
                <Text style={styles.noDeviceListText}>{NoDevicesMessages[state].message}</Text>
            </View>
        );
    }

    const renderDeviceCallback = renderDeviceItem.bind(null, selectDevice);
    return (
        <View
            style={styles.deviceListView}>
            <HeaderSection title='Dispositivos disponibles'/>
            <FlatList
                data={devices}
                renderItem={renderDeviceCallback}
                keyExtractor={(item) => item.address}
            />
        </View>
    );
};

const proccessValue = (key, data) => {
    const value = data[key];
    if (value && value.cmdResult && !value.cmdResult.includes('NODATA')) {
        return value.cmdResult;
    }
    return '--';
};

export const DeviceSelectedView = ({isConnecting, device, data, adapter, error}) => {
    if (!device) return null;
    if (isConnecting) {
        return (
            <View style={styles.deviceSelectedView}>
                <HeaderSection title='Dispositivo conectado'/>
                <View style={styles.deviceSelectedInfoView}>
                    <Text style={styles.itemName}>{device.name}</Text>
                    <Text style={styles.itemAddress}>{device.address}</Text>
                </View>
                <View style={styles.noDeviceListView}>
                    <ActivityIndicator color={COLORS.ACTIVE} size='large'/>
                    <Text style={styles.noDeviceListText}>{`Conectando con ${device.name}`}</Text>
                </View>
            </View>
        );
    }
    if (!!error) {
        return (
            <View style={styles.deviceSelectedView}>
                <HeaderSection title='Dispositivo conectado'/>
                <View style={styles.deviceSelectedInfoView}>
                    <Text style={styles.itemName}>{device.name}</Text>
                    <Text style={styles.itemAddress}>{device.address}</Text>
                </View>
                <View style={styles.noDeviceListView}>
                <Text style={styles.noDeviceListErrorText}>{`Error en dispositivo ${device.name}`}</Text>
                    <Text style={styles.noDeviceListText}>{error.message}</Text>
                </View>
            </View>
        );

    }
    return (
        <View style={styles.deviceSelectedView}>
            <HeaderSection title='Dispositivo conectado'/>
            <View style={styles.deviceSelectedInfoView}>
                <Text style={styles.itemName}>{device.name}</Text>
                <Text style={styles.itemAddress}>{device.address}</Text>
                <Text style={styles.itemAddress}>{adapter}</Text>
            </View>
            <SensorView
                title={'VIN: ' + proccessValue('0902', data)}
                sensors={[
                    {name: 'Aceite', type: 'Temperatura', value: proccessValue('0146', data)},
                    {name: 'Motor', type: 'Temperatura', value: proccessValue('0105', data)},
                    {name: 'Motor', type: 'Carga', value: proccessValue('0104', data)},
                    {name: 'Combus.', type: 'Litros', value: proccessValue('012F', data)}
                ]}
                />
        </View>
    );
};

export const HomeScreen = (props) => {
    const {isBluetoothEnabled, enableBluetooth} = useBTStatus();
    const {devices, isFinding, findDevices} = useFindBTDevices(isBluetoothEnabled);
    const [isVisible, setVisible] = useState(false);
    const {deviceConnected, isDeviceConnecting, data, error: errorDevice, setDeviceConnected} = useConnectDevice(isVisible);
    const messageDeviceStatus = !!deviceConnected ?
        `Conectado con ${deviceConnected.name}` :
        `No estás conectado a ningún dispositivo`;
        //console.log('Home props', props);
    return (
        <View style={styles.container}>
            <NavigationEvents
                onDidFocus={payload => {
                    //console.log('OBD2 subs HomeScreen did focus',payload);
                    setVisible(true);
                }}
                onDidBlur={payload => {
                    //console.log('OBD2 subs HomeScreen did blur',payload);
                    setVisible(false);
            }}
            />
            <HeaderSection title={messageDeviceStatus}/>
            <View style={styles.iconsView}>
                <IconStatusView
                    color={isBluetoothEnabled ? COLORS.DEVICE_ON : COLORS.DEVICE_OFF}
                    text={'Bluetooth'}
                    icon={'ios-bluetooth'}/>
                <IconStatusView
                    color={!!deviceConnected ? (isDeviceConnecting ? COLORS.DEVICE_CONNECTING : (!!errorDevice ? COLORS.ERRORTEXT : COLORS.DEVICE_ON)) : COLORS.DEVICE_OFF}
                    text={'OBD2'}
                    icon={'ios-car'}/>
            </View>
            <DeviceActionButton
                isBluetoothEnabled={isBluetoothEnabled}
                isOBD2Enabled={!!deviceConnected}
                isLoading={isFinding}
                enableBluetooth={enableBluetooth}
                findDevices={findDevices}
                selectDevice={setDeviceConnected}/>
            <DeviceList
                devices={devices}
                selectDevice={setDeviceConnected}
                isDeviceConnected={!!deviceConnected}
                isLoading={isFinding}
                isBluetoothEnabled={isBluetoothEnabled}/>
            <DeviceSelectedView
                isConnecting={isDeviceConnecting}
                error={errorDevice}
                device={deviceConnected}
                adapter={data.ATZ ? data.ATZ.cmdResult : ''}
                data={data}
                />
        </View>
    );
};

HomeScreen.navigationOptions = {
    title: 'Help On Road'
};

export default HomeScreen;
