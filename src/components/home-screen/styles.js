import {StyleSheet} from 'react-native';
import { COLORS } from '../../theme';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    iconsView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: COLORS.SCREEN_BACKGROUND,
        margin: 4,
    },
    actionButtonView: {
        margin: 4,
        padding: 4
    },
    actionButtonText: {
        fontWeight: '700',
        fontSize: 18,
        color: 'white',
        flex: 1,
        textAlign: 'center'
    },
    noDeviceListView: {
        marginTop: 36,
        alignItems: 'center'
    },
    noDeviceListText: {
        margin: 10,
        color: COLORS.TEXT_INVERT,
        fontWeight: '500',
        fontSize: 18,
        textAlign: 'center'
    },
    noDeviceListErrorText: {
        margin: 10,
        color: COLORS.ERRORTEXT,
        fontWeight: '700',
        fontSize: 22,
        textAlign: 'center'
    },
    deviceListView: {
        flex: 1
    },
    deviceItemView: {
        margin: 4,
        padding: 8
    },
    itemName: {
        color: COLORS.TEXT_INVERT,
        fontSize: 16,
        fontWeight: '500'
    },
    itemAddress: {
        color: COLORS.TEXT_INVERT,
        fontSize: 14,
        fontWeight: '100'
    },
    deviceSelectedView: {},
    deviceSelectedInfoView: {
        margin: 4,
        padding: 8
    }
});