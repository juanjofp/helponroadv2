import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    createAppContainer,
    createSwitchNavigator,
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import HomeScreen from './components/home-screen';
import MaintenanceScreen from './components/maintenance-screen';
import GarageMapScreen from './components/garage-map-screen';
import GarageListScreen from './components/garage-list-screen';
import DiagnosisScreen from './components/diagnosis-screen';
import AdvancedIssueScreen from './components/advanced-issue-screen';
import FormDiagnosisScreen from './components/form-diagnosis-screen';
import DrivingScreen from './components/driving-screen';
import LoginScreen from './components/login-screen';
import AuthLoadingScreen from './components/auth-loading-screen';
import SettingsScreen from './components/settings-screen';
import DrawerMenu from './components/drawer-menu';
import { COLORS } from './theme';

const arrowButton = (navigation) => (
    <TouchableOpacity
        onPress={() => navigation.goBack()}
        style = {{padding: 20}}>
        <Icon name='ios-arrow-back' size={24} color={COLORS.ACTIVE} />
    </TouchableOpacity>
);

const themeStackNavigator = ({navigation}) => {
    return {
        headerStyle: {
            backgroundColor: COLORS.ACTION_BAR
        },
        headerTintColor: COLORS.ACTIVE,
        headerTitleStyle: {
            color: COLORS.ACTIVE
        },
        headerLeft: arrowButton(navigation)
    };
};

const burguerButton = (navigation) => (
    <TouchableOpacity
        onPress={() => navigation.toggleDrawer()}
        style = {{padding: 20}}>
        <Icon name='ios-menu' size={24} color={COLORS.ACTIVE} />
    </TouchableOpacity>
);

const themeDrawerNavigator = ({navigation}) => {
    return {
        headerStyle: {
            backgroundColor: COLORS.ACTION_BAR
        },
        headerTintColor: COLORS.ACTIVE,
        headerTitleStyle: {
            color: COLORS.ACTIVE
        },
        headerLeft: burguerButton(navigation)
    };
};

const HomeNavigator = createStackNavigator({
    Home: {screen: HomeScreen}
},{
    initialRouteName: 'Home',
    defaultNavigationOptions: themeDrawerNavigator
});
HomeNavigator.navigationOptions = {
    title: 'Help On Road',
    drawerIcon: ({tintColor}) => (<Icon name='ios-bluetooth' size={24} color={tintColor} />)
};

const GarageTabNavigator = createBottomTabNavigator({
    GarageList: {
        screen: GarageListScreen,
        navigationOptions: ({navigation}) => {
            return {
                title: 'Lista de Talleres',
                tabBarIcon: ({tintColor}) => <Icon name='ios-bookmarks' size={24} color={tintColor} />
            };
        }
    },
    GarageMap: {
        screen: GarageMapScreen,
        navigationOptions: ({navigation}) => {
            return {
                title: 'Mapa de Talleres',
                tabBarIcon: ({tintColor}) => <Icon name='ios-pin' size={24} color={tintColor} />
            };
        }
    }
}, {
    tabBarOptions: {
        activeTintColor: COLORS.ACTIVE,
        activeBackgroundColor: COLORS.BACKGROUND,
        labelStyle: {
          fontSize: 12,
        },
        style: {
          backgroundColor: COLORS.ACTION_BAR,
        },
      }
});
const MaintenanceNavigator = createStackNavigator({
    Maintenance: {screen: MaintenanceScreen},
    Garage: {
        screen: GarageTabNavigator,
        navigationOptions: ({navigation}) => {
            return {
                title: 'Talleres',
                headerLeft: arrowButton(navigation)
            };
        }
    }
},{
    initialRouteName: 'Maintenance',
    defaultNavigationOptions: themeDrawerNavigator
});
MaintenanceNavigator.navigationOptions = {
    title: 'Mantenimiento',
    drawerIcon: ({tintColor}) => (<Icon name='ios-construct' size={24} color={tintColor} />)
};
const DiagnosisNavigator = createStackNavigator({
    Diagnosis: {screen: DiagnosisScreen},
    AdvanceIssue: {
        screen: AdvancedIssueScreen,
        navigationOptions: ({navigation}) => {
            return {
                title: 'Diagnostico Avanzado',
                headerLeft: arrowButton(navigation)
            };
        }
    },
    FormDiagnosis: {
        screen: FormDiagnosisScreen,
        navigationOptions: ({navigation}) => {
            return {
                title: 'Formulario de contacto',
                headerLeft: arrowButton(navigation)
            };
        }
    }
},{
    initialRouteName: 'Diagnosis',
    defaultNavigationOptions: themeDrawerNavigator
});
DiagnosisNavigator.navigationOptions = {
    title: 'Diagnostico',
    drawerIcon: ({tintColor}) => (<Icon name='ios-pulse' size={24} color={tintColor} />)
};

const DrivingNavigator = createStackNavigator({
    Driving: {screen: DrivingScreen}
},{
    initialRouteName: 'Driving',
    defaultNavigationOptions: themeDrawerNavigator
});
DrivingNavigator.navigationOptions = {
    title: 'Panel de Conducción',
    drawerIcon: ({tintColor}) => (<Icon name='ios-car' size={24} color={tintColor} />)
};

const FormNavigator = createStackNavigator({
    GeneralFormDiagnosis: {
        screen: FormDiagnosisScreen,
        navigationOptions: ({navigation}) => {
            return {
                title: 'Operador'
            };
        }
    }
},{
    initialRouteName: 'GeneralFormDiagnosis',
    defaultNavigationOptions: themeDrawerNavigator
});
FormNavigator.navigationOptions = {
    title: 'Operador',
    drawerIcon: ({tintColor}) => (<Icon name='ios-people' size={24} color={tintColor} />)
};

const SettingsNavigator = createStackNavigator({
    Settings: {
        screen: SettingsScreen,
        navigationOptions: ({navigation}) => {
            return {
                title: 'Ajustes'
            };
        }
    }
},{
    initialRouteName: 'Settings',
    defaultNavigationOptions: themeDrawerNavigator
});
SettingsNavigator.navigationOptions = {
    title: 'Ajustes',
    drawerIcon: ({tintColor}) => (<Icon name='ios-settings' size={24} color={tintColor} />)
};

const AppNavigator = createDrawerNavigator({
    HomeSection: {
        screen: HomeNavigator
    },
    MaintenanceSection: {
        screen: MaintenanceNavigator
    },
    DiagnosisSection: {
        screen: DiagnosisNavigator
    },
    DrivingSection: {
        screen: DrivingNavigator
    },
    FormSection: {
        screen: FormNavigator
    },
    SettingsSection: {
        screen: SettingsNavigator
    }
},{
    initialRouteName: 'HomeSection',
    drawerWidth: 280,
    contentComponent: DrawerMenu,
    drawerBackgroundColor: COLORS.DRAWER_BACKGROUND,
    contentOptions: {
        activeTintColor: COLORS.ACTIVE,
        inactiveTintColor: COLORS.TEXT
    }
});

const AuthNavigator = createStackNavigator({
    Login: {screen: LoginScreen}
},{
    initialRouteName: 'Login',
    defaultNavigationOptions: themeDrawerNavigator
});

const App = createAppContainer(
    createSwitchNavigator({
        AuthLoading: AuthLoadingScreen,
        App: AppNavigator,
        Auth: AuthNavigator
    },{
        initialRouteName: 'AuthLoading'
    })
);

export default App;