export const DtcService = (code) => {
    console.log('Searching code', code);
    if(codes[code]) {
        return codes[code];
    }
    return {
        "description": `El código ${code} no está en nuestra base de datos`,
        "severity": -1.0
    };
};

export default DtcService;

const codes = {
    "P0001": {
        "description": "Control regulador volumen combustible - circuito abierto",
        "severity": 1.0
    },
    "P0002": {
        "description": "Control regulador volumen combustible - rango/funcionamiento circuito",
        "severity": 1.0
    },
    "P0003": {
        "description": "Control regulador volumen combustible - se\u00f1al baja",
        "severity": 1.0
    },
    "P0004": {
        "description": "Control regulador volumen combustible - se\u00f1al alta",
        "severity": 1.0
    },
    "P0005": {
        "description": "Valvula corte combustible - circuito abierto",
        "severity": 1.0
    },
    "P0006": {
        "description": "Valvula corte combustible - se\u00f1al baja",
        "severity": 1.0
    },
    "P0007": {
        "description": "Valvula corte combustible - se\u00f1al alta",
        "severity": 1.0
    },
    "P0008": {
        "description": "Sistema posicion motor (bloque 1) - rendimiento motor",
        "severity": 2.0
    },
    "P0009": {
        "description": "Sistema posicion motor (bloque 2) - rendimiento motor",
        "severity": 2.0
    },
    "P0010": {
        "description": "Actuador posicion arbol levas (bloque 1) - circuito defectuoso",
        "severity": 3.0
    },
    "P0011": {
        "description": "Posicion arbol levas (bloque 1) - encendido avanzado, rendimiento",
        "severity": 3.0
    },
    "P0012": {
        "description": "Posicion arbol levas (bloque 1) - encendido atrasado",
        "severity": 3.0
    },
    "P0013": {
        "description": "Actuador posicion arbol levas (bloque 1) - circuito defectuoso",
        "severity": 2.0
    },
    "P0014": {
        "description": "Actuador posicion arbol levas (bloque 1) - encendido avanzado, rendimiento",
        "severity": 3.0
    },
    "P0015": {
        "description": "Actuador posicion arbol levas (bloque 1) - encendido atrasado",
        "severity": 3.0
    },
    "P0016": {
        "description": "Posicion cigue\u00f1al-arbol levas (bloque 1 sensor A) - correlacion",
        "severity": 3.0
    },
    "P0017": {
        "description": "Posicion cigue\u00f1al-arbol levas (bloque 1 sensor B) - correlacion",
        "severity": 3.0
    },
    "P0018": {
        "description": "Posicion cigue\u00f1al-arbol levas (bloque 2 sensor A) - correlacion",
        "severity": 3.0
    },
    "P0019": {
        "description": "Posicion cigue\u00f1al-arbol levas (bloque 2 sensor B) - correlacion",
        "severity": 3.0
    },
    "P0020": {
        "description": "Actuador posicion arbol levas (bloque 2) - circuito defectuoso",
        "severity": 3.0
    },
    "P0021": {
        "description": "Posicion arbol levas (bloque 2) - encendido avanzado, rendimiento",
        "severity": 3.0
    },
    "P0022": {
        "description": "Posicion arbol levas (bloque 2) - encendido atrasado",
        "severity": 3.0
    },
    "P0023": {
        "description": "Actuador posicion arbol levas (bloque 2) - circuito defectuoso",
        "severity": 3.0
    },
    "P0024": {
        "description": "Actuador posicion arbol levas (bloque 2) - encendido avanzado, rendimiento",
        "severity": 3.0
    },
    "P0025": {
        "description": "Actuador posicion arbol levas (bloque 2) - encendido atrasado",
        "severity": 3.0
    },
    "P0026": {
        "description": "Circuito solenoide control valvula admision (bloque 1) - Problema de funcionamiento/ rango",
        "severity": 1.0
    },
    "P0027": {
        "description": "Circuito solenoide control valvula escape (bloque 1) - Problema de funcionamiento/ rango",
        "severity": 1.0
    },
    "P0028": {
        "description": "Circuito solenoide control valvula admision (bloque 2) - Problema de funcionamiento/ rango",
        "severity": 1.0
    },
    "P0029": {
        "description": "Circuito solenoide control valvula escape (bloque 2) - Problema de funcionamiento/ rango",
        "severity": 1.0
    },
    "P0030": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0031": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0032": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0033": {
        "description": "Valvula descarga turbocompresor - circuito defectuoso",
        "severity": 2.0
    },
    "P0034": {
        "description": "Valvula descarga turbocompresor - se\u00f1al baja",
        "severity": 2.0
    },
    "P0035": {
        "description": "Valvula descarga turbocompresor - se\u00f1al alta",
        "severity": 1.0
    },
    "P0036": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0037": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0038": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0039": {
        "description": "Valvula derivacion turbocompresor - Problema de funcionamiento/ rango",
        "severity": 2.0
    },
    "P0040": {
        "description": "Se\u00f1ales sensor oxigeno cambiadas (bloque 1 sensor 1 y bloque 2 sensor 1)",
        "severity": 1.0
    },
    "P0041": {
        "description": "Se\u00f1ales sensor oxigeno cambiadas (bloque 1 sensor 2 y bloque 2 sensor 2)",
        "severity": 1.0
    },
    "P0042": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0043": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0044": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0045": {
        "description": "Solenoide sobrealimentacion turbocompresor - circuito abierto",
        "severity": 3.0
    },
    "P0046": {
        "description": "Solenoide sobrealimentacion turbocompresor - Problema de funcionamiento/ rango, rendimiento",
        "severity": 3.0
    },
    "P0047": {
        "description": "Solenoide sobrealimentacion turbocompresor - se\u00f1al baja",
        "severity": 3.0
    },
    "P0048": {
        "description": "Solenoide sobrealimentacion turbocompresor - se\u00f1al alta",
        "severity": 3.0
    },
    "P0049": {
        "description": "Turbina turbocompresor - sobrevelocidad",
        "severity": 3.0
    },
    "P0050": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0051": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0052": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0053": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 1) - Resistencia del calentador",
        "severity": 1.0
    },
    "P0054": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 1) - resistencia",
        "severity": 1.0
    },
    "P0055": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 1) - resistencia",
        "severity": 1.0
    },
    "P0056": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0057": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0058": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0059": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 2) - resistencia",
        "severity": 1.0
    },
    "P0060": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 2) - resistencia",
        "severity": 1.0
    },
    "P0061": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 2) - resistencia",
        "severity": 1.0
    },
    "P0062": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0063": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0064": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0065": {
        "description": "Inyector asistido por aire - rango,funcionamiento",
        "severity": 2.0
    },
    "P0066": {
        "description": "Inyector asistido por aire - circuito defectuoso, se\u00f1al baja",
        "severity": 2.0
    },
    "P0067": {
        "description": "Inyector asistido por aire - se\u00f1al alta",
        "severity": 2.0
    },
    "P0068": {
        "description": "Correlacion sensor MAP/sensor MAF/Posicion mariposa",
        "severity": 2.0
    },
    "P0069": {
        "description": "Correlacion sensor presion absoluta colector/sensor presion barometrica",
        "severity": 2.0
    },
    "P0070": {
        "description": "Sensor temperatura aire ambiente - circuito defectuoso",
        "severity": 1.0
    },
    "P0071": {
        "description": "Sensor temperatura aire ambiente - rango,funcionamiento",
        "severity": 1.0
    },
    "P0072": {
        "description": "Sensor temperatura aire ambiente - se\u00f1al baja",
        "severity": 1.0
    },
    "P0073": {
        "description": "Sensor temperatura aire ambiente - se\u00f1al alta",
        "severity": 1.0
    },
    "P0074": {
        "description": "Sensor temperatura aire ambiente - Interrupcion intermitente",
        "severity": 1.0
    },
    "P0075": {
        "description": "Solenoide control valvula admision (bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0076": {
        "description": "Solenoide control valvula admision (bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0077": {
        "description": "Solenoide control valvula admision (bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0078": {
        "description": "Solenoide control valvula escape (bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0079": {
        "description": "Solenoide control valvula escape (bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0080": {
        "description": "Solenoide control valvula escape (bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0081": {
        "description": "Solenoide control valvula admision (bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0082": {
        "description": "Solenoide control valvula admision (bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0083": {
        "description": "Solenoide control valvula admision (bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0084": {
        "description": "Solenoide control valvula escape (bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0085": {
        "description": "Solenoide control valvula escape (bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0086": {
        "description": "Solenoide control valvula escape (bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0087": {
        "description": "Rampa combustible/Presion sistema demasiado baja",
        "severity": 2.0
    },
    "P0088": {
        "description": "Rampa combustible/Presion sistema demasiado alta",
        "severity": 2.0
    },
    "P0089": {
        "description": "Regulador presion combustible 1 - funcionamiento",
        "severity": 2.0
    },
    "P0090": {
        "description": "Solenoide dosificador combustible 1 - circuito abierto",
        "severity": 2.0
    },
    "P0091": {
        "description": "Solenoide dosificador combustible 1 - cortocircuito a masa",
        "severity": 2.0
    },
    "P0092": {
        "description": "Solenoide dosificador combustible 1 - cortocircuito a positivo",
        "severity": 2.0
    },
    "P0093": {
        "description": "Fuga en sistema combustible - fuga grande",
        "severity": 3.0
    },
    "P0094": {
        "description": "Fuga en sistema combustible - fuga peque\u00f1a",
        "severity": 3.0
    },
    "P0095": {
        "description": "Sensor temperatura aire admision 2 - circuito defectuoso",
        "severity": 2.0
    },
    "P0096": {
        "description": "Sensor temperatura aire admision 2 - rango,funcionamiento",
        "severity": 2.0
    },
    "P0097": {
        "description": "Sensor temperatura aire admision 2 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0098": {
        "description": "Sensor temperatura aire admision 2 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0099": {
        "description": "Sensor temperatura aire admision 2 - circuito intermitente",
        "severity": 2.0
    },
    "P0100": {
        "description": "Sensor masa/volumen aire - circuito defectuoso",
        "severity": 1.0
    },
    "P0101": {
        "description": "Sensor masa/volumen aire - fuga, obstrucci\u00f3n",
        "severity": 1.0
    },
    "P0102": {
        "description": "Sensor masa/volumen aire - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0103": {
        "description": "Sensor masa/volumen aire - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0104": {
        "description": "Sensor masa/volumen aire - interrupcion intermitente",
        "severity": 1.0
    },
    "P0105": {
        "description": "Sensor presion absoluta colector/presion barometrica - circuito defectuoso",
        "severity": 3.0
    },
    "P0106": {
        "description": "Sensor presion absoluta colector/presion barometrica - rango,funcionamiento, fuga en sistema de admisi\u00f3n/ escape",
        "severity": 3.0
    },
    "P0107": {
        "description": "Sensor presion absoluta colector/presion barometrica - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0108": {
        "description": "Sensor presion absoluta colector/presion barometrica - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0109": {
        "description": "Sensor presion absoluta colector/presion barometrica - interrupcion intermitente",
        "severity": 3.0
    },
    "P0110": {
        "description": "Sensor temperatura aire admision - circuito defectuoso",
        "severity": 1.0
    },
    "P0111": {
        "description": "Sensor temperatura aire admision - rango, funcionamiento",
        "severity": 1.0
    },
    "P0112": {
        "description": "Sensor temperatura aire admision - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0113": {
        "description": "Sensor temperatura aire admision - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0114": {
        "description": "Sensor temperatura aire admision - interrupcion intermitente",
        "severity": 1.0
    },
    "P0115": {
        "description": "Sensor temperatura refrigerante motor - circuito defectuoso",
        "severity": 3.0
    },
    "P0116": {
        "description": "Sensor temperatura refrigerante motor - rango, funcionamiento",
        "severity": 3.0
    },
    "P0117": {
        "description": "Sensor temperatura refrigerante motor - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0118": {
        "description": "Sensor temperatura refrigerante motor - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0119": {
        "description": "Sensor temperatura refrigerante motor - interrupcion intermitente",
        "severity": 2.0
    },
    "P0120": {
        "description": "Sensor posicion pedal acelerador A/Mariposa A - circuito defectuoso",
        "severity": 1.0
    },
    "P0121": {
        "description": "Sensor posicion pedal acelerador A/Mariposa A - rango, funcionamiento",
        "severity": 1.0
    },
    "P0122": {
        "description": "Sensor posicion pedal acelerador A/Mariposa A - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0123": {
        "description": "Sensor posicion pedal acelerador A/Mariposa A - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0124": {
        "description": "Sensor posicion pedal acelerador A/Mariposa A - interrupcion intermitente",
        "severity": 1.0
    },
    "P0125": {
        "description": "Temperatura refrigerante insuficiente para control combustible bucle cerrado",
        "severity": 2.0
    },
    "P0126": {
        "description": "Temperatura refrigerante insuficiente para funcionamiento estable",
        "severity": 2.0
    },
    "P0127": {
        "description": "Temperatura aire admision demasiado alta",
        "severity": 3.0
    },
    "P0128": {
        "description": "Termostato refrigerante - circuito defectuoso",
        "severity": 2.0
    },
    "P0129": {
        "description": "Presion barometrica demasiado baja",
        "severity": 2.0
    },
    "P0130": {
        "description": "Sensor oxigeno (Sensor 1 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0131": {
        "description": "Sensor oxigeno (Sensor 1 bloque 1) - Baja Tension",
        "severity": 1.0
    },
    "P0132": {
        "description": "Sensor oxigeno (Sensor 1 bloque 1) - Alta Tension",
        "severity": 1.0
    },
    "P0133": {
        "description": "Sensor oxigeno (Sensor 1 bloque 1) - respuesta lenta",
        "severity": 1.0
    },
    "P0134": {
        "description": "Sensor oxigeno (Sensor 1 bloque 1) - actividad no detectada",
        "severity": 1.0
    },
    "P0135": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0136": {
        "description": "Sensor oxigeno (Sensor 2 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0137": {
        "description": "Sensor oxigeno (Sensor 2 bloque 1) - Baja Tension",
        "severity": 1.0
    },
    "P0138": {
        "description": "Sensor oxigeno (Sensor 2 bloque 1) - Alta Tension",
        "severity": 1.0
    },
    "P0139": {
        "description": "Sensor oxigeno (Sensor 2 bloque 1) - respuesta lenta",
        "severity": 1.0
    },
    "P0140": {
        "description": "Sensor oxigeno (Sensor 2 bloque 1) - actividad no detectada",
        "severity": 1.0
    },
    "P0141": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0142": {
        "description": "Sensor oxigeno (Sensor 3 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0143": {
        "description": "Sensor oxigeno (Sensor 3 bloque 1) - Baja Tension",
        "severity": 1.0
    },
    "P0144": {
        "description": "Sensor oxigeno (Sensor 3 bloque 1) - Alta Tension",
        "severity": 1.0
    },
    "P0145": {
        "description": "Sensor oxigeno (Sensor 3 bloque 1) - respuesta lenta",
        "severity": 1.0
    },
    "P0146": {
        "description": "Sensor oxigeno (Sensor 3 bloque 1) - actividad no detectada",
        "severity": 1.0
    },
    "P0147": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0148": {
        "description": "Error alimentacion combustible",
        "severity": 2.0
    },
    "P0149": {
        "description": "Error reglaje combustible",
        "severity": 2.0
    },
    "P0150": {
        "description": "Sensor oxigeno (Sensor 1 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0151": {
        "description": "Sensor oxigeno (Sensor 1 bloque 2) - Baja Tension",
        "severity": 1.0
    },
    "P0152": {
        "description": "Sensor oxigeno (Sensor 1 bloque 2) - Alta Tension",
        "severity": 1.0
    },
    "P0153": {
        "description": "Sensor oxigeno (Sensor 1 bloque 2) - respuesta lenta",
        "severity": 1.0
    },
    "P0154": {
        "description": "Sensor oxigeno (Sensor 1 bloque 2) - actividad no detectada",
        "severity": 1.0
    },
    "P0155": {
        "description": "Sensor calentado oxigeno (Sensor 1 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0156": {
        "description": "Sensor oxigeno (Sensor 2 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0157": {
        "description": "Sensor oxigeno (Sensor 2 bloque 2) - Baja Tension",
        "severity": 1.0
    },
    "P0158": {
        "description": "Sensor oxigeno (Sensor 2 bloque 2) - Alta Tension",
        "severity": 1.0
    },
    "P0159": {
        "description": "Sensor oxigeno (Sensor 2 bloque 2) - respuesta lenta",
        "severity": 1.0
    },
    "P0160": {
        "description": "Sensor oxigeno (Sensor 2 bloque 2) - actividad no detectada",
        "severity": 1.0
    },
    "P0161": {
        "description": "Sensor calentado oxigeno (Sensor 2 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0162": {
        "description": "Sensor oxigeno (Sensor 3 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0163": {
        "description": "Sensor oxigeno (Sensor 3 bloque 2) - Baja Tension",
        "severity": 1.0
    },
    "P0164": {
        "description": "Sensor oxigeno (Sensor 3 bloque 2) - Alta Tension",
        "severity": 1.0
    },
    "P0165": {
        "description": "Sensor oxigeno (Sensor 3 bloque 2) - respuesta lenta",
        "severity": 1.0
    },
    "P0166": {
        "description": "Sensor oxigeno (Sensor 3 bloque 2) - actividad no detectada",
        "severity": 1.0
    },
    "P0167": {
        "description": "Sensor calentado oxigeno (Sensor 3 bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0168": {
        "description": "Temperatura combustible demasiado alta",
        "severity": 3.0
    },
    "P0169": {
        "description": "Composicion combustible incorrecta",
        "severity": 3.0
    },
    "P0170": {
        "description": "Regulacion inyeccion (bloque 1) - circuito defectuoso",
        "severity": 2.0
    },
    "P0171": {
        "description": "Regulacion inyeccion (bloque 1) - mezcla demasiado pobre",
        "severity": 2.0
    },
    "P0172": {
        "description": "Regulacion inyeccion (bloque 1) - demasiado rico",
        "severity": 2.0
    },
    "P0173": {
        "description": "Regulacion inyeccion (bloque 2) - circuito defectuoso",
        "severity": 2.0
    },
    "P0174": {
        "description": "Regulacion inyeccion (bloque 2) - mezcla demasiado pobre",
        "severity": 2.0
    },
    "P0175": {
        "description": "Regulacion inyeccion (bloque 2) - demasiado rico",
        "severity": 2.0
    },
    "P0176": {
        "description": "Sensor composicion combustible - circuito defectuoso",
        "severity": 2.0
    },
    "P0177": {
        "description": "Sensor composicion combustible - rango,funcionamiento",
        "severity": 2.0
    },
    "P0178": {
        "description": "Sensor composicion combustible - se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P0179": {
        "description": "Sensor composicion combustible - se\u00f1al entrada alta",
        "severity": 2.0
    },
    "P0180": {
        "description": "Sensor temperatura combustible A - circuito defectuoso",
        "severity": 1.0
    },
    "P0181": {
        "description": "Sensor temperatura combustible A - rango,funcionamiento",
        "severity": 1.0
    },
    "P0182": {
        "description": "Sensor temperatura combustible A - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0183": {
        "description": "Sensor temperatura combustible A - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0184": {
        "description": "Sensor temperatura combustible A - interrupcion intermitente",
        "severity": 1.0
    },
    "P0185": {
        "description": "Sensor temperatura combustible B - circuito defectuoso",
        "severity": 1.0
    },
    "P0186": {
        "description": "Sensor temperatura combustible B - rango,funcionamiento",
        "severity": 1.0
    },
    "P0187": {
        "description": "Sensor temperatura combustible B - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0188": {
        "description": "Sensor temperatura combustible B - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0189": {
        "description": "Sensor temperatura combustible B - interrupcion intermitente",
        "severity": 1.0
    },
    "P0190": {
        "description": "Sensor presion rampa combustible - circuito defectuoso",
        "severity": 2.0
    },
    "P0191": {
        "description": "Sensor presion rampa combustible - rango, funcionamiento",
        "severity": 2.0
    },
    "P0192": {
        "description": "Sensor presion rampa combustible - se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P0193": {
        "description": "Sensor presion rampa combustible - se\u00f1al entrada alta",
        "severity": 2.0
    },
    "P0194": {
        "description": "Sensor presion rampa combustible - interrupcion intermitente",
        "severity": 2.0
    },
    "P0195": {
        "description": "Sensor temperatura aceite motor - circuito defectuoso",
        "severity": 3.0
    },
    "P0196": {
        "description": "Sensor temperatura aceite motor - rango, funcionamiento",
        "severity": 3.0
    },
    "P0197": {
        "description": "Sensor temperatura aceite motor - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0198": {
        "description": "Sensor temperatura aceite motor - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0199": {
        "description": "Sensor de temperatura del aceite del motor - interrupci\u00f3n intermitente",
        "severity": 3.0
    },
    "P0200": {
        "description": "Inyector - circuito defectuoso",
        "severity": 2.0
    },
    "P0201": {
        "description": "Inyector Cilindro 1 - circuito defectuoso",
        "severity": 2.0
    },
    "P0202": {
        "description": "Inyector Cilindro 2 - circuito defectuoso",
        "severity": 2.0
    },
    "P0203": {
        "description": "Inyector Cilindro 3 - circuito defectuoso",
        "severity": 2.0
    },
    "P0204": {
        "description": "Inyector Cilindro 4 - circuito defectuoso",
        "severity": 2.0
    },
    "P0205": {
        "description": "Inyector Cilindro 5 - circuito defectuoso",
        "severity": 2.0
    },
    "P0206": {
        "description": "Inyector Cilindro 6 - circuito defectuoso",
        "severity": 2.0
    },
    "P0207": {
        "description": "Inyector Cilindro 7 - circuito defectuoso",
        "severity": 2.0
    },
    "P0208": {
        "description": "Inyector Cilindro 8 - circuito defectuoso",
        "severity": 2.0
    },
    "P0209": {
        "description": "Inyector Cilindro 9 - circuito defectuoso",
        "severity": 2.0
    },
    "P0210": {
        "description": "Inyector Cilindro 10 - circuito defectuoso",
        "severity": 2.0
    },
    "P0211": {
        "description": "Inyector Cilindro 11 - circuito defectuoso",
        "severity": 2.0
    },
    "P0212": {
        "description": "Inyector Cilindro 12 - circuito defectuoso",
        "severity": 2.0
    },
    "P0213": {
        "description": "Inyector arranque en frio 1 - circuito defectuoso",
        "severity": 1.0
    },
    "P0214": {
        "description": "Inyector arranque en frio 2 - circuito defectuoso",
        "severity": 1.0
    },
    "P0215": {
        "description": "Solenoide corte combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P0216": {
        "description": "Control reglaje inyeccion - circuito defectuoso",
        "severity": 1.0
    },
    "P0217": {
        "description": "Sobrecalentamiento motor",
        "severity": 3.0
    },
    "P0218": {
        "description": "Sobrecalentamiento transmision",
        "severity": 3.0
    },
    "P0219": {
        "description": "Sobreregimen motor",
        "severity": 3.0
    },
    "P0220": {
        "description": "Sensor posicion pedal acelerador B/Mariposa B - circuito defectuoso",
        "severity": 1.0
    },
    "P0221": {
        "description": "Sensor posicion pedal acelerador B/Mariposa B - rango,funcionamiento",
        "severity": 1.0
    },
    "P0222": {
        "description": "Sensor posicion pedal acelerador B/Mariposa B - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0223": {
        "description": "Sensor posicion pedal acelerador B/Mariposa B - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0224": {
        "description": "Sensor posicion pedal acelerador B/Mariposa B - interrupcion intermitente",
        "severity": 1.0
    },
    "P0225": {
        "description": "Sensor posicion pedal acelerador C/Mariposa C - circuito defectuoso",
        "severity": 1.0
    },
    "P0226": {
        "description": "Sensor posicion pedal acelerador C/Mariposa C - rango,funcionamiento",
        "severity": 1.0
    },
    "P0227": {
        "description": "Sensor posicion pedal acelerador C/Mariposa C - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0228": {
        "description": "Sensor posicion pedal acelerador C/Mariposa C - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0229": {
        "description": "Sensor posicion pedal acelerador C/Mariposa C - interrupcion intermitente",
        "severity": 1.0
    },
    "P0230": {
        "description": "Rele bomba combustible principal - circuito defectuoso",
        "severity": 2.0
    },
    "P0231": {
        "description": "Rele bomba combustible secundaria - se\u00f1al baja",
        "severity": 2.0
    },
    "P0232": {
        "description": "Rele bomba combustible secundaria - se\u00f1al alta",
        "severity": 2.0
    },
    "P0233": {
        "description": "Rele bomba combustible secundaria - interrupcion intermitente",
        "severity": 2.0
    },
    "P0234": {
        "description": "Sobrealimentacion motor - limite excedido",
        "severity": 3.0
    },
    "P0235": {
        "description": "Sobrealimentacion motor turbocompresor - limite no alcanzado",
        "severity": 3.0
    },
    "P0236": {
        "description": "Sensor presion absoluta colector A (turbo) - rango,funcionamiento",
        "severity": 3.0
    },
    "P0237": {
        "description": "Sensor presion absoluta colector A (turbo) - se\u00f1al baja",
        "severity": 3.0
    },
    "P0238": {
        "description": "Sensor presion absoluta colector A (turbo) - se\u00f1al alta",
        "severity": 3.0
    },
    "P0239": {
        "description": "Sensor presion absoluta colector B (turbo) - circuito defectuoso",
        "severity": 3.0
    },
    "P0240": {
        "description": "Sensor presion absoluta colector B (turbo) - rango,funcionamiento",
        "severity": 3.0
    },
    "P0241": {
        "description": "Sensor presion absoluta colector B (turbo) - se\u00f1al baja",
        "severity": 3.0
    },
    "P0242": {
        "description": "Sensor presion absoluta colector B (turbo) - se\u00f1al alta",
        "severity": 3.0
    },
    "P0243": {
        "description": "Valvula descarga turbo A - circuito defectuoso",
        "severity": 3.0
    },
    "P0244": {
        "description": "Valvula descarga turbo A - rango,funcionamiento",
        "severity": 3.0
    },
    "P0245": {
        "description": "Valvula descarga turbo A - se\u00f1al baja",
        "severity": 3.0
    },
    "P0246": {
        "description": "Valvula descarga turbo A - se\u00f1al alta",
        "severity": 3.0
    },
    "P0247": {
        "description": "Valvula descarga turbo B - circuito defectuoso",
        "severity": 3.0
    },
    "P0248": {
        "description": "Valvula descarga turbo B - rango,funcionamiento",
        "severity": 3.0
    },
    "P0249": {
        "description": "Valvula descarga turbo B - se\u00f1al baja",
        "severity": 3.0
    },
    "P0250": {
        "description": "Valvula descarga turbo B - se\u00f1al alta",
        "severity": 3.0
    },
    "P0251": {
        "description": "Bomba inyeccion A (arbol levas/rotor) - circuito defectuoso",
        "severity": 2.0
    },
    "P0252": {
        "description": "Bomba inyeccion A (arbol levas/rotor) - rango,funcionamiento",
        "severity": 2.0
    },
    "P0253": {
        "description": "Bomba inyeccion A (arbol levas/rotor) - se\u00f1al baja",
        "severity": 2.0
    },
    "P0254": {
        "description": "Bomba inyeccion A (arbol levas/rotor) - se\u00f1al alta",
        "severity": 2.0
    },
    "P0255": {
        "description": "Bomba inyeccion A (arbol levas/rotor) - interrupcion intermitente",
        "severity": 2.0
    },
    "P0256": {
        "description": "Bomba inyeccion B (arbol levas/rotor) - circuito defectuoso",
        "severity": 2.0
    },
    "P0257": {
        "description": "Bomba inyeccion B (arbol levas/rotor) - rango,funcionamiento",
        "severity": 2.0
    },
    "P0258": {
        "description": "Bomba inyeccion B (arbol levas/rotor) - se\u00f1al baja",
        "severity": 2.0
    },
    "P0259": {
        "description": "Bomba inyeccion B (arbol levas/rotor) - se\u00f1al alta",
        "severity": 2.0
    },
    "P0260": {
        "description": "Bomba inyeccion B (arbol levas/rotor) - interrupcion intermitente",
        "severity": 2.0
    },
    "P0261": {
        "description": "Inyector Cilindro 1 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0262": {
        "description": "Inyector Cilindro 1 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0263": {
        "description": "Cilindro 1 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0264": {
        "description": "Inyector Cilindro 2 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0265": {
        "description": "Inyector Cilindro 2 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0266": {
        "description": "Cilindro 2 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0267": {
        "description": "Inyector Cilindro 3 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0268": {
        "description": "Inyector Cilindro 3 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0269": {
        "description": "Cilindro 3 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0270": {
        "description": "Inyector Cilindro 4 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0271": {
        "description": "Inyector Cilindro 4 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0272": {
        "description": "Cilindro 4 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0273": {
        "description": "Inyector Cilindro 5 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0274": {
        "description": "Inyector Cilindro 5 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0275": {
        "description": "Cilindro 5 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0276": {
        "description": "Inyector Cilindro 6 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0277": {
        "description": "Inyector Cilindro 6 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0278": {
        "description": "Cilindro 6 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0279": {
        "description": "Inyector Cilindro 7 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0280": {
        "description": "Inyector Cilindro 7 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0281": {
        "description": "Cilindro 7 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0282": {
        "description": "Inyector Cilindro 8 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0283": {
        "description": "Inyector Cilindro 8 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0284": {
        "description": "Cilindro 8 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0285": {
        "description": "Inyector Cilindro 9 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0286": {
        "description": "Inyector Cilindro 9 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0287": {
        "description": "Cilindro 9 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0288": {
        "description": "Inyector Cilindro 10 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0289": {
        "description": "Inyector Cilindro 10 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0290": {
        "description": "Cilindro 10 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0291": {
        "description": "Inyector Cilindro 11 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0292": {
        "description": "Inyector Cilindro 11 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0293": {
        "description": "Cilindro 11 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0294": {
        "description": "Inyector Cilindro 12 - se\u00f1al baja",
        "severity": 2.0
    },
    "P0295": {
        "description": "Inyector Cilindro 12 - se\u00f1al alta",
        "severity": 2.0
    },
    "P0296": {
        "description": "Cilindro 12 - fallo contribucion/equilibrio",
        "severity": 3.0
    },
    "P0297": {
        "description": "Sobrevelocidad del vehiculo",
        "severity": 1.0
    },
    "P0298": {
        "description": "Temperatura aceite motor demasiado alta",
        "severity": 3.0
    },
    "P0299": {
        "description": "Turbocompresor- sobrealimentaci\u00f3n baja",
        "severity": 3.0
    },
    "P0300": {
        "description": "Fallo en uno o varios cilindros - falsa explosion detectada",
        "severity": 2.0
    },
    "P0301": {
        "description": "Cilindro 1 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0302": {
        "description": "Cilindro 2 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0303": {
        "description": "Cilindro 3 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0304": {
        "description": "Cilindro 4 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0305": {
        "description": "Cilindro 5 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0306": {
        "description": "Cilindro 6 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0307": {
        "description": "Cilindro 7 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0308": {
        "description": "Cilindro 8 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0309": {
        "description": "Cilindro 9 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0310": {
        "description": "Cilindro 10 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0311": {
        "description": "Cilindro 11 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0312": {
        "description": "Cilindro 12 - falsa explosion detectada",
        "severity": 2.0
    },
    "P0313": {
        "description": "Falsa explosion detectada - nivel bajo combustible",
        "severity": 1.0
    },
    "P0314": {
        "description": "Falsa explosion en un solo cilindro",
        "severity": 1.0
    },
    "P0315": {
        "description": "Sistema posicion cigue\u00f1al",
        "severity": 2.0
    },
    "P0316": {
        "description": "Falsa explosion durante arranque motor",
        "severity": 1.0
    },
    "P0317": {
        "description": "No encuentra hardware carretera desnivelada",
        "severity": 1.0
    },
    "P0318": {
        "description": "Sensor carretera desnivelada A - circuito defectuoso",
        "severity": 1.0
    },
    "P0319": {
        "description": "Sensor carretera desnivelada B - circuito defectuoso",
        "severity": 1.0
    },
    "P0320": {
        "description": "Sensor posicion cigue\u00f1al/regimen motor - circuito defectuoso",
        "severity": 3.0
    },
    "P0321": {
        "description": "Sensor posicion cigue\u00f1al/regimen motor - rango,funcionamiento",
        "severity": 3.0
    },
    "P0322": {
        "description": "Sensor posicion cigue\u00f1al/regimen motor - No hay se\u00f1al",
        "severity": 3.0
    },
    "P0323": {
        "description": "Sensor posicion cigue\u00f1al/regimen motor - Interrupcion intermitente",
        "severity": 3.0
    },
    "P0324": {
        "description": "Error sistema control detonacion",
        "severity": 2.0
    },
    "P0325": {
        "description": "Sensor detonacion 1 (bloque 1) - circuito defectuoso",
        "severity": 2.0
    },
    "P0326": {
        "description": "Sensor detonacion 1 (bloque 1) - rango funcionamiento",
        "severity": 2.0
    },
    "P0327": {
        "description": "Sensor detonacion 1 (bloque 1) - se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P0328": {
        "description": "Sensor detonacion 1 (bloque 1) - se\u00f1al entrada alta",
        "severity": 2.0
    },
    "P0329": {
        "description": "Sensor detonacion 1 (bloque 1) - interrupcion intermitente",
        "severity": 2.0
    },
    "P0330": {
        "description": "Sensor detonacion 2 (bloque 2) - circuito defectuoso",
        "severity": 2.0
    },
    "P0331": {
        "description": "Sensor detonacion 2 (bloque 2) - rango,funcionamiento",
        "severity": 2.0
    },
    "P0332": {
        "description": "Sensor detonacion 2 (bloque 2) - se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P0333": {
        "description": "Sensor detonacion 2 (bloque 2) - se\u00f1al entrada alta",
        "severity": 2.0
    },
    "P0334": {
        "description": "Sensor detonacion 2 (bloque 2) - interrupcion intermitente",
        "severity": 2.0
    },
    "P0335": {
        "description": "Sensor posicion cigue\u00f1al A - circuito defectuoso",
        "severity": 3.0
    },
    "P0336": {
        "description": "Sensor posicion cigue\u00f1al A - rango,funcionamiento",
        "severity": 3.0
    },
    "P0337": {
        "description": "Sensor posicion cigue\u00f1al A - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0338": {
        "description": "Sensor posicion cigue\u00f1al A - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0339": {
        "description": "Sensor posicion cigue\u00f1al A - interrupcion intermitente",
        "severity": 3.0
    },
    "P0340": {
        "description": "Sensor posicion arbol levas A (bloque 1) - circuito defectuoso",
        "severity": 3.0
    },
    "P0341": {
        "description": "Sensor posicion arbol levas A (bloque 1) - rango,funcionamiento",
        "severity": 3.0
    },
    "P0342": {
        "description": "Sensor posicion arbol levas A (bloque 1) - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0343": {
        "description": "Sensor posicion arbol levas A (bloque 1) - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0344": {
        "description": "Sensor posicion arbol levas A (bloque 1) - interrupcion intermitente",
        "severity": 3.0
    },
    "P0345": {
        "description": "Sensor posicion arbol levas A (bloque 2) - circuito defectuoso",
        "severity": 3.0
    },
    "P0346": {
        "description": "Sensor posicion arbol levas A (bloque 2) - rango,funcionamiento",
        "severity": 3.0
    },
    "P0347": {
        "description": "Sensor posicion arbol levas A (bloque 2) - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0348": {
        "description": "Sensor posicion arbol levas A (bloque 2) - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0349": {
        "description": "Sensor posicion arbol levas A (bloque 2) - interrupcion intermitente",
        "severity": 3.0
    },
    "P0350": {
        "description": "Bobina encendido Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0351": {
        "description": "Bobina encendido A Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0352": {
        "description": "Bobina encendido B Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0353": {
        "description": "Bobina encendido C Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0354": {
        "description": "Bobina encendido D Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0355": {
        "description": "Bobina encendido E Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0356": {
        "description": "Bobina encendido F Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0357": {
        "description": "Bobina encendido G Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0358": {
        "description": "Bobina encendido H Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0359": {
        "description": "Bobina encendido I Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0360": {
        "description": "Bobina encendido J Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0361": {
        "description": "Bobina encendido K Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0362": {
        "description": "Bobina encendido L Primaria/Secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P0363": {
        "description": "Falsa explosion detectada - cancelacion alimentacion combustible",
        "severity": 2.0
    },
    "P0364": {
        "description": "Reservado",
        "severity": -1.0
    },
    "P0365": {
        "description": "Sensor posicion arbol levas B, bloque 1 - circuito defectuoso",
        "severity": 3.0
    },
    "P0366": {
        "description": "Sensor posicion arbol levas B, bloque 1 - rango,funcionamiento",
        "severity": 3.0
    },
    "P0367": {
        "description": "Sensor posicion arbol levas B, bloque 1 - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0368": {
        "description": "Sensor posicion arbol levas B, bloque 1 - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0369": {
        "description": "Sensor posicion arbol levas B, bloque 1 - interrupcion intermitente",
        "severity": 3.0
    },
    "P0370": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion A - defectuosa",
        "severity": 1.0
    },
    "P0371": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion A - demasiados impulsos",
        "severity": 1.0
    },
    "P0372": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion A - pocos impulsos",
        "severity": 1.0
    },
    "P0373": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion A - Impulsos intermitentes",
        "severity": 1.0
    },
    "P0374": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion A - No hay impulsos",
        "severity": 1.0
    },
    "P0375": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion B - defectuosa",
        "severity": 1.0
    },
    "P0376": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion B - demasiados impulsos",
        "severity": 1.0
    },
    "P0377": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion B - pocos impulsos",
        "severity": 1.0
    },
    "P0378": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion B - impulsos intermitentes",
        "severity": 1.0
    },
    "P0379": {
        "description": "Referencia reglaje encendido, se\u00f1al alta resolucion B - No hay impulsos",
        "severity": 1.0
    },
    "P0380": {
        "description": "Calentadores, circuito A - defectuoso",
        "severity": 1.0
    },
    "P0381": {
        "description": "Testigo calentadores - circuito defectuoso",
        "severity": 1.0
    },
    "P0382": {
        "description": "Calentadores, circuito B - defectuoso",
        "severity": 1.0
    },
    "P0383": {
        "description": "Reservado por SAE J2012",
        "severity": -1.0
    },
    "P0384": {
        "description": "Reservado por SAE J2012",
        "severity": -1.0
    },
    "P0385": {
        "description": "Sensor posicion cigue\u00f1al B - circuito defectuoso",
        "severity": 3.0
    },
    "P0386": {
        "description": "Sensor posicion cigue\u00f1al B - rango,funcionamiento",
        "severity": 3.0
    },
    "P0387": {
        "description": "Sensor posicion cigue\u00f1al B - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0388": {
        "description": "Sensor posicion cigue\u00f1al B - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0389": {
        "description": "Sensor posicion cigue\u00f1al B - interrupcion intermitente",
        "severity": 3.0
    },
    "P0390": {
        "description": "Sensor posicion cigue\u00f1al B (bloque 2) - circuito defectuoso",
        "severity": 3.0
    },
    "P0391": {
        "description": "Sensor posicion cigue\u00f1al B (bloque 2) - rango,funcionamiento",
        "severity": 3.0
    },
    "P0392": {
        "description": "Sensor posicion cigue\u00f1al B (bloque 2) - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0393": {
        "description": "Sensor posicion cigue\u00f1al B (bloque 2) - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0394": {
        "description": "Sensor posicion cigue\u00f1al B (bloque 2) - interrupcion intermitente",
        "severity": 3.0
    },
    "P0400": {
        "description": "Rercirculacion gases escape - flujo defectuoso. Fuera de especificaciones EGR",
        "severity": 3.0
    },
    "P0401": {
        "description": "Rercirculacion gases escape - flujo insuficiente",
        "severity": 2.0
    },
    "P0402": {
        "description": "Rercirculacion gases escape - flujo excesivo",
        "severity": 3.0
    },
    "P0403": {
        "description": "Rercirculacion gases escape - circuito defectuoso",
        "severity": 2.0
    },
    "P0404": {
        "description": "Rercirculacion gases escape - rango,funcionamiento",
        "severity": 2.0
    },
    "P0405": {
        "description": "Sensor Valvula EGR A - se\u00f1al baja",
        "severity": 1.0
    },
    "P0406": {
        "description": "Sensor Valvula EGR A - se\u00f1al alta",
        "severity": 1.0
    },
    "P0407": {
        "description": "Sensor Valvula EGR B - se\u00f1al baja",
        "severity": 1.0
    },
    "P0408": {
        "description": "Sensor Valvula EGR B - se\u00f1al alta",
        "severity": 1.0
    },
    "P0409": {
        "description": "Sensor recirculacion gases escape A - circuito defectuoso",
        "severity": 1.0
    },
    "P0410": {
        "description": "Sistema Inyeccion aire secundario - defectuoso",
        "severity": 2.0
    },
    "P0411": {
        "description": "Sistema inyeccion aire secundario - flujo incorrecto",
        "severity": 2.0
    },
    "P0412": {
        "description": "Valvula inyeccion aire secundario A - circuito defectuoso",
        "severity": 1.0
    },
    "P0413": {
        "description": "Valvula inyeccion aire secundario A - circuito abierto",
        "severity": 1.0
    },
    "P0414": {
        "description": "Valvula inyeccion aire secundario A - cortocircuito",
        "severity": 1.0
    },
    "P0415": {
        "description": "Valvula inyeccion aire secundario B - circuito defectuoso",
        "severity": 1.0
    },
    "P0416": {
        "description": "Valvula inyeccion aire secundario B - circuito abierto",
        "severity": 1.0
    },
    "P0417": {
        "description": "Valvula inyeccion aire secundario B - cortocircuito",
        "severity": 1.0
    },
    "P0418": {
        "description": "Rele inyeccion aire secundario A - circuito defectuoso",
        "severity": 1.0
    },
    "P0419": {
        "description": "Rele inyeccion aire secundario B - circuito defectuoso",
        "severity": 1.0
    },
    "P0420": {
        "description": "Sistema catalizador (bloque 1) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0421": {
        "description": "Catalizador delantero (bloque 1) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0422": {
        "description": "Catalizador principal (bloque 1) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0423": {
        "description": "Catalizador calentado (bloque 1) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0424": {
        "description": "Catalizador calentado (bloque 1) - temperatura por debajo umbral",
        "severity": 1.0
    },
    "P0425": {
        "description": "Sensor temperatura catalizador (bloque 1)",
        "severity": 1.0
    },
    "P0426": {
        "description": "Sensor temperatura catalizador (bloque 1) - rango,funcionamiento",
        "severity": 1.0
    },
    "P0427": {
        "description": "Sensor temperatura catalizador (bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0428": {
        "description": "Sensor temperatura catalizador (bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0429": {
        "description": "Calentador catalizador (bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P0430": {
        "description": "Sistema catalizador (bloque 2) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0431": {
        "description": "Catalizador delantero (bloque 2) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0432": {
        "description": "Catalizador principal (bloque 2) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0433": {
        "description": "Catalizador calentado (bloque 2) - eficiencia por debajo umbral",
        "severity": 1.0
    },
    "P0434": {
        "description": "Catalizador calentado (bloque 2) - temperatura por debajo umbral",
        "severity": 1.0
    },
    "P0435": {
        "description": "Sensor temperatura catalizador (bloque 2)",
        "severity": 1.0
    },
    "P0436": {
        "description": "Sensor temperatura catalizador (bloque 2) - rango,funcionamiento",
        "severity": 1.0
    },
    "P0437": {
        "description": "Sensor temperatura catalizador (bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0438": {
        "description": "Sensor temperatura catalizador (bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0439": {
        "description": "Calentador catalizador (bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P0440": {
        "description": "Sistema emisiones evaporacion - defectuoso",
        "severity": 1.0
    },
    "P0441": {
        "description": "Sistema emisiones evaporacion - flujo incorrecto",
        "severity": 1.0
    },
    "P0442": {
        "description": "Sistema emisiones evaporacion - fuga peque\u00f1a",
        "severity": 1.0
    },
    "P0443": {
        "description": "Valvula control emisiones evaporacion - circuito defectuoso",
        "severity": 1.0
    },
    "P0444": {
        "description": "Valvula control emisiones evaporacion - circuito abierto",
        "severity": 1.0
    },
    "P0445": {
        "description": "Valvula control emisiones evaporacion - cortocircuito",
        "severity": 1.0
    },
    "P0446": {
        "description": "Sistema emisiones evaporacion, control ventilacion - circuito defectuoso",
        "severity": 1.0
    },
    "P0447": {
        "description": "Sistema emisiones evaporacion, control ventilacion - circuito abierto",
        "severity": 1.0
    },
    "P0448": {
        "description": "Sistema emisiones evaporacion, control ventilacion - cortocircuito",
        "severity": 1.0
    },
    "P0449": {
        "description": "Sistema emisiones evaporacion, valvula ventilacion - circuito defectuoso",
        "severity": 1.0
    },
    "P0450": {
        "description": "Sensor presion emisiones evaporacion - circuito defectuoso",
        "severity": 1.0
    },
    "P0451": {
        "description": "Sensor presion emisiones evaporacion - rango,funcionamiento",
        "severity": 1.0
    },
    "P0452": {
        "description": "Sensor presion emisiones evaporacion - se\u00f1al baja",
        "severity": 1.0
    },
    "P0453": {
        "description": "Sensor presion emisiones evaporacion - se\u00f1al alta",
        "severity": 1.0
    },
    "P0454": {
        "description": "Sensor presion emisiones evaporacion - interrupcion intermitente",
        "severity": 1.0
    },
    "P0455": {
        "description": "Sistema emisiones evaporacion - fuga grande",
        "severity": 1.0
    },
    "P0456": {
        "description": "Sistema emisiones evaporacion - fuga peque\u00f1a",
        "severity": 1.0
    },
    "P0457": {
        "description": "Sistema emisiones evaporacion - fuga detectada",
        "severity": 1.0
    },
    "P0458": {
        "description": "Valvula control emisiones evaporacion - se\u00f1al baja",
        "severity": 1.0
    },
    "P0459": {
        "description": "Sistema de emisiones por evaporaci\u00f3n - se\u00f1al alta ",
        "severity": 1.0
    },
    "P0460": {
        "description": "Sensor nivel deposito combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P0461": {
        "description": "Sensor nivel deposito combustible - rango,funcionamiento",
        "severity": 1.0
    },
    "P0462": {
        "description": "Sensor nivel deposito combustible - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P0463": {
        "description": "Sensor nivel deposito combustible - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P0464": {
        "description": "Sensor nivel deposito combustible - interrupcion intermitente",
        "severity": 1.0
    },
    "P0465": {
        "description": "Sensor flujo purga filtro emisiones evaporacion - circuito defectuoso",
        "severity": 1.0
    },
    "P0466": {
        "description": "Sensor flujo purga filtro emisiones evaporacion - rango, funcionamiento",
        "severity": 1.0
    },
    "P0467": {
        "description": "Sensor flujo purga filtro emisiones evaporacion - se\u00f1al baja",
        "severity": 1.0
    },
    "P0468": {
        "description": "Sensor flujo purga filtro emisiones evaporacion - se\u00f1al lta",
        "severity": 1.0
    },
    "P0469": {
        "description": "Sensor flujo purga filtro emisiones evaporacion - interrupcion intermitente",
        "severity": 1.0
    },
    "P0470": {
        "description": "Sensor presion gases escape - circuito defectuoso",
        "severity": 1.0
    },
    "P0471": {
        "description": "Sensor presion gases escape - rango,funcionamiento",
        "severity": 1.0
    },
    "P0472": {
        "description": "Sensor presion gases escape - se\u00f1al baja",
        "severity": 1.0
    },
    "P0473": {
        "description": "Sensor presion gases escape - se\u00f1al alta",
        "severity": 2.0
    },
    "P0474": {
        "description": "Sensor presion gases escape - interrupcion intermitente",
        "severity": 1.0
    },
    "P0475": {
        "description": "Valvula reguladora presion gases escape - circuito defectuoso",
        "severity": 2.0
    },
    "P0476": {
        "description": "Valvula reguladora presion gases escape - rango,funcionamiento",
        "severity": 2.0
    },
    "P0477": {
        "description": "Valvula reguladora presion gases escape - se\u00f1al baja",
        "severity": 2.0
    },
    "P0478": {
        "description": "Valvula reguladora presion gases escape - se\u00f1al alta",
        "severity": 2.0
    },
    "P0479": {
        "description": "Valvula reguladora presion gases escape - interrupcion intermitente",
        "severity": 2.0
    },
    "P0480": {
        "description": "Ventilador refrigerante motor 1 - circuito defectuoso",
        "severity": 2.0
    },
    "P0481": {
        "description": "Ventilador refrigerante motor 2 - circuito defectuoso",
        "severity": 2.0
    },
    "P0482": {
        "description": "Ventilador refrigerante motor 3 - circuito defectuoso",
        "severity": 2.0
    },
    "P0483": {
        "description": "Ventilador refrigerante motor, prueba plausibilidad - defectuoso",
        "severity": 2.0
    },
    "P0484": {
        "description": "Ventilador refrigerante motor, sobrecarga corriente en circuito",
        "severity": 2.0
    },
    "P0485": {
        "description": "Ventilador refrigerante motor, Potencia/Masa - circuito defectuoso",
        "severity": 2.0
    },
    "P0486": {
        "description": "Sensor valvula EGR B - circuito defectuoso",
        "severity": 3.0
    },
    "P0487": {
        "description": "Recirculacion gases escape/posicion Mariposa - circuito defectuoso",
        "severity": 1.0
    },
    "P0488": {
        "description": "Recirculacion gases escape/posicion Mariposa - rango,funcionamiento",
        "severity": 1.0
    },
    "P0489": {
        "description": "Recirculacion gases escape - se\u00f1al baja",
        "severity": 1.0
    },
    "P0490": {
        "description": "Recirculacion gases escape - se\u00f1al alta",
        "severity": 1.0
    },
    "P0491": {
        "description": "Sistema inyeccion aire secundario (bloque 1) - funcionamiento",
        "severity": 1.0
    },
    "P0492": {
        "description": "Sistema inyeccion aire secundario (bloque 2) - funcionamiento",
        "severity": 1.0
    },
    "P0493": {
        "description": "Sobrevelocidad ventilador refrigerante motor",
        "severity": 2.0
    },
    "P0494": {
        "description": "Velocidad ventilador refrigerante motor baja",
        "severity": 2.0
    },
    "P0495": {
        "description": "Velocidad ventilador refrigerante motor alta",
        "severity": 2.0
    },
    "P0496": {
        "description": "Sistema emisiones evaporacion - flujo purga alto",
        "severity": 1.0
    },
    "P0497": {
        "description": "Sistema emisiones evaporacion - flujo purga bajo",
        "severity": 1.0
    },
    "P0498": {
        "description": "Sistema emisiones evaporacion, control ventilacion - se\u00f1al baja",
        "severity": 1.0
    },
    "P0499": {
        "description": "Sistema emisiones evaporacion, control ventilacion - se\u00f1al alta",
        "severity": 1.0
    },
    "P0500": {
        "description": "Sensor velocidad vehiculo - circuito defectuoso",
        "severity": 2.0
    },
    "P0501": {
        "description": "Sensor velocidad vehiculo - rango,funcionamiento",
        "severity": 2.0
    },
    "P0502": {
        "description": "Sensor velocidad vehiculo - se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P0503": {
        "description": "Sensor velocidad vehiculo - se\u00f1al alta/intermitente",
        "severity": 2.0
    },
    "P0504": {
        "description": "Interruptor freno - correlacion A/B",
        "severity": 2.0
    },
    "P0505": {
        "description": "Sistema control ralenti - defectuoso",
        "severity": 1.0
    },
    "P0506": {
        "description": "Sistema control ralenti - rpm inferior a previsto",
        "severity": 1.0
    },
    "P0507": {
        "description": "Sistema control ralenti - rpm superior a previsto",
        "severity": 1.0
    },
    "P0508": {
        "description": "Control aire ralenti - se\u00f1al baja",
        "severity": 1.0
    },
    "P0509": {
        "description": "Control aire ralenti - se\u00f1al alta",
        "severity": 1.0
    },
    "P0510": {
        "description": "Interruptor mariposa cerrada - circuito defectuoso",
        "severity": 1.0
    },
    "P0511": {
        "description": "Control aire ralenti - circuito defectuoso",
        "severity": 1.0
    },
    "P0512": {
        "description": "Circuito peticion motor arranque - funcionamiento incorrecto",
        "severity": 1.0
    },
    "P0513": {
        "description": "Llave inmovilizadora incorrecta",
        "severity": 1.0
    },
    "P0514": {
        "description": "Sensor temperatura bateria - rango,funcionamiento",
        "severity": 1.0
    },
    "P0515": {
        "description": "Sensor temperatura bateria - circuito defectuoso",
        "severity": 1.0
    },
    "P0516": {
        "description": "Sensor temperatura bateria - se\u00f1al baja",
        "severity": 1.0
    },
    "P0517": {
        "description": "Sensor temperatura bateria - se\u00f1al alta",
        "severity": 1.0
    },
    "P0518": {
        "description": "Control aire ralenti - interrupcion intermitente",
        "severity": 1.0
    },
    "P0519": {
        "description": "Control aire ralenti - funcionamiento circuito",
        "severity": 1.0
    },
    "P0520": {
        "description": "Sensor/interruptor presion aceite motor - circuito defectuoso",
        "severity": 3.0
    },
    "P0521": {
        "description": "Sensor/interruptor presion aceite motor - rango,funcionamiento",
        "severity": 3.0
    },
    "P0522": {
        "description": "Sensor/interruptor presion aceite motor - baja tension",
        "severity": 3.0
    },
    "P0523": {
        "description": "Sensor/interruptor presion aceite motor - alta tension",
        "severity": 3.0
    },
    "P0524": {
        "description": "Presion aceite motor demasiado baja",
        "severity": 3.0
    },
    "P0525": {
        "description": "Control velocidad crucero - rango,funcionamiento",
        "severity": 1.0
    },
    "P0526": {
        "description": "Sensor velocidad ventilador refrigerante motor - circuito defectuoso",
        "severity": 2.0
    },
    "P0527": {
        "description": "Sensor velocidad ventilador refrigerante motor - rango,funcionamiento",
        "severity": 2.0
    },
    "P0528": {
        "description": "Sensor velocidad ventilador refrigerante motor - No hay se\u00f1al",
        "severity": 2.0
    },
    "P0529": {
        "description": "Refrigerante del motor - interrupci\u00f3n intermitente de circuito ",
        "severity": 2.0
    },
    "P0530": {
        "description": "Sensor presion refrigerante Aire acondicionado - circuito defectuoso",
        "severity": 1.0
    },
    "P0531": {
        "description": "Sensor presion refrigerante Aire acondicionado - rango,funcionamiento",
        "severity": 1.0
    },
    "P0532": {
        "description": "Sensor presion refrigerante Aire acondicionado - se\u00f1al baja",
        "severity": 1.0
    },
    "P0533": {
        "description": "Sensor presion refrigerante Aire acondicionado - se\u00f1al alta",
        "severity": 1.0
    },
    "P0534": {
        "description": "Perdida refrigerante aire acondicionado",
        "severity": 1.0
    },
    "P0535": {
        "description": "Sensor temperatura evaporador aire acondicionado - circuito defectuoso",
        "severity": 1.0
    },
    "P0536": {
        "description": "Sensor temperatura evaporador aire acondicionado - rango,funcionamiento",
        "severity": 1.0
    },
    "P0537": {
        "description": "Sensor temperatura evaporador aire acondicionado - se\u00f1al baja",
        "severity": 1.0
    },
    "P0538": {
        "description": "Sensor temperatura evaporador aire acondicionado - se\u00f1al alta",
        "severity": 1.0
    },
    "P0539": {
        "description": "Sensor temperatura evaporador aire acondicionado - interrupcion intermitente",
        "severity": 1.0
    },
    "P0540": {
        "description": "Calentador aire admision A - circuito defectuoso",
        "severity": 1.0
    },
    "P0541": {
        "description": "Calentador aire admision A - se\u00f1al baja",
        "severity": 1.0
    },
    "P0542": {
        "description": "Calentador aire admision A - se\u00f1al alta",
        "severity": 1.0
    },
    "P0543": {
        "description": "Calentador aire admision A - circuito abierto",
        "severity": 1.0
    },
    "P0544": {
        "description": "Sensor temperatura recirculacion gases escape (bloque 1) - circuito defectuoso",
        "severity": 2.0
    },
    "P0545": {
        "description": "Sensor temperatura recirculacion gases escape (bloque 1) - se\u00f1al baja",
        "severity": 2.0
    },
    "P0546": {
        "description": "Sensor temperatura recirculacion gases escape (bloque 1) - se\u00f1al alta",
        "severity": 2.0
    },
    "P0547": {
        "description": "Sensor temperatura gases escape (bloque 2) - circuito defectuoso",
        "severity": 2.0
    },
    "P0548": {
        "description": "Sensor temperatura gases escape (bloque 2) - se\u00f1al baja",
        "severity": 2.0
    },
    "P0549": {
        "description": "Sensor temperatura gases escape (bloque 2) - se\u00f1al alta",
        "severity": 2.0
    },
    "P0550": {
        "description": "Sensor/interruptor presion direccion asistida - circuito defectuoso",
        "severity": 3.0
    },
    "P0551": {
        "description": "Sensor/interruptor presion direccion asistida - rango,funcionamiento",
        "severity": 3.0
    },
    "P0552": {
        "description": "Sensor/interruptor presion direccion asistida - se\u00f1al baja",
        "severity": 3.0
    },
    "P0553": {
        "description": "Sensor/interruptor presion direccion asistida - se\u00f1al alta",
        "severity": 3.0
    },
    "P0554": {
        "description": "Sensor/interruptor presion direccion asistida - interrupcion intermitente",
        "severity": 3.0
    },
    "P0555": {
        "description": "Sensor presion servofreno - circuito defectuoso",
        "severity": 3.0
    },
    "P0556": {
        "description": "Sensor presion servofreno - rango,funcionamiento",
        "severity": 3.0
    },
    "P0557": {
        "description": "Sensor presion servofreno - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P0558": {
        "description": "Sensor presion servofreno - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P0559": {
        "description": "Sensor presion servofreno - interrupcion intermitente",
        "severity": 2.0
    },
    "P0560": {
        "description": "Tension del sistema - defectuosa",
        "severity": 2.0
    },
    "P0561": {
        "description": "Tension del sistema - inestable",
        "severity": 2.0
    },
    "P0562": {
        "description": "Tension del sistema - baja",
        "severity": 2.0
    },
    "P0563": {
        "description": "Tension del sistema - alta",
        "severity": 2.0
    },
    "P0564": {
        "description": "Control velocidad crucero se\u00f1a entrada A - circuito defectuoso",
        "severity": 2.0
    },
    "P0565": {
        "description": "Interruptor principal control velocidad, se\u00f1al ON (encendido) - defectuoso",
        "severity": 2.0
    },
    "P0566": {
        "description": "Interruptor principal control velocidad, se\u00f1al OFF (apagado) - defectuoso",
        "severity": 2.0
    },
    "P0567": {
        "description": "Interruptor selector control velocidad, RESUME (reanudacion) - defectuoso",
        "severity": 2.0
    },
    "P0568": {
        "description": "Interruptor principal control velocidad, se\u00f1al SET (fijacion) - defectuoso",
        "severity": 2.0
    },
    "P0569": {
        "description": "Interruptor selector control velocidad, se\u00f1al COAST (reduccion) - defectuoso",
        "severity": 2.0
    },
    "P0570": {
        "description": "Sensor posicion pedal acelerador control velocidad - defectuoso",
        "severity": 2.0
    },
    "P0571": {
        "description": "Interruptor de velocidad/de freno A - circuito defectuoso",
        "severity": 2.0
    },
    "P0572": {
        "description": "Interruptor de velocidad/de freno A - se\u00f1al baja",
        "severity": 2.0
    },
    "P0573": {
        "description": "Interruptor de velocidad/de freno A - se\u00f1al alta",
        "severity": 2.0
    },
    "P0574": {
        "description": "Control velocidad crucero - velocidad vehiculo alta",
        "severity": 2.0
    },
    "P0575": {
        "description": "Control velocidad crucero - circuito defectuoso",
        "severity": 2.0
    },
    "P0576": {
        "description": "Control velocidad crucero - se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P0577": {
        "description": "Control velocidad crucero - se\u00f1al entrada alta",
        "severity": 2.0
    },
    "P0578": {
        "description": "Control velocidad crucero, se\u00f1al entrada A - activacion permantente",
        "severity": 2.0
    },
    "P0579": {
        "description": "Control velocidad crucero, se\u00f1al entrada A - rango,funcionamiento",
        "severity": 2.0
    },
    "P0580": {
        "description": "Control velocidad crucero, se\u00f1al entrada A - se\u00f1al baja",
        "severity": 2.0
    },
    "P0581": {
        "description": "Control velocidad crucero, se\u00f1al entrada A - se\u00f1al alta",
        "severity": 2.0
    },
    "P0582": {
        "description": "Control velocidad crucero, control vacio - circuito abierto",
        "severity": 2.0
    },
    "P0583": {
        "description": "Control velocidad crucero, control vacio - se\u00f1al baja",
        "severity": 2.0
    },
    "P0584": {
        "description": "Control velocidad crucero, control vacio - se\u00f1al alta",
        "severity": 2.0
    },
    "P0585": {
        "description": "Control velocidad crucero, se\u00f1al entrada A/B - correlacion",
        "severity": 2.0
    },
    "P0586": {
        "description": "Control velocidad crucero, control ventilacion - circuito abierto",
        "severity": 2.0
    },
    "P0587": {
        "description": "Control velocidad crucero, control ventilacion - se\u00f1al baja",
        "severity": 2.0
    },
    "P0588": {
        "description": "Control velocidad crucero, control ventilacion - se\u00f1al alta",
        "severity": 2.0
    },
    "P0589": {
        "description": "Control velocidad crucero, se\u00f1al entrada B - circuito defectuoso",
        "severity": 2.0
    },
    "P0590": {
        "description": "Control velocidad crucero, se\u00f1al entrada B - activacion permanente",
        "severity": 2.0
    },
    "P0591": {
        "description": "Control velocidad crucero, se\u00f1al entrada B - rango,funcionamiento",
        "severity": 2.0
    },
    "P0592": {
        "description": "Control velocidad crucero, se\u00f1al entrada B - se\u00f1al baja",
        "severity": 2.0
    },
    "P0593": {
        "description": "Control velocidad crucero, se\u00f1al entrada B - se\u00f1al alta",
        "severity": 2.0
    },
    "P0594": {
        "description": "Control velocidad crucero, control actuador - circuito abierto",
        "severity": 2.0
    },
    "P0595": {
        "description": "Control velocidad crucero, control actuador - se\u00f1al baja",
        "severity": 2.0
    },
    "P0596": {
        "description": "Control velocidad crucero, control actuador - se\u00f1al alta",
        "severity": 2.0
    },
    "P0597": {
        "description": "Control calentador termostato - circuito abierto",
        "severity": 3.0
    },
    "P0598": {
        "description": "Control calentador termostato - se\u00f1al baja",
        "severity": 3.0
    },
    "P0599": {
        "description": "Control calentador termostato - se\u00f1al alta",
        "severity": 3.0
    },
    "P0600": {
        "description": "Bus de datos CAN - defectuoso",
        "severity": 2.0
    },
    "P0601": {
        "description": "Modulo control motor - memoria ROM",
        "severity": 3.0
    },
    "P0602": {
        "description": "Modulo control motor - error programacion",
        "severity": 3.0
    },
    "P0603": {
        "description": "Modulo control motor - error memoria permantente KAM",
        "severity": 3.0
    },
    "P0604": {
        "description": "Modulo control motor - error memoria RAM",
        "severity": 3.0
    },
    "P0605": {
        "description": "Modulo control motor - error memoria ROM",
        "severity": 3.0
    },
    "P0606": {
        "description": "Modulo de control - fallo del procesador",
        "severity": 3.0
    },
    "P0607": {
        "description": "Modulo de control - problema de funcionamiento",
        "severity": 3.0
    },
    "P0608": {
        "description": "Modulo control, se\u00f1al salida sensor velocidad A - defectuosa",
        "severity": 3.0
    },
    "P0609": {
        "description": "Modulo control, se\u00f1al salida sensor velocidad B - defectuosa",
        "severity": 3.0
    },
    "P0610": {
        "description": "Modulo control - error opciones vehiculo",
        "severity": 3.0
    },
    "P0611": {
        "description": "Modulo control inyector combustible - problema funcionamiento",
        "severity": 3.0
    },
    "P0612": {
        "description": "Modulo control inyector combustible - circuito Rele",
        "severity": 3.0
    },
    "P0613": {
        "description": "Modulo control transmision - error procesador",
        "severity": 3.0
    },
    "P0614": {
        "description": "Modulo control motor/transmision - discrepancia",
        "severity": 3.0
    },
    "P0615": {
        "description": "Rele motor arranque - circuito defectuoso",
        "severity": 1.0
    },
    "P0616": {
        "description": "Rele motor arranque - se\u00f1al baja",
        "severity": 1.0
    },
    "P0617": {
        "description": "Rele motor arranque - se\u00f1al alta",
        "severity": 1.0
    },
    "P0618": {
        "description": "Modulo control combustible alternativo - error memoria permanente KAM",
        "severity": 1.0
    },
    "P0619": {
        "description": "Modulo control combustible alternativo - error memoria RAM/ROM",
        "severity": 1.0
    },
    "P0620": {
        "description": "Control Alternador - circuito defectuoso",
        "severity": 1.0
    },
    "P0621": {
        "description": "Testigo alternador - circuito defectuoso",
        "severity": 1.0
    },
    "P0622": {
        "description": "Alternador, control de campo - circuito defectuoso",
        "severity": 1.0
    },
    "P0623": {
        "description": "Testigo control generador - circuito defectuoso",
        "severity": 1.0
    },
    "P0624": {
        "description": "Testigo control tapon llenado - circuito defectuoso",
        "severity": 1.0
    },
    "P0625": {
        "description": "Terminal campo generador - se\u00f1al baja",
        "severity": 1.0
    },
    "P0626": {
        "description": "Terminal campo generador - se\u00f1al alta",
        "severity": 1.0
    },
    "P0627": {
        "description": "Control bomba combustible - circuito abierto",
        "severity": 2.0
    },
    "P0628": {
        "description": "Control bomba combustible - se\u00f1al baja",
        "severity": 2.0
    },
    "P0629": {
        "description": "Control bomba combustible - se\u00f1al alta",
        "severity": 2.0
    },
    "P0630": {
        "description": "Numero bastidor VIN modulo motor/transmision - no programado o erroneo",
        "severity": -1.0
    },
    "P0631": {
        "description": "Numero bastidor VIN modulo transmision - no programado o erroneo",
        "severity": -1.0
    },
    "P0632": {
        "description": "Cuentakilometros modulo motor/transmision - no programado",
        "severity": -1.0
    },
    "P0633": {
        "description": "Llave inmovilizadora modulo motor/transmision - no programada",
        "severity": 1.0
    },
    "P0634": {
        "description": "Modulo motor/transmision - temperatura interna alta",
        "severity": 3.0
    },
    "P0635": {
        "description": "Control direccion asistida - circuito defectuoso",
        "severity": 3.0
    },
    "P0636": {
        "description": "Control direccion asistida - se\u00f1al baja",
        "severity": 3.0
    },
    "P0637": {
        "description": "Control direccion asistida - se\u00f1al alta",
        "severity": 3.0
    },
    "P0638": {
        "description": "Control actuador mariposa (bloque 1) - rango,funcionamiento",
        "severity": 1.0
    },
    "P0639": {
        "description": "Control actuador mariposa (bloque 2) - rango,funcionamiento",
        "severity": 1.0
    },
    "P0640": {
        "description": "Control calentador aire admision - circuito defectuoso",
        "severity": 1.0
    },
    "P0641": {
        "description": "Tension referencia sensor A - circuito abierto",
        "severity": 1.0
    },
    "P0642": {
        "description": "Control de detonacion motor - defectuoso",
        "severity": 2.0
    },
    "P0643": {
        "description": "Tension referencia sensor A - se\u00f1al alta",
        "severity": 1.0
    },
    "P0644": {
        "description": "Pantalla del conductor, comunicacion serie - circuito defectuoso",
        "severity": 1.0
    },
    "P0645": {
        "description": "Aire acondicionado",
        "severity": 1.0
    },
    "P0646": {
        "description": "Rele embrague compresor aire acondicionado - se\u00f1al baja",
        "severity": 1.0
    },
    "P0647": {
        "description": "Rele embrague compresor aire acondicionado - se\u00f1al alta",
        "severity": 1.0
    },
    "P0648": {
        "description": "Testigo control inmovilizador - circuito defectuoso",
        "severity": 1.0
    },
    "P0649": {
        "description": "Testigo velocidad crucero - circuito defectuoso",
        "severity": 2.0
    },
    "P0650": {
        "description": "Testigo de averias - circuito defectuoso",
        "severity": 2.0
    },
    "P0651": {
        "description": "Tension referencia sensor B - circuito abierto",
        "severity": 1.0
    },
    "P0652": {
        "description": "Tension referencia sensor B - se\u00f1al baja",
        "severity": 1.0
    },
    "P0653": {
        "description": "Tension referencia sensor B - se\u00f1al alta",
        "severity": 1.0
    },
    "P0654": {
        "description": "Regimen motor (rpm) se\u00f1al salida - circuito defectuoso",
        "severity": 2.0
    },
    "P0655": {
        "description": "Se\u00f1al salida testigo sobrecalentamiento motor - circuito defectuoso",
        "severity": 2.0
    },
    "P0656": {
        "description": "Se\u00f1al salida nivel combustible - circuito defectuoso",
        "severity": 2.0
    },
    "P0657": {
        "description": "Tension alimentacion actuador - circuito abierto",
        "severity": 2.0
    },
    "P0658": {
        "description": "Tension alimentacion actuador - se\u00f1al baja",
        "severity": 2.0
    },
    "P0659": {
        "description": "Tension alimentacion actuador - se\u00f1al alta",
        "severity": 2.0
    },
    "P0660": {
        "description": "Valvula control aire colector admision (bloque 1) - circuito abierto",
        "severity": 1.0
    },
    "P0661": {
        "description": "Valvula control aire colector admision (bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0662": {
        "description": "Valvula control aire colector admision (bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0663": {
        "description": "Valvula control aire colector admision (bloque 2) - circuito abierto",
        "severity": 1.0
    },
    "P0664": {
        "description": "Valvula control aire colector admision (bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0665": {
        "description": "Valvula control aire colector admision (bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0666": {
        "description": "Sensor temperatura interna modulo motor/transmision - circuito defectuoso",
        "severity": 2.0
    },
    "P0667": {
        "description": "Sensor temperatura interna modulo motor/transmision - rango,funcionamiento",
        "severity": 2.0
    },
    "P0668": {
        "description": "Sensor temperatura interna modulo motor/transmision - se\u00f1al baja",
        "severity": 2.0
    },
    "P0669": {
        "description": "Sensor temperatura interna modulo motor/transmision - se\u00f1al alta",
        "severity": 2.0
    },
    "P0670": {
        "description": "Modulo control calentadores - circuito defectuoso",
        "severity": 1.0
    },
    "P0671": {
        "description": "Calentadores cilindro 1 - circuito defectuoso",
        "severity": 1.0
    },
    "P0672": {
        "description": "Calentadores cilindro 2 - circuito defectuoso",
        "severity": 1.0
    },
    "P0673": {
        "description": "Calentadores cilindro 3 - circuito defectuoso",
        "severity": 1.0
    },
    "P0674": {
        "description": "Calentadores cilindro 4 - circuito defectuoso",
        "severity": 1.0
    },
    "P0675": {
        "description": "Calentadores cilindro 5 - circuito defectuoso",
        "severity": 1.0
    },
    "P0676": {
        "description": "Calentadores cilindro 6 - circuito defectuoso",
        "severity": 1.0
    },
    "P0677": {
        "description": "Calentadores cilindro 7 - circuito defectuoso",
        "severity": 1.0
    },
    "P0678": {
        "description": "Calentadores cilindro 8 - circuito defectuoso",
        "severity": 1.0
    },
    "P0679": {
        "description": "Calentadores cilindro 9 - circuito defectuoso",
        "severity": 1.0
    },
    "P0680": {
        "description": "Calentadores cilindro 10 - circuito defectuoso",
        "severity": 1.0
    },
    "P0681": {
        "description": "Calentadores cilindro 11 - circuito defectuoso",
        "severity": 1.0
    },
    "P0682": {
        "description": "Calentadores cilindro 12 - circuito defectuoso",
        "severity": 1.0
    },
    "P0683": {
        "description": "Comunicacion modulo calentadores/motor/transmision - incorrecto",
        "severity": 1.0
    },
    "P0684": {
        "description": "Comunicacion modulo calentadores/motor/transmision - rango,funcionamiento",
        "severity": 1.0
    },
    "P0685": {
        "description": "Rele alimentacion modulo motor/transmision - circuito abierto",
        "severity": 2.0
    },
    "P0686": {
        "description": "Rele alimentacion modulo motor/transmision - se\u00f1al baja",
        "severity": 2.0
    },
    "P0687": {
        "description": "Rele control motor - cortocircuito a masa",
        "severity": 2.0
    },
    "P0688": {
        "description": "Rele control motor - cortocircuito a positivo",
        "severity": 2.0
    },
    "P0689": {
        "description": "Rele alimentacion modulo motor/transmision - se\u00f1al baja",
        "severity": 2.0
    },
    "P0690": {
        "description": "Rele alimentacion modulo motor/transmision - se\u00f1al alta",
        "severity": 2.0
    },
    "P0691": {
        "description": "Ventilador refrigerante motor 1 - cortocircuito a masa",
        "severity": 2.0
    },
    "P0692": {
        "description": "Ventilador refrigerante motor 1 - cortocircuito a positivo",
        "severity": 2.0
    },
    "P0693": {
        "description": "Ventilador refrigerante motor 2 - cortocircuito a masa",
        "severity": 2.0
    },
    "P0694": {
        "description": "Ventilador refrigerante motor 2 - cortocircuito a positivo",
        "severity": 2.0
    },
    "P0695": {
        "description": "Ventilador refrigerante motor 3 - cortocircuito a masa",
        "severity": 2.0
    },
    "P0696": {
        "description": "Ventilador refrigerante motor 3 - cortocircuito a positivo",
        "severity": 2.0
    },
    "P0697": {
        "description": "Tension referencia sensor C - circuito abierto",
        "severity": 1.0
    },
    "P0698": {
        "description": "Tension referencia sensor C - se\u00f1al baja",
        "severity": 1.0
    },
    "P0699": {
        "description": "Tension referencia sensor C - se\u00f1al alta",
        "severity": 1.0
    },
    "P0700": {
        "description": "Sistema control transmision - defectuoso",
        "severity": 2.0
    },
    "P0701": {
        "description": "Sistema control transmision - rango,funcionamiento",
        "severity": 2.0
    },
    "P0702": {
        "description": "Sistema control transmision - electrico",
        "severity": 2.0
    },
    "P0703": {
        "description": "Convertidor par/interruptor freno B - circuito defectuoso",
        "severity": 2.0
    },
    "P0704": {
        "description": "Interruptor posicion pedal embrague - circuito defectuoso",
        "severity": 1.0
    },
    "P0705": {
        "description": "Sensor/Interruptor marchas cortas/largas P/R/N/D/L - circuito defectuoso",
        "severity": 2.0
    },
    "P0706": {
        "description": "Sensor/Interruptor marchas cortas/largas - rango,funcionamiento",
        "severity": 2.0
    },
    "P0707": {
        "description": "Sensor/Interruptor marchas cortas/largas - se\u00f1al baja",
        "severity": 2.0
    },
    "P0708": {
        "description": "Sensor/Interruptor marchas cortas/largas - se\u00f1al alta",
        "severity": 2.0
    },
    "P0709": {
        "description": "Sensor/Interruptor marchas cortas/largas - interrupcion intermitente",
        "severity": 2.0
    },
    "P0710": {
        "description": "Sensor temperatura aceite transmision - circuito defectuoso",
        "severity": 2.0
    },
    "P0711": {
        "description": "Sensor temperatura aceite transmision - rango,funcionamiento",
        "severity": 2.0
    },
    "P0712": {
        "description": "Sensor temperatura aceite transmision - se\u00f1al baja",
        "severity": 2.0
    },
    "P0713": {
        "description": "Sensor temperatura aceite transmision - se\u00f1al alta",
        "severity": 2.0
    },
    "P0714": {
        "description": "Sensor temperatura aceite transmision - interrupcion intermitente",
        "severity": 2.0
    },
    "P0715": {
        "description": "Sensor velocidad giro arbol turbina - circuito defectuoso",
        "severity": 3.0
    },
    "P0716": {
        "description": "Sensor velocidad giro arbol turbina - rango,funcionamiento",
        "severity": 3.0
    },
    "P0717": {
        "description": "Sensor velocidad giro arbol turbina - no hay se\u00f1al",
        "severity": 3.0
    },
    "P0718": {
        "description": "Sensor velocidad giro arbol turbina - interrupcion intermitente",
        "severity": 3.0
    },
    "P0719": {
        "description": "Convertidor par/interruptor freno B - se\u00f1al baja",
        "severity": 2.0
    },
    "P0720": {
        "description": "Sensor velocidad vehiculo - circuito defectuoso",
        "severity": 2.0
    },
    "P0721": {
        "description": "Sensor velocidad vehiculo - rango,funcionamiento",
        "severity": 2.0
    },
    "P0722": {
        "description": "Sensor velocidad vehiculo - no hay se\u00f1al",
        "severity": 2.0
    },
    "P0723": {
        "description": "Sensor velocidad vehiculo - interrupcion intermitente",
        "severity": 2.0
    },
    "P0724": {
        "description": "Convertidor par/interruptor freno B - se\u00f1al alta",
        "severity": 2.0
    },
    "P0725": {
        "description": "Se\u00f1al entrada regimen motor - circuito defectuoso",
        "severity": 2.0
    },
    "P0726": {
        "description": "Se\u00f1al entrada regimen motor - rango,funcionamiento",
        "severity": 2.0
    },
    "P0727": {
        "description": "Se\u00f1al entrada regimen motor - no hay se\u00f1al",
        "severity": 2.0
    },
    "P0728": {
        "description": "Se\u00f1al entrada regimen motor - interrupcion intermitente",
        "severity": 2.0
    },
    "P0729": {
        "description": "Relaci\u00f3n de marchas incorrecta, sensor/ interruptor de marchas cortas/ largas",
        "severity": 3.0
    },
    "P0730": {
        "description": "Relacion de marchas incorrecta",
        "severity": 3.0
    },
    "P0731": {
        "description": "Marcha 1 - relacion incorrecta",
        "severity": 3.0
    },
    "P0732": {
        "description": "Marcha 2 - relacion incorrecta",
        "severity": 3.0
    },
    "P0733": {
        "description": "Marcha 3 - relacion incorrecta",
        "severity": 3.0
    },
    "P0734": {
        "description": "Marcha 4 - relacion incorrecta",
        "severity": 3.0
    },
    "P0735": {
        "description": "Marcha 5 - relacion incorrecta",
        "severity": 3.0
    },
    "P0736": {
        "description": "Marcha atras - relacion incorrecta",
        "severity": 3.0
    },
    "P0737": {
        "description": "Regimen motor modulo control transmision - circuito salida",
        "severity": 3.0
    },
    "P0738": {
        "description": "Regimen motor modulo control transmision - se\u00f1al salida baja",
        "severity": 3.0
    },
    "P0739": {
        "description": "Regimen motor modulo control transmision - se\u00f1al salida alta",
        "severity": 3.0
    },
    "P0740": {
        "description": "Valvula embrague convertidor par - circuito defectuoso",
        "severity": 3.0
    },
    "P0741": {
        "description": "Valvula embrague convertidor par - funcionamiento,desactivado",
        "severity": 3.0
    },
    "P0742": {
        "description": "Valvula embrague convertidor par - activado permanente",
        "severity": 3.0
    },
    "P0743": {
        "description": "Valvula embrague convertidor par - circuito electrico",
        "severity": 3.0
    },
    "P0744": {
        "description": "Valvula embrague convertidor par - interrupcion intermitente",
        "severity": 3.0
    },
    "P0745": {
        "description": "Solenoide presion aceite transmision - circuito defectuoso",
        "severity": 3.0
    },
    "P0746": {
        "description": "Solenoide presion aceite transmision - funcionamiento,desactivado",
        "severity": 3.0
    },
    "P0747": {
        "description": "Solenoide presion aceite transmision - activado permanente",
        "severity": 3.0
    },
    "P0748": {
        "description": "Solenoide presion aceite transmision - circuito electrico",
        "severity": 3.0
    },
    "P0749": {
        "description": "Solenoide presion aceite transmision - interrupcion intermitente",
        "severity": 3.0
    },
    "P0750": {
        "description": "Electrovalvula cambio A - circuito defectuoso",
        "severity": 3.0
    },
    "P0751": {
        "description": "Electrov\u00e1lvula de cambio A - funcionamiento o desactivado permanentemente ",
        "severity": 3.0
    },
    "P0752": {
        "description": "Electrov\u00e1lvula de cambio A - activado permanentemente ",
        "severity": 3.0
    },
    "P0753": {
        "description": "Electrov\u00e1lvula de cambio A - componente electrico ",
        "severity": 3.0
    },
    "P0754": {
        "description": "Electrov\u00e1lvula de cambio A - interrupci\u00f3n intermitente de circuito ",
        "severity": 3.0
    },
    "P0755": {
        "description": "Electrov\u00e1lvula de cambio B - circuito defectuoso ",
        "severity": 3.0
    },
    "P0756": {
        "description": "Electrov\u00e1lvula de cambio B - funcionamiento o desactivado permanentemente ",
        "severity": 3.0
    },
    "P0757": {
        "description": "Electrov\u00e1lvula de cambio B - activado permanentemente ",
        "severity": 3.0
    },
    "P0758": {
        "description": "Electrov\u00e1lvula de cambio B - componente el\u00e9ctrico",
        "severity": 3.0
    },
    "P0759": {
        "description": "Electrov\u00e1lvula de cambio B - interrupci\u00f3n intermitente de circuito ",
        "severity": 3.0
    },
    "P0760": {
        "description": "Electrov\u00e1lvula de cambio C - circuito defectuoso ",
        "severity": 3.0
    },
    "P0761": {
        "description": "Electrov\u00e1lvula de cambio C - funcionamiento o desactivado permanentemente ",
        "severity": 3.0
    },
    "P0762": {
        "description": "Electrov\u00e1lvula de cambio C - activado permanentemente ",
        "severity": 3.0
    },
    "P0763": {
        "description": "Electrov\u00e1lvula de cambio C - componente electrico ",
        "severity": 3.0
    },
    "P0764": {
        "description": "Electrov\u00e1lvula de cambio C - interrupci\u00f3n intermitente de circuito ",
        "severity": 3.0
    },
    "P0765": {
        "description": "Electrov\u00e1lvula de cambio D - circuito defectuoso ",
        "severity": 3.0
    },
    "P0766": {
        "description": "Electrov\u00e1lvula de cambio D - funcionamiento o desactivado permanentemente ",
        "severity": 3.0
    },
    "P0767": {
        "description": "Electrov\u00e1lvula de cambio D - activado permanentemente",
        "severity": 3.0
    },
    "P0768": {
        "description": "Electrov\u00e1lvula de cambio D - componente electrico ",
        "severity": 3.0
    },
    "P0769": {
        "description": "Electrov\u00e1lvula de cambio D - interrupci\u00f3n intermitente de circuito ",
        "severity": 3.0
    },
    "P0770": {
        "description": "Electrov\u00e1lvula de cambio E - circuito defectuoso ",
        "severity": 3.0
    },
    "P0771": {
        "description": "Electrov\u00e1lvula de cambio E - funcionamiento o desactivado permanentemente",
        "severity": 3.0
    },
    "P0772": {
        "description": "Electrov\u00e1lvula de cambio E - activado permanentemente ",
        "severity": 3.0
    },
    "P0773": {
        "description": "Electrov\u00e1lvula de cambio E - componente el\u00e9ctrico ",
        "severity": 3.0
    },
    "P0774": {
        "description": "Electrov\u00e1lvula de cambio E - interrupci\u00f3n intermitente de circuito",
        "severity": 3.0
    },
    "P0775": {
        "description": "Solenoide de control de presi\u00f3n B - funcionamiento incorrecto ",
        "severity": 3.0
    },
    "P0776": {
        "description": "Solenoide de control de presi\u00f3n B - funcionamiento o desactivaci\u00f3n permanente ",
        "severity": 3.0
    },
    "P0777": {
        "description": "Solenoide de control de presi\u00f3n B - activaci\u00f3n permanente ",
        "severity": 3.0
    },
    "P0778": {
        "description": "Solenoide de control de presi\u00f3n B - aver\u00eda el\u00e9ctrica ",
        "severity": 3.0
    },
    "P0779": {
        "description": "Solenoide de control de presi\u00f3n B - interrupci\u00f3n intermitente ",
        "severity": 3.0
    },
    "P0780": {
        "description": "Selecci\u00f3n de marcha - cambio defectuoso ",
        "severity": 3.0
    },
    "P0781": {
        "description": "Selecci\u00f3n de marcha, 1-2 - cambio defectuoso ",
        "severity": 2.0
    },
    "P0782": {
        "description": "Selecci\u00f3n de marcha, 2-3 - cambio defectuoso ",
        "severity": 2.0
    },
    "P0783": {
        "description": "Selecci\u00f3n de marcha, 3-4 - cambio defectuoso",
        "severity": 2.0
    },
    "P0784": {
        "description": "Selecci\u00f3n de marcha, 4-5 - cambio defectuoso",
        "severity": 2.0
    },
    "P0785": {
        "description": "Electrov\u00e1lvula de cambio/ de reglaje - circuito defectuoso ",
        "severity": 2.0
    },
    "P0786": {
        "description": "Electrov\u00e1lvula de cambio/ de reglaje - problema de rango/ funcionamiento ",
        "severity": 2.0
    },
    "P0787": {
        "description": "Electrov\u00e1lvula de cambio/ de reglaje - se\u00f1al baja ",
        "severity": 2.0
    },
    "P0788": {
        "description": "Electrov\u00e1lvula de cambio/ de reglaje - se\u00f1al alta ",
        "severity": 2.0
    },
    "P0789": {
        "description": "Electrov\u00e1lvula de cambio/ de reglaje - se|al intermitente ",
        "severity": 2.0
    },
    "P0790": {
        "description": "Interruptor de selecci\u00f3n de modo del cambio - circuito defectuoso ",
        "severity": 2.0
    },
    "P0791": {
        "description": "Sensor de velocidad de giro del \u00e1rbol intermedio -circuito defectuoso",
        "severity": 3.0
    },
    "P0792": {
        "description": "Sensor de velocidad de giro del \u00e1rbol intermedio - problema de rango/ funcionamiento ",
        "severity": 3.0
    },
    "P0793": {
        "description": "Sensor de velocidad de giro del \u00e1rbol intermedio - no hay se\u00f1al Cableado",
        "severity": 3.0
    },
    "P0794": {
        "description": "Sensor de velocidad de giro del \u00e1rbol intermedio - aver\u00eda intermitente del circuito ",
        "severity": 3.0
    },
    "P0795": {
        "description": "Solenoide de presi\u00f3n de aceite de la transmisi\u00f3n C - circuito defectuoso ",
        "severity": 3.0
    },
    "P0796": {
        "description": "Solenoide de presi\u00f3n de aceite de la transmisi\u00f3n C - funcionamiento o desactivaci\u00f3n permanente ",
        "severity": 3.0
    },
    "P0797": {
        "description": "Solenoide de presi\u00f3n de aceite de la transmisi\u00f3n C - activaci\u00f3n permanente ",
        "severity": 3.0
    },
    "P0798": {
        "description": "Solenoide de presi\u00f3n de aceite de la transmisi\u00f3n C - aver\u00eda el\u00e9ctrica ",
        "severity": 3.0
    },
    "P0799": {
        "description": "Solenoide de presi\u00f3n de aceite de la transmisi\u00f3n C - aver\u00eda intermitente del circuito ",
        "severity": 3.0
    },
    "P0800": {
        "description": "Sistema de control de la caja de transferencia, petici\u00f3n del testigo de aver\u00edas",
        "severity": 2.0
    },
    "P0801": {
        "description": "Mal funcionamiento en circuito de control de inhibici\u00f3n de reversa",
        "severity": 2.0
    },
    "P0802": {
        "description": "Sistema de control de la transmision, petici\u00f3n del testigo de aver\u00edas ",
        "severity": 2.0
    },
    "P0803": {
        "description": "Mal funcionamiento en circuito de control del solenoide de cambio 1 a 4(skip shift)",
        "severity": 2.0
    },
    "P0804": {
        "description": "Mal funcionamiento en circuito de control de la luz indicadora de cambio1 a 4(skip shift)",
        "severity": 2.0
    },
    "P0805": {
        "description": "Circuito del sensor de posici\u00f3n del embrague",
        "severity": 2.0
    },
    "P0806": {
        "description": "Rango/desempe\u00f1o del circuito del sensor de posici\u00f3n del embrague",
        "severity": 2.0
    },
    "P0807": {
        "description": "Sensor de posici\u00f3n del embrague - se\u00f1al de entrada baja",
        "severity": 2.0
    },
    "P0808": {
        "description": "Sensor de posici\u00f3n del embrague - se\u00f1al de entrada alta",
        "severity": 2.0
    },
    "P0809": {
        "description": "Sensor de posici\u00f3n del embrague - se\u00f1al de entrada intermitente",
        "severity": 2.0
    },
    "P0810": {
        "description": "Error en control de posici\u00f3n del embrague",
        "severity": 2.0
    },
    "P0811": {
        "description": "Deslizamiento excesivo en el embrague",
        "severity": 2.0
    },
    "P0812": {
        "description": "Marcha atr\u00e1s - Circuito de entrada defectuoso",
        "severity": 2.0
    },
    "P0813": {
        "description": "Marcha atr\u00e1s - Circuito de salida defectuoso",
        "severity": 2.0
    },
    "P0814": {
        "description": "Indicador de posici\u00f3n de la transmisi\u00f3n (marcha atr\u00e1s) defectuoso",
        "severity": 2.0
    },
    "P0815": {
        "description": "Control de cambio ascendente defectuosos",
        "severity": 2.0
    },
    "P0816": {
        "description": "Control de cambio descendente defectuoso",
        "severity": 2.0
    },
    "P0817": {
        "description": "Circuito de inhabilitaci\u00f3n del motor de arranque",
        "severity": 2.0
    },
    "P0818": {
        "description": "Circuito del interruptor de desconexi\u00f3n del tren de transmisi\u00f3n defectuoso",
        "severity": 2.0
    },
    "P0819": {
        "description": "Correlaci\u00f3n de interruptor de cambio a mayor y menor a marchas cortas/ largas , mala conexi\u00f3n",
        "severity": 2.0
    },
    "P0820": {
        "description": "Sensor de posici\u00f3n X-Y de la palanca de cambios - circuito defectuoso",
        "severity": 2.0
    },
    "P0821": {
        "description": "Sensor de posici\u00f3n X de la palanca de cambios - circuito defectuoso",
        "severity": 1.0
    },
    "P0822": {
        "description": "Sensor de posici\u00f3n Y de la palanca de cambios - circuito defectuoso",
        "severity": 1.0
    },
    "P0823": {
        "description": "Sensor de posici\u00f3n X de la palanca de cambios intermitente -circuito defectuoso",
        "severity": 1.0
    },
    "P0824": {
        "description": "Sensor de posici\u00f3n Y de la palanca de cambios intermitente - circuito defectuoso",
        "severity": 1.0
    },
    "P0825": {
        "description": "Interruptor de direccionamiento de la palanca de cambios (anticipaci\u00f3n de cambios)",
        "severity": 1.0
    },
    "P0826": {
        "description": "Interruptor de cambio a mayor/ menor - circuito defectuoso",
        "severity": 1.0
    },
    "P0827": {
        "description": "Interruptor de cambio a mayor/ menor - se\u00f1al de entrada baja ",
        "severity": 1.0
    },
    "P0828": {
        "description": "Interruptor de cambio a mayor/ menor - se\u00f1al de entrada alta en circuito ",
        "severity": 1.0
    },
    "P0829": {
        "description": "Cambio a mayor 5-6 - Aver\u00eda mec\u00e1nica",
        "severity": 2.0
    },
    "P0830": {
        "description": "Interruptor A del pedal del embrague - circuito defectuoso",
        "severity": 1.0
    },
    "P0831": {
        "description": "Interruptor A del pedal del embrague bajo - circuito defectuoso",
        "severity": 1.0
    },
    "P0832": {
        "description": "Interruptor A del pedal del embrague alto - circuito defectuoso",
        "severity": 1.0
    },
    "P0833": {
        "description": "Interruptor B del pedal del embrague - circuito defectuoso",
        "severity": 1.0
    },
    "P0834": {
        "description": "Interruptor B del pedal del embrague bajo - circuito defectuoso",
        "severity": 1.0
    },
    "P0835": {
        "description": "Interruptor B del pedal del embrague alto - circuito defectuoso",
        "severity": 1.0
    },
    "P0836": {
        "description": "Interruptor de tracci\u00f3n en las 4 ruedas (4WD) - circuito defectuoso",
        "severity": 1.0
    },
    "P0837": {
        "description": "Interruptor de tracci\u00f3n en las 4 ruedas(4WD) - problema de rango",
        "severity": 1.0
    },
    "P0838": {
        "description": "Interruptor de tracci\u00f3n en las 4 ruedas (4WD) - se\u00f1al baja",
        "severity": 1.0
    },
    "P0839": {
        "description": "Interruptor de tracci\u00f3n en las 4 ruedas (4WD) - se\u00f1al alta",
        "severity": 1.0
    },
    "P0840": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n A - problema de rango",
        "severity": 3.0
    },
    "P0841": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n A - problema de rango",
        "severity": 3.0
    },
    "P0842": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n A - se\u00f1al de entrada baja",
        "severity": 3.0
    },
    "P0843": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n A - se\u00f1al de entrada alta",
        "severity": 3.0
    },
    "P0844": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n A - aver\u00eda intermitente",
        "severity": 3.0
    },
    "P0845": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n B - circuito defectuoso",
        "severity": 3.0
    },
    "P0846": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n B - problema de rango",
        "severity": 3.0
    },
    "P0847": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n B - se\u00f1al de entrada baja",
        "severity": 3.0
    },
    "P0848": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n B - se\u00f1al de entrada alta",
        "severity": 3.0
    },
    "P0849": {
        "description": "Sensor de presi\u00f3n del aceite de la transmisi\u00f3n B -aver\u00eda intermitente",
        "severity": 3.0
    },
    "P0850": {
        "description": "Interruptor de posici\u00f3n estacionamiento/punto muerto -circuito de entrada defectuoso ",
        "severity": 1.0
    },
    "P0851": {
        "description": "Interruptor de posici\u00f3n estacionamiento/punto muerto -se\u00f1al de entrada baja en circuito ",
        "severity": 1.0
    },
    "P0852": {
        "description": "Interruptor de posici\u00f3n estacionamiento/punto muerto - se\u00f1al de entrada alta en circuito ",
        "severity": 1.0
    },
    "P0853": {
        "description": "Interruptor de conducci\u00f3n autom\u00e1tica de la transmisi\u00f3n - circuito de entrada defectuoso ",
        "severity": 1.0
    },
    "P0854": {
        "description": "Interruptor de conducci\u00f3n autom\u00e1tica de la transmisi\u00f3n -se\u00f1al de entrada baja en circuito ",
        "severity": 1.0
    },
    "P0855": {
        "description": "Interruptor de conducci\u00f3n autom\u00e1tica de la transmisi\u00f3n - se\u00f1al de entrada alta ",
        "severity": 1.0
    },
    "P0856": {
        "description": "Se\u00f1al de entrada del control de tracci\u00f3n - funcionamiento incorrecto ",
        "severity": 1.0
    },
    "P0857": {
        "description": "Se\u00f1al de entrada del control de tracci\u00f3n - problema de rango/ funcionamiento",
        "severity": 1.0
    },
    "P0858": {
        "description": "Se\u00f1al de entrada del control de tracci\u00f3n - baja ",
        "severity": 1.0
    },
    "P0859": {
        "description": "Se\u00f1al de entrada del control de tracci\u00f3n - alta ",
        "severity": 1.0
    },
    "P0860": {
        "description": "Circuito de comunicaci\u00f3n del m\u00f3dulo de cambio de marchas - funcionamiento incorrecto ",
        "severity": 2.0
    },
    "P0861": {
        "description": "Circuito de comunicaci\u00f3n del mdulo de cambio de marchas - se\u00f1al de entrada baja ",
        "severity": 2.0
    },
    "P0862": {
        "description": "Circuito de comunicaci\u00f3n del m\u00f3dulo de cambio de marchas - se\u00f1al de entrada alta",
        "severity": 2.0
    },
    "P0863": {
        "description": "Circuito de comunicaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - funcionamiento incorrecto ",
        "severity": 2.0
    },
    "P0864": {
        "description": "Circuito de comunicaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - problema de rango/ funcionamiento",
        "severity": 2.0
    },
    "P0865": {
        "description": "Circuito de comunicaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al de entrada baja ",
        "severity": 2.0
    },
    "P0866": {
        "description": "Circuito de comunicaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al de entrada alta ",
        "severity": 2.0
    },
    "P0867": {
        "description": "Sensor de presi\u00f3n de aceite de la transmis\u00f3n ",
        "severity": 3.0
    },
    "P0868": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n - baja ",
        "severity": 3.0
    },
    "P0869": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n - alta ",
        "severity": 3.0
    },
    "P0870": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n C - circuito defectuoso ",
        "severity": 3.0
    },
    "P0871": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n C - rango/ funcionamiento ",
        "severity": 3.0
    },
    "P0872": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n C - se\u00f1al baja ",
        "severity": 3.0
    },
    "P0873": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n C - se\u00f1al ",
        "severity": 3.0
    },
    "P0874": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n C - aver\u00eda intermitente del circuito ",
        "severity": 3.0
    },
    "P0875": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n D - circuito defectuoso ",
        "severity": 3.0
    },
    "P0876": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n D - rango/ funcionamiento",
        "severity": 3.0
    },
    "P0877": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n D - se\u00f1al baja ",
        "severity": 3.0
    },
    "P0878": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n D - se\u00f1al alta ",
        "severity": 3.0
    },
    "P0879": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n D - aver\u00eda intermitente del circuito ",
        "severity": 3.0
    },
    "P0880": {
        "description": "M\u00f3dulo de control de la transmisi\u00f3n - anomal\u00eda de la se|al de entrada de alimentaci\u00f3n",
        "severity": 2.0
    },
    "P0881": {
        "description": "M\u00f3dulo de control de la transmisi\u00f3n - rango/ funcionamiento de la se\u00f1al de entrada de alimentaci\u00f3n ",
        "severity": 2.0
    },
    "P0882": {
        "description": "M\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al baja de entrada de alimentaci\u00f3n ",
        "severity": 2.0
    },
    "P0883": {
        "description": "M\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al alta de entrada de alimentaci\u00f3n ",
        "severity": 2.0
    },
    "P0884": {
        "description": "M\u00f3dulo de control de la transmisi\u00f3n - anomal\u00eda intermitente de se\u00f1al de entrada de alimentaci\u00f3n ",
        "severity": 2.0
    },
    "P0885": {
        "description": "Rel\u00e9 de alimentaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - circuito de control abierto ",
        "severity": 1.0
    },
    "P0886": {
        "description": "Rel\u00e9 de alimentaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al baja en circuito de control",
        "severity": 1.0
    },
    "P0887": {
        "description": "Rel\u00e9 de alimentaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al alta en circuito de control ",
        "severity": 1.0
    },
    "P0888": {
        "description": "Rel\u00e9 de alimentaci\u00e9n del m\u00f3dulo de control de la transmisi\u00f3n - circuito de supervisi\u00f3n defectuoso ",
        "severity": 1.0
    },
    "P0889": {
        "description": "Rel\u00e9 de alimentaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - rango/ funcionamiento del circuito de supervisi\u00f3n ",
        "severity": 1.0
    },
    "P0890": {
        "description": "Rel\u00e9 de alimentaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al baja en circuito de supervisi\u00f3n ",
        "severity": 1.0
    },
    "P0891": {
        "description": "Rel\u00e9 de alimentaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - se\u00f1al alta en circuito de supervisi\u00f3n",
        "severity": 1.0
    },
    "P0892": {
        "description": "Rel\u00e9 de alimentaci\u00f3n del m\u00f3dulo de control de la transmisi\u00f3n - anomal\u00eda intermitente del circuito de supervision",
        "severity": 1.0
    },
    "P0893": {
        "description": "Varias marchas engranadas Aver\u00eda mec\u00e1nica",
        "severity": 3.0
    },
    "P0894": {
        "description": "Deslizamiento de componente de la transmisi\u00f3n Aver\u00eda mec\u00ednica",
        "severity": 3.0
    },
    "P0895": {
        "description": "Tiempo de cambio escaso Aver\u00eda mec\u00e1nica",
        "severity": 3.0
    },
    "P0896": {
        "description": "Tiempo de cambio excesivo Aver\u00eda mec\u00e1nica",
        "severity": 3.0
    },
    "P0897": {
        "description": "Aceite de la transmisi\u00f3n deteriorado Aver\u00eda mec\u00e1nica",
        "severity": 3.0
    },
    "P0898": {
        "description": "Sistema de control de la transmisi\u00f3n - petici\u00f3n del testigo de aver\u00edas - se\u00f1al baja ",
        "severity": 3.0
    },
    "P0899": {
        "description": "Sistema de control de la transmisi\u00f3n - petici\u00f3n del testigo de aver\u00edas - se\u00f1al alta",
        "severity": 3.0
    },
    "P0900": {
        "description": "Actuador del embrague - circuito abierto Cableado",
        "severity": 1.0
    },
    "P0901": {
        "description": "Actuador del embrague - rango/ funcionamiento del circuito",
        "severity": 1.0
    },
    "P0902": {
        "description": "Actuador del embrague - se\u00f1al baja Cableado",
        "severity": 1.0
    },
    "P0903": {
        "description": "Actuador del embrague - se\u00f1al alta Cableado",
        "severity": 1.0
    },
    "P0904": {
        "description": "Circuito de posici\u00f3n neutral de la caja de cambios - funcionamiento incorrecto ",
        "severity": 1.0
    },
    "P0905": {
        "description": "Circuito de posici\u00f3n neutral de la caja de cambios - rango/ funcionamiento",
        "severity": 1.0
    },
    "P0906": {
        "description": "Circuito de posici\u00f3n neutral de la caja de cambios - baja ",
        "severity": 1.0
    },
    "P0907": {
        "description": "Circuito de posici\u00f3n neutral de la caja de cambios - alta ",
        "severity": 1.0
    },
    "P0908": {
        "description": "Circuito de posici\u00f3n neutral de la caja de cambios - aver\u00eda intermitente del circuito",
        "severity": 1.0
    },
    "P0909": {
        "description": "Error en regulaci\u00f3n de posici\u00f3n neutral de la caja de cambios Aver\u00eda mec\u00e1nica",
        "severity": 3.0
    },
    "P0910": {
        "description": "Actuador seleccionador de posici\u00f3n neutral de la caja de cambios - circuito abierto ",
        "severity": 1.0
    },
    "P0911": {
        "description": "Actuador seleccionador de posici\u00f3n neutral de la caja de cambios - rango/ funcionamiento del circuito ",
        "severity": 1.0
    },
    "P0912": {
        "description": "Actuador seleccionador de posici\u00f3n neutral de la caja de cambios - se\u00f1al baja ",
        "severity": 1.0
    },
    "P0913": {
        "description": "Actuador seleccionador de posici\u00f3n neutral de la caja de cambios - se\u00f1al alta ",
        "severity": 1.0
    },
    "P0914": {
        "description": "Circuito de posici\u00f3n del cambio de marchas - funcionamiento incorrecto ",
        "severity": 1.0
    },
    "P0915": {
        "description": "Circuito de posici\u00f3n del cambio de marchas -rango/ funcionamiento ",
        "severity": 1.0
    },
    "P0916": {
        "description": "Circuito de posici\u00f3n del cambio de marchas - baja ",
        "severity": 1.0
    },
    "P0917": {
        "description": "Circuito de posici\u00f3n del cambio de marchas - alta ",
        "severity": 1.0
    },
    "P0918": {
        "description": "Circuito de posici\u00f3n del cambio de marchas - aver\u00eda intermitente",
        "severity": 1.0
    },
    "P0919": {
        "description": "Control de posici\u00f3n del cambio de marchas - error ",
        "severity": 1.0
    },
    "P0920": {
        "description": "Actuador de avance del cambio de marchas - circuito abierto ",
        "severity": 1.0
    },
    "P0921": {
        "description": "Actuador de avance del cambio de marchas - rango/ funcionamiento del circuito",
        "severity": 1.0
    },
    "P0922": {
        "description": "Actuador de avance del cambio de marchas - se\u00f1al baja ",
        "severity": 1.0
    },
    "P0923": {
        "description": "Actuador de avance del cambio de marchas - se\u00f1al alta",
        "severity": 1.0
    },
    "P0924": {
        "description": "Actuador de retroceso del cambio de marchas - circuito abierto ",
        "severity": 1.0
    },
    "P0925": {
        "description": "Actuador de retroceso del cambio de marchas - rango/ funcionamiento del circuito",
        "severity": 1.0
    },
    "P0926": {
        "description": "Actuador de retroceso del cambio de marchas - se\u00f1al baja",
        "severity": 1.0
    },
    "P0927": {
        "description": "Actuador de retroceso del cambio de marchas - se\u00f1al alta ",
        "severity": 1.0
    },
    "P0928": {
        "description": "Electrov\u00e1lvula de bloqueo del cambio de marchas -circuito abierto ",
        "severity": 1.0
    },
    "P0929": {
        "description": "Electrov\u00e1lvula de bloqueo del cambio de marchas - rango/ funcionamiento del circuito ",
        "severity": 1.0
    },
    "P0930": {
        "description": "Electrov\u00e1lvula de bloqueo del cambio de marchas -se\u00f1al baja ",
        "severity": 1.0
    },
    "P0931": {
        "description": "Electrov\u00e1lvula de bloqueo del cambio de marchas -se\u00f1al alta ",
        "severity": 1.0
    },
    "P0932": {
        "description": "Sensor de presi\u00f3n hidr\u00e1ulica - circuito defectuoso",
        "severity": 3.0
    },
    "P0933": {
        "description": "Sensor de presi\u00f3n hidr\u00e1ulica - rango/ funcionamiento ",
        "severity": 3.0
    },
    "P0934": {
        "description": "Sensor de presi\u00f3n hidr\u00e1ulica - se\u00f1al de entrada baja en circuito",
        "severity": 3.0
    },
    "P0935": {
        "description": "Sensor de presi\u00f3n hidr\u00e1ulica - se\u00f1al de entrada alta en circuito",
        "severity": 3.0
    },
    "P0936": {
        "description": "Sensor de presi\u00f3n hidr\u00e1ulica - interrupci\u00f3n intermitente de circuito",
        "severity": 3.0
    },
    "P0937": {
        "description": "Sensor de temperatura del aceite hidr\u00e1ulico - circuito defectuoso",
        "severity": 3.0
    },
    "P0938": {
        "description": "Sensor de temperatura del aceite hidr\u00e1ulico - rango/ funcionamiento ",
        "severity": 3.0
    },
    "P0939": {
        "description": "Sensor de temperatura del aceite hidr\u00e1ulico - se\u00f1al de entrada baja en circuito",
        "severity": 3.0
    },
    "P0940": {
        "description": "Sensor de temperatura del aceite hidr\u00e1ulico - se\u00f1al de entrada alta en circuito",
        "severity": 3.0
    },
    "P0941": {
        "description": "Sensor de temperatura del aceite hidr\u00e1ulico -interrupci\u00f3n intermitente de circuito",
        "severity": 3.0
    },
    "P0942": {
        "description": "Unidad de presi\u00f3n hidr\u00e1ulica Aver\u00eda mec\u00e1nica",
        "severity": 3.0
    },
    "P0943": {
        "description": "Unidad de presi\u00f3n hidr\u00e1ulica - recorrido demasiado corto",
        "severity": 3.0
    },
    "P0944": {
        "description": "Unidad de presi\u00f3n hidr\u00e1ulica - perdida de presi\u00f3n",
        "severity": 3.0
    },
    "P0945": {
        "description": "Rel\u00e9 de la bomba hidr\u00e1ulica - circuito abierto",
        "severity": 3.0
    },
    "P0946": {
        "description": "Rel\u00e9 de la bomba hidr\u00e1ulica - rango/ funcionamiento",
        "severity": 3.0
    },
    "P0947": {
        "description": "Rel\u00e9 de la bomba hidr\u00e1ulica - se\u00f1al baja ",
        "severity": 3.0
    },
    "P0948": {
        "description": "Rel\u00e9 de la bomba hidr\u00e1ulica - se\u00f1al alta",
        "severity": 3.0
    },
    "P0949": {
        "description": "Cambio manual automatizado (ASM) - valores adaptativos no registrados ",
        "severity": 1.0
    },
    "P0950": {
        "description": "Circuito de control del cambio manual automatizado (ASM) ",
        "severity": 1.0
    },
    "P0951": {
        "description": "Circuito de control del cambio manual automatizado (ASM) - rango/ funcionamiento",
        "severity": 1.0
    },
    "P0952": {
        "description": "Circuito de control del cambio manual automatizado (ASM) - baja ",
        "severity": 1.0
    },
    "P0953": {
        "description": "Circuito de control del cambio manual automatizado (ASM) - alta",
        "severity": 1.0
    },
    "P0954": {
        "description": "Cambio manual automatizado (ASM) - aver\u00eda intermitente del circuito ",
        "severity": 1.0
    },
    "P0955": {
        "description": "Circuito del modo de cambio manual automatizado (ASM) - funcionamiento incorrecto",
        "severity": 1.0
    },
    "P0956": {
        "description": "Circuito del modo de cambio manual automatizado (ASM) - rango/ funcionamiento",
        "severity": 1.0
    },
    "P0957": {
        "description": "Circuito del modo de cambio manual automatizado (ASM) - baja ",
        "severity": 1.0
    },
    "P0958": {
        "description": "Circuito del modo de cambio manual automatizado (ASM) - alta ",
        "severity": 1.0
    },
    "P0959": {
        "description": "Circuito del modo de cambio manual automatizado (ASM) - aver\u00eda intermitente del circuito ",
        "severity": 1.0
    },
    "P0960": {
        "description": "Solenoide de control de presi\u00f3n A - circuito de control abierto",
        "severity": 3.0
    },
    "P0961": {
        "description": "Solenoide de control de presi\u00f3n A - rango/ funcionamiento ",
        "severity": 3.0
    },
    "P0962": {
        "description": "Solenoide de control de presi\u00f3n A - se\u00f1al baja",
        "severity": 3.0
    },
    "P0963": {
        "description": "Solenoide de control de presi\u00f3n A - se\u00f1al alta ",
        "severity": 3.0
    },
    "P0964": {
        "description": "Solenoide de control de presi\u00f3n B - circuito de control abierto",
        "severity": 3.0
    },
    "P0965": {
        "description": "Solenoide de control de presi\u00f3n B - rango/ funcionamiento",
        "severity": 3.0
    },
    "P0966": {
        "description": "Solenoide de control de presi\u00f3n B - se\u00f1al baja",
        "severity": 3.0
    },
    "P0967": {
        "description": "Solenoide de control de presi\u00f3n B - se\u00f1al alta",
        "severity": 3.0
    },
    "P0968": {
        "description": "Solenoide de control de presi\u00f3n C - circuito de control abierto",
        "severity": 3.0
    },
    "P0969": {
        "description": "Solenoide de control de presi\u00f3n C -rango/ funcionamiento del circuito de control ",
        "severity": 3.0
    },
    "P0970": {
        "description": "Solenoide de control de presi\u00f3n C - se\u00f1al baja en circuito de control",
        "severity": 3.0
    },
    "P0971": {
        "description": "Solenoide de control de presi\u00f3n C - se\u00f1al alta en circuito de control ",
        "severity": 3.0
    },
    "P0972": {
        "description": "Electrov\u00e1lvula de cambio A - rango/ funcionamiento del circuito de control",
        "severity": 2.0
    },
    "P0973": {
        "description": "Electrov\u00e1lvula de cambio A - se\u00f1al baja en circuito de control ",
        "severity": 2.0
    },
    "P0974": {
        "description": "Electrov\u00e1lvula de cambio A - se\u00f1al alta en circuito de control",
        "severity": 2.0
    },
    "P0975": {
        "description": "Electrov\u00e1lvula de cambio B - rango/ funcionamiento del circuito de control",
        "severity": 2.0
    },
    "P0976": {
        "description": "Electrov\u00e1lvula de cambio B - se\u00f1al baja en circuito de control",
        "severity": 2.0
    },
    "P0977": {
        "description": "Electrov\u00e1lvula de cambio B - se\u00f1al alta en circuito de control",
        "severity": 2.0
    },
    "P0978": {
        "description": "Electrov\u00e1lvula de cambio C - rango/ funcionamiento del circuito de control",
        "severity": 2.0
    },
    "P0979": {
        "description": "Electrov\u00e1lvula de cambio C - se\u00f1al baja en circuito de control",
        "severity": 2.0
    },
    "P0980": {
        "description": "Electrov\u00e1lvula de cambio C - se\u00f1al alta en circuito de control",
        "severity": 2.0
    },
    "P0981": {
        "description": "Electrov\u00e1lvula de cambio D - rango/ funcionamiento del circuito de control",
        "severity": 2.0
    },
    "P0982": {
        "description": "Electrov\u00e1lvula de cambio D - se\u00f1al baja en circuito de control",
        "severity": 2.0
    },
    "P0983": {
        "description": "Electrov\u00e1lvula de cambio D - se\u00f1al alta en circuito de control",
        "severity": 2.0
    },
    "P0984": {
        "description": "Electrov\u00e1lvula de cambio E - rango/ funcionamiento del circuito de control ",
        "severity": 2.0
    },
    "P0985": {
        "description": "Electrov\u00e1lvula de cambio E - se\u00f1al baja en circuito de control",
        "severity": 2.0
    },
    "P0986": {
        "description": "Electrov\u00e1lvula de cambio E - se\u00f1al alta en circuito de control",
        "severity": 2.0
    },
    "P0987": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n E - circuito defectuoso ",
        "severity": 3.0
    },
    "P0988": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n E - rango/ funcionamiento del circuito",
        "severity": 3.0
    },
    "P0989": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n E - se\u00f1al baja ",
        "severity": 3.0
    },
    "P0990": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n E - se\u00f1al alta",
        "severity": 3.0
    },
    "P0991": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n E - interrupci\u00f3n intermitente de circuito",
        "severity": 3.0
    },
    "P0992": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n F - circuito defectuoso",
        "severity": 3.0
    },
    "P0993": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n F - rango/ funcionamiento del circuito",
        "severity": 3.0
    },
    "P0994": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n F - se\u00f1al baja",
        "severity": 3.0
    },
    "P0995": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n F - se\u00f1al alta",
        "severity": 3.0
    },
    "P0996": {
        "description": "Sensor de presi\u00f3n de aceite de la transmisi\u00f3n F - interrupci\u00f3n intermitente de circuito",
        "severity": 3.0
    },
    "P0997": {
        "description": "Electrov\u00e1lvula de cambio F - rango/ funcionamiento del circuito de control",
        "severity": 1.0
    },
    "P0998": {
        "description": "Electrov\u00e1lvula de cambio F - se\u00f1al baja en circuito de control ",
        "severity": 1.0
    },
    "P0999": {
        "description": "Electrov\u00e1lvula de cambio F - se\u00f1al alta en circuito de control ",
        "severity": 1.0
    },
    "P1100": {
        "description": "Sensor flujo masa aire - interrupcion intermitente",
        "severity": 1.0
    },
    "P1101": {
        "description": "Sensor flujo masa aire - fuera limites",
        "severity": 1.0
    },
    "P1102": {
        "description": "Sensor MAF menor que previsto",
        "severity": 1.0
    },
    "P1103": {
        "description": "Sensor MAF mayor que previsto",
        "severity": 1.0
    },
    "P1104": {
        "description": "Sensor MAF - circuito defectuoso",
        "severity": 1.0
    },
    "P1105": {
        "description": "Sensor presion barometrica - circuito defectuoso",
        "severity": 2.0
    },
    "P1106": {
        "description": "Alternador/Sensor MAP se\u00f1al tension baja",
        "severity": 1.0
    },
    "P1107": {
        "description": "Alternador/Sensor MAP se\u00f1al tension baja",
        "severity": 1.0
    },
    "P1108": {
        "description": "Testigo Bateria, alternador - circuito defectuoso",
        "severity": 1.0
    },
    "P1109": {
        "description": "Temperatura aire admision - averia intermitente",
        "severity": 1.0
    },
    "P1110": {
        "description": "Temperatura aire admision - circuito abierto",
        "severity": 1.0
    },
    "P1111": {
        "description": "Temperatura aire admision - interrupcion intermitente/alta",
        "severity": 1.0
    },
    "P1112": {
        "description": "Temperatura aire admision - interrupcion intermitente/baja",
        "severity": 1.0
    },
    "P1113": {
        "description": "Temperatura aire admision - circuito abierto",
        "severity": 1.0
    },
    "P1114": {
        "description": "Temperatura motor intermitente/Aire admision se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P1115": {
        "description": "Temperatura motor intermitente/Aire admision se\u00f1al entrada alta",
        "severity": 2.0
    },
    "P1116": {
        "description": "Temperatura motor fuera de rango",
        "severity": 3.0
    },
    "P1117": {
        "description": "Sensor temperatura refrigerante motor - interrupcion intermitente",
        "severity": 2.0
    },
    "P1118": {
        "description": "Sensor Temperatura absoluta escape - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P1119": {
        "description": "Sensor Temperatura absoluta escape - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P1120": {
        "description": "Sensor posicion mariposa - fuera de rango",
        "severity": 2.0
    },
    "P1121": {
        "description": "Sensor posicion mariposa - intermitente,se\u00f1al alta",
        "severity": 2.0
    },
    "P1122": {
        "description": "Sensor posicion mariposa - intermitente,se\u00f1al baja",
        "severity": 2.0
    },
    "P1123": {
        "description": "Sensor posicion mariposa - valor elevado",
        "severity": 2.0
    },
    "P1124": {
        "description": "Sensor posicion mariposa - fuera limites",
        "severity": 2.0
    },
    "P1125": {
        "description": "Sensor posicion mariposa - interrupcion intermitente",
        "severity": 2.0
    },
    "P1126": {
        "description": "Sensor posicion mariposa - circuito defectuoso",
        "severity": 2.0
    },
    "P1127": {
        "description": "Temperatura escape - fuera de rango",
        "severity": 2.0
    },
    "P1128": {
        "description": "Sondas lambda 1 - transpuestos",
        "severity": 1.0
    },
    "P1129": {
        "description": "Sondas lambda 2 - transpuestos",
        "severity": 1.0
    },
    "P1130": {
        "description": "Sonda lambda - regulacion inyeccion limite",
        "severity": 1.0
    },
    "P1131": {
        "description": "Sonda lambda - mezcla pobre",
        "severity": 1.0
    },
    "P1132": {
        "description": "Sonda lambda - mezcla rica",
        "severity": 1.0
    },
    "P1133": {
        "description": "Sonda lambda 1 - Insuficiente",
        "severity": 1.0
    },
    "P1134": {
        "description": "Sonda lambda 1 - tiempo reaccion",
        "severity": 1.0
    },
    "P1135": {
        "description": "Sensor posicion pedal A - circuito defectuoso",
        "severity": 1.0
    },
    "P1136": {
        "description": "Ventilador motor - circuito defectuoso",
        "severity": 2.0
    },
    "P1137": {
        "description": "Sonda lambda 1 bloque 2 - mezcla pobre",
        "severity": 1.0
    },
    "P1138": {
        "description": "Sonda lambda 1 bloque 2 - mezcla rica",
        "severity": 1.0
    },
    "P1139": {
        "description": "Indicador agua en combustible - circuito defectuoso",
        "severity": 2.0
    },
    "P1140": {
        "description": "Agua en el combustible",
        "severity": 3.0
    },
    "P1141": {
        "description": "Indicador restriccion combustible - circuito defectuoso",
        "severity": 2.0
    },
    "P1142": {
        "description": "Restriccion combustible",
        "severity": 3.0
    },
    "P1143": {
        "description": "Valvula control aire asistido - rango,funcionamiento",
        "severity": 1.0
    },
    "P1144": {
        "description": "Valvula control aire asistido - circuito defectuoso",
        "severity": 1.0
    },
    "P1145": {
        "description": "Error de par calculado",
        "severity": 2.0
    },
    "P1150": {
        "description": "Sonda lambda - regulacion inyeccion al limite",
        "severity": 1.0
    },
    "P1151": {
        "description": "Sonda lambda - mezcla pobre",
        "severity": 1.0
    },
    "P1152": {
        "description": "Sonda lambda - mezcla rica",
        "severity": 1.0
    },
    "P1153": {
        "description": "Control combustible (Bloque 2) - mezcla pobre",
        "severity": 1.0
    },
    "P1154": {
        "description": "Control combustible (bloque 2) - mezcla rica",
        "severity": 1.0
    },
    "P1155": {
        "description": "Controlador alternativo combustible",
        "severity": 1.0
    },
    "P1156": {
        "description": "Interruptor seleccion combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P1157": {
        "description": "Sonda lambda 2 bloque 2 - mezcla pobre",
        "severity": 1.0
    },
    "P1158": {
        "description": "Sonda lambda 2 bloque 2 - mezcla rica",
        "severity": 1.0
    },
    "P1159": {
        "description": "Motor paso a paso combustible - circuito defectuoso",
        "severity": 2.0
    },
    "P1167": {
        "description": "Fallo diagnosis, Mariposa no soltada",
        "severity": 1.0
    },
    "P1168": {
        "description": "Sensor combustible rail - valor muy bajo",
        "severity": 2.0
    },
    "P1169": {
        "description": "Sensor combustible rail - valor muy alto",
        "severity": 2.0
    },
    "P1170": {
        "description": "Solenoide desconexion motor - fallo",
        "severity": 2.0
    },
    "P1171": {
        "description": "Sensor rotor - fallo",
        "severity": 2.0
    },
    "P1172": {
        "description": "Control rotor - fallo",
        "severity": 2.0
    },
    "P1173": {
        "description": "Calibracion rotor - fallo",
        "severity": 2.0
    },
    "P1174": {
        "description": "Sensor arbol levas - fallo",
        "severity": 3.0
    },
    "P1175": {
        "description": "Control arbol levas - fallo",
        "severity": 3.0
    },
    "P1176": {
        "description": "Calibracion arbol levas - fallo",
        "severity": 3.0
    },
    "P1177": {
        "description": "Fallo sincronizacion",
        "severity": 3.0
    },
    "P1178": {
        "description": "Circuito abierto",
        "severity": 2.0
    },
    "P1180": {
        "description": "Sensor temperatura combustible -se\u00f1al baja",
        "severity": 1.0
    },
    "P1181": {
        "description": "Sensor temperatura combustible -se\u00f1al alta",
        "severity": 1.0
    },
    "P1182": {
        "description": "Solenoide desconexion combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P1183": {
        "description": "Temperatura aceite motor - circuito defectuoso",
        "severity": 3.0
    },
    "P1184": {
        "description": "Temperatura aceite motor - fuera de rango",
        "severity": 3.0
    },
    "P1185": {
        "description": "Sensor temperatura bomba combustible - alto",
        "severity": 3.0
    },
    "P1186": {
        "description": "Sensor temperatura bomba combustible - bajo",
        "severity": 3.0
    },
    "P1187": {
        "description": "Seleccion variante",
        "severity": 1.0
    },
    "P1188": {
        "description": "Fallo calibracion memoria",
        "severity": 2.0
    },
    "P1189": {
        "description": "Velocidad Bomba - fallo se\u00f1al",
        "severity": 2.0
    },
    "P1190": {
        "description": "Resistencia Calibracion fuera de rango",
        "severity": 1.0
    },
    "P1191": {
        "description": "Tension linea llave",
        "severity": 1.0
    },
    "P1192": {
        "description": "Tension externa",
        "severity": 1.0
    },
    "P1193": {
        "description": "Controlador sobrecorriente EGR",
        "severity": 2.0
    },
    "P1194": {
        "description": "UCE conversor A/D",
        "severity": 1.0
    },
    "P1195": {
        "description": "Bomba combustible - fallo inicializar",
        "severity": 1.0
    },
    "P1196": {
        "description": "Tension apagado llave - valor alto",
        "severity": 1.0
    },
    "P1197": {
        "description": "Tension apagado llave - valor bajo",
        "severity": 1.0
    },
    "P1198": {
        "description": "Control rotor combustible - bajo combustible",
        "severity": 1.0
    },
    "P1199": {
        "description": "Nivel combustible - se\u00f1al entrada baja",
        "severity": 2.0
    },
    "P1200": {
        "description": "Inyector - rango,funcionamiento",
        "severity": 2.0
    },
    "P1201": {
        "description": "Inyector Cilindro 1 - circuito abierto,cortocicuito",
        "severity": 2.0
    },
    "P1202": {
        "description": "Inyector Cilindro 2 - circuito abierto,cortocicuito",
        "severity": 2.0
    },
    "P1203": {
        "description": "Inyector Cilindro 3 - circuito abierto,cortocicuito",
        "severity": 2.0
    },
    "P1204": {
        "description": "Inyector Cilindro 4 - circuito abierto,cortocicuito",
        "severity": 2.0
    },
    "P1205": {
        "description": "Inyector Cilindro 5 - circuito abierto,cortocicuito",
        "severity": 2.0
    },
    "P1206": {
        "description": "Inyector Cilindro 6 - circuito abierto,cortocicuito",
        "severity": 2.0
    },
    "P1209": {
        "description": "Presion control inyector - fallo sistema",
        "severity": 2.0
    },
    "P1210": {
        "description": "Presion control inyector - valor superior esperado",
        "severity": 2.0
    },
    "P1211": {
        "description": "Presion control inyector - alta/baja",
        "severity": 2.0
    },
    "P1212": {
        "description": "Presion control inyector - sin se\u00f1al",
        "severity": 2.0
    },
    "P1213": {
        "description": "Inyector arranque - circuito defectuoso",
        "severity": 2.0
    },
    "P1214": {
        "description": "Sensor posicion pedal B - interrupcion intermitente",
        "severity": 2.0
    },
    "P1215": {
        "description": "Sensor posicion pedal C - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P1216": {
        "description": "Sensor posicion pedal C - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P1217": {
        "description": "Sensor posicion pedal C - interrupcion intermitente",
        "severity": 1.0
    },
    "P1218": {
        "description": "Control Inyeccion - alto",
        "severity": 2.0
    },
    "P1219": {
        "description": "Control Inyeccion - bajo",
        "severity": 2.0
    },
    "P1220": {
        "description": "Control Mariposa - circuito defectuoso",
        "severity": 2.0
    },
    "P1221": {
        "description": "Control Traccion - circuito defectuoso",
        "severity": 2.0
    },
    "P1222": {
        "description": "Control Traccuin salida - circuito defectuoso",
        "severity": 1.0
    },
    "P1223": {
        "description": "Parada de emergencia redundante",
        "severity": 1.0
    },
    "P1224": {
        "description": "Sensor posicion Mariposa B - valor fuera de rango",
        "severity": 1.0
    },
    "P1225": {
        "description": "Sensor elevacion aguja inyector",
        "severity": 1.0
    },
    "P1226": {
        "description": "Sensor posicion regulador cantidad combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P1227": {
        "description": "Solenoide reglaje inyeccion - valor Sobrepresion",
        "severity": 1.0
    },
    "P1228": {
        "description": "Solenoide reglaje inyeccion - valor bajopresion",
        "severity": 1.0
    },
    "P1229": {
        "description": "Controlador bomba intercooler - fallo",
        "severity": 2.0
    },
    "P1230": {
        "description": "Bomba combustible velocidad baja - circuito defectuoso",
        "severity": 1.0
    },
    "P1231": {
        "description": "Bomba combustible secundaria, alta velocidad - se\u00f1al baja",
        "severity": 1.0
    },
    "P1232": {
        "description": "Velocidad bomba combustible principal - circuito defectuoso",
        "severity": 1.0
    },
    "P1233": {
        "description": "Modulo Controlador Bomba combustible desconexion linea",
        "severity": 1.0
    },
    "P1234": {
        "description": "Modulo Controlador Bomba combustible desconexion linea",
        "severity": 1.0
    },
    "P1235": {
        "description": "Control bomba combustible - fuera de rango",
        "severity": 1.0
    },
    "P1236": {
        "description": "Control bomba combustible - fuera de rango",
        "severity": 1.0
    },
    "P1237": {
        "description": "Bomba combustible secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P1238": {
        "description": "Bomba combustible secundaria - circuito defectuoso",
        "severity": 1.0
    },
    "P1239": {
        "description": "Fallo velocidad bomba combustible",
        "severity": 1.0
    },
    "P1240": {
        "description": "Sensor alimentacion corriente",
        "severity": 1.0
    },
    "P1241": {
        "description": "Sensor alimentacion corriente - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P1242": {
        "description": "Sensor alimentacion corriente - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P1243": {
        "description": "Fallo segunda bomba combustible o masa",
        "severity": 1.0
    },
    "P1244": {
        "description": "Alternador - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P1245": {
        "description": "Alternador - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P1246": {
        "description": "Alternador",
        "severity": 2.0
    },
    "P1247": {
        "description": "Presion turbo baja",
        "severity": 3.0
    },
    "P1248": {
        "description": "Presion turbo no detectada",
        "severity": 3.0
    },
    "P1249": {
        "description": "Valvula reglaje inyeccion - funcionamiento",
        "severity": 2.0
    },
    "P1250": {
        "description": "Solenoide Bomba - circuito defectuoso",
        "severity": 1.0
    },
    "P1251": {
        "description": "Solenoide mezcla aire - circuito defectuoso",
        "severity": 1.0
    },
    "P1252": {
        "description": "Correlacion pedal PDS1 y LPDS alta",
        "severity": 1.0
    },
    "P1253": {
        "description": "Correlacion pedal PDS1 y LPDS baja",
        "severity": 1.0
    },
    "P1254": {
        "description": "Correlacion pedal PDS2 y LPDS alta",
        "severity": 1.0
    },
    "P1255": {
        "description": "Correlacion pedal PDS2 y LPDS baja",
        "severity": 1.0
    },
    "P1256": {
        "description": "Correlacion pedal PDS1 y HPDS",
        "severity": 1.0
    },
    "P1257": {
        "description": "Correlacion pedal PDS2 y HPDS",
        "severity": 1.0
    },
    "P1258": {
        "description": "Correlacion pedal PDS1 y PDS2",
        "severity": 1.0
    },
    "P1259": {
        "description": "Error se\u00f1al inmovilizador a modulo motor",
        "severity": 2.0
    },
    "P1260": {
        "description": "Robo detectado - vehiculo inmovilizado",
        "severity": 3.0
    },
    "P1261": {
        "description": "Cilindro 1 alto a bajo - corto",
        "severity": 2.0
    },
    "P1262": {
        "description": "Cilindro 2 alto a bajo - corto",
        "severity": 2.0
    },
    "P1263": {
        "description": "Cilindro 3 alto a bajo - corto",
        "severity": 2.0
    },
    "P1264": {
        "description": "Cilindro 4 alto a bajo - corto",
        "severity": 2.0
    },
    "P1265": {
        "description": "Cilindro 5 alto a bajo - corto",
        "severity": 2.0
    },
    "P1266": {
        "description": "Cilindro 6 alto a bajo - corto",
        "severity": 2.0
    },
    "P1267": {
        "description": "Cilindro 7 alto a bajo - corto",
        "severity": 2.0
    },
    "P1268": {
        "description": "Cilindro 8 alto a bajo - corto",
        "severity": 2.0
    },
    "P1269": {
        "description": "Codigo inmovilizador no programado",
        "severity": 2.0
    },
    "P1270": {
        "description": "Regimen motor/velocidad vehiculo maximos alcanzados",
        "severity": 3.0
    },
    "P1271": {
        "description": "Cilindro 1 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1272": {
        "description": "Cilindro 2 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1273": {
        "description": "Cilindro 3 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1274": {
        "description": "Cilindro 4 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1275": {
        "description": "Cilindro 5 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1276": {
        "description": "Cilindro 6 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1277": {
        "description": "Cilindro 7 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1278": {
        "description": "Cilindro 8 alto a bajo - abierto",
        "severity": 2.0
    },
    "P1280": {
        "description": "Presion control inyeccion - demasiado bajo",
        "severity": 2.0
    },
    "P1281": {
        "description": "Presion control inyeccion - demasiado alto",
        "severity": 2.0
    },
    "P1282": {
        "description": "Presion control inyeccion - excesiva",
        "severity": 3.0
    },
    "P1283": {
        "description": "Regulador presion inyector - circuito defectuoso",
        "severity": 2.0
    },
    "P1284": {
        "description": "Fallo en presion control inyector",
        "severity": 2.0
    },
    "P1285": {
        "description": "Sensor temperatura culata - sobrecalentamiento",
        "severity": 3.0
    },
    "P1286": {
        "description": "Impulso combustible - menor de lo previsto",
        "severity": 2.0
    },
    "P1287": {
        "description": "Impulso combustible - mayor de lo previsto",
        "severity": 2.0
    },
    "P1288": {
        "description": "Sensor temperatura culata - fuera de limites",
        "severity": 3.0
    },
    "P1289": {
        "description": "Sensor temperatura culata - se\u00f1al entrada alta",
        "severity": 3.0
    },
    "P1290": {
        "description": "Sensor temperatura culata - se\u00f1al entrada baja",
        "severity": 3.0
    },
    "P1291": {
        "description": "Inyector bloque 1 corto a masa o positivo",
        "severity": 2.0
    },
    "P1292": {
        "description": "Inyector bloque 2 corto a masa o positivo",
        "severity": 2.0
    },
    "P1293": {
        "description": "Inyector bloque 1 abierto",
        "severity": 2.0
    },
    "P1294": {
        "description": "Inyector bloque 2 abierto",
        "severity": 2.0
    },
    "P1295": {
        "description": "Multiples fallos bloque 1",
        "severity": 2.0
    },
    "P1296": {
        "description": "Multiples fallos bloque 2",
        "severity": 2.0
    },
    "P1297": {
        "description": "Inyector cortado",
        "severity": 2.0
    },
    "P1298": {
        "description": "Fallo IDM",
        "severity": 2.0
    },
    "P1299": {
        "description": "Sensor temperatura culada - sistema proteccion activo",
        "severity": 3.0
    },
    "P1300": {
        "description": "Fallo calibracion compresor",
        "severity": 2.0
    },
    "P1301": {
        "description": "Calibracion compresor alta",
        "severity": 2.0
    },
    "P1302": {
        "description": "Calibracion compresor baja",
        "severity": 2.0
    },
    "P1303": {
        "description": "Fallo calibracion EGR",
        "severity": 2.0
    },
    "P1304": {
        "description": "Calibracion EGR alta",
        "severity": 2.0
    },
    "P1305": {
        "description": "Calibracion EGR baja",
        "severity": 2.0
    },
    "P1306": {
        "description": "Rele Kickdown forzado - fallo circuito",
        "severity": 1.0
    },
    "P1307": {
        "description": "Rele Kickdown mantenido - fallo circuito",
        "severity": 1.0
    },
    "P1308": {
        "description": "Aire acondicionado - fallo circuito",
        "severity": 1.0
    },
    "P1309": {
        "description": "Fallo chip supervisor detonacion",
        "severity": 2.0
    },
    "P1313": {
        "description": "Fallo catalizador/detonacion bloque 1",
        "severity": 1.0
    },
    "P1314": {
        "description": "Fallo catalizador/detonacion bloque 2",
        "severity": 1.0
    },
    "P1315": {
        "description": "Detonacion persistente",
        "severity": 2.0
    },
    "P1316": {
        "description": "Circuito inyector/Codigos IDM",
        "severity": 1.0
    },
    "P1317": {
        "description": "Circuito inyector/Codigos IDM",
        "severity": 1.0
    },
    "P1319": {
        "description": "Sensor posicion piston reglaje inyeccion - circuito defectuoso",
        "severity": 2.0
    },
    "P1336": {
        "description": "Sensor arbol levas - rango,funcionamiento",
        "severity": 2.0
    },
    "P1340": {
        "description": "Sensor posicion arbol levas B - circuito defectuoso",
        "severity": 2.0
    },
    "P1341": {
        "description": "Sensor posicion arbol levas B - rango,funcionamiento",
        "severity": 2.0
    },
    "P1342": {
        "description": "Sensor posicion pedal acelerador A - rango,funcionamiento",
        "severity": 1.0
    },
    "P1343": {
        "description": "Sensor posicion pedal acelerador B - rango,funcionamiento",
        "severity": 1.0
    },
    "P1344": {
        "description": "Sensor posicion pedal acelerador C - rango,funcionamiento",
        "severity": 1.0
    },
    "P1345": {
        "description": "Sensor posicion arbol levas - circuito defectuoso",
        "severity": 2.0
    },
    "P1346": {
        "description": "Sensor nivel combustible B - circuito defectuoso",
        "severity": 1.0
    },
    "P1347": {
        "description": "Sensor nivel combustible B - rango,funcionamiento",
        "severity": 1.0
    },
    "P1348": {
        "description": "Sensor nivel combustible B - se\u00f1al baja",
        "severity": 1.0
    },
    "P1349": {
        "description": "Sensor nivel combustible B - se\u00f1al alta",
        "severity": 1.0
    },
    "P1350": {
        "description": "Sensor nivel combustible B - interrupcion intermitente",
        "severity": 1.0
    },
    "P1351": {
        "description": "Monitor diagnosis encendido - circuito entrada defectuoso",
        "severity": 1.0
    },
    "P1352": {
        "description": "Primario encendido A - circuito defectuoso",
        "severity": 1.0
    },
    "P1353": {
        "description": "Primario encendido B - circuito defectuoso",
        "severity": 1.0
    },
    "P1354": {
        "description": "Primario encendido C - circuito defectuoso",
        "severity": 1.0
    },
    "P1355": {
        "description": "Primario encendido D - circuito defectuoso",
        "severity": 1.0
    },
    "P1358": {
        "description": "Monitor diagnosis encendido - se\u00f1al fuera de limites",
        "severity": 2.0
    },
    "P1359": {
        "description": "Sistema encendido, se\u00f1al salida chispa - circuito defectuoso",
        "severity": 1.0
    },
    "P1360": {
        "description": "Secundario encendido A - circuito defectuoso",
        "severity": 1.0
    },
    "P1361": {
        "description": "Control encendido - tension baja",
        "severity": 1.0
    },
    "P1362": {
        "description": "Secundario encendido C - circuito defectuoso",
        "severity": 1.0
    },
    "P1363": {
        "description": "Secundario encendido D - circuito defectuoso",
        "severity": 1.0
    },
    "P1364": {
        "description": "Primario encendido - circuito defectuoso",
        "severity": 1.0
    },
    "P1365": {
        "description": "Secundario encendido - circuito defectuoso",
        "severity": 1.0
    },
    "P1366": {
        "description": "Componentes encendido",
        "severity": 1.0
    },
    "P1367": {
        "description": "Componentes encendido",
        "severity": 1.0
    },
    "P1368": {
        "description": "Componentes encendido",
        "severity": 1.0
    },
    "P1369": {
        "description": "Testigo temperatura motor - circuito defectuoso",
        "severity": 1.0
    },
    "P1370": {
        "description": "Insuficiente incremento RPM",
        "severity": 2.0
    },
    "P1371": {
        "description": "Encendido cilindro 1 - fallo activacion",
        "severity": 1.0
    },
    "P1372": {
        "description": "Encendido cilindro 2 - fallo activacion",
        "severity": 1.0
    },
    "P1373": {
        "description": "Encendido cilindro 3 - fallo activacion",
        "severity": 1.0
    },
    "P1374": {
        "description": "Encendido cilindro 4 - fallo activacion",
        "severity": 1.0
    },
    "P1375": {
        "description": "Encendido cilindro 5 - fallo activacion",
        "severity": 1.0
    },
    "P1376": {
        "description": "Encendido cilindro 6 - fallo activacion",
        "severity": 1.0
    },
    "P1380": {
        "description": "Detectada detonacion",
        "severity": 1.0
    },
    "P1381": {
        "description": "Actuador posicion arbol levas (bloque 1) - encendido sobreavanzado",
        "severity": 2.0
    },
    "P1382": {
        "description": "Solenoide posicion arbol levas (bloque 1) - circuito defectuoso",
        "severity": 2.0
    },
    "P1383": {
        "description": "Actuador posicion arbol levas (bloque 1) - encendido sobreretrasado",
        "severity": 2.0
    },
    "P1384": {
        "description": "Solenoide A arbol levas - circuito defectuoso",
        "severity": 2.0
    },
    "P1385": {
        "description": "Solenoide B arbol levas - circuito defectuoso",
        "severity": 2.0
    },
    "P1386": {
        "description": "Actuador posicion arbol levas (bloque 2) - encendido sobreavanzado",
        "severity": 2.0
    },
    "P1387": {
        "description": "Solenoide posicion arbol levas (bloque 2) - circuito defectuoso",
        "severity": 2.0
    },
    "P1388": {
        "description": "Actuador posicion arbol levas (bloque 2) - encendido sobreretrasado",
        "severity": 2.0
    },
    "P1389": {
        "description": "Bujias - se\u00f1al baja",
        "severity": 2.0
    },
    "P1390": {
        "description": "Conector codificacion octano - circuito abierto",
        "severity": 1.0
    },
    "P1391": {
        "description": "Bujias encendido (bloque 1) - se\u00f1al baja",
        "severity": 1.0
    },
    "P1392": {
        "description": "Bujias encendido (bloque 1) - se\u00f1al alta",
        "severity": 1.0
    },
    "P1393": {
        "description": "Bujias encendido (bloque 2) - se\u00f1al baja",
        "severity": 1.0
    },
    "P1394": {
        "description": "Bujias encendido (bloque 2) - se\u00f1al alta",
        "severity": 1.0
    },
    "P1395": {
        "description": "Fallo supervisor bujias (bloque 1)",
        "severity": 1.0
    },
    "P1396": {
        "description": "Fallo supervisor bujias (bloque 2)",
        "severity": 1.0
    },
    "P1397": {
        "description": "Tension alimentacion fuera de rango",
        "severity": 1.0
    },
    "P1398": {
        "description": "Solenoide B arbol levas - se\u00f1al alta",
        "severity": 1.0
    },
    "P1399": {
        "description": "Bujias encendido - se\u00f1al alta",
        "severity": 1.0
    },
    "P1400": {
        "description": "Sensor presion recirculacion gases escape - se\u00f1al baja",
        "severity": 1.0
    },
    "P1401": {
        "description": "Sensor presion recirculacion gases escape - se\u00f1al alta",
        "severity": 1.0
    },
    "P1402": {
        "description": "Sistema recirculacion gases escape EGR - orificio medicion restringido",
        "severity": 3.0
    },
    "P1403": {
        "description": "Sensor EGR - Diferencia control",
        "severity": 2.0
    },
    "P1404": {
        "description": "Temperatura aire inyeccion/Recirculacion EGR",
        "severity": 3.0
    },
    "P1405": {
        "description": "Sensor EGR - tubo flexible delantero desconectado u obstruido",
        "severity": 3.0
    },
    "P1406": {
        "description": "Sensor EGR - tubo flexible trasero desconectado u obstruido",
        "severity": 3.0
    },
    "P1407": {
        "description": "Sensor EGR - No detecta flujo",
        "severity": 3.0
    },
    "P1408": {
        "description": "Recirculacion gases escape - flujo fuera limites",
        "severity": 2.0
    },
    "P1409": {
        "description": "Solenoide recirculacion gases escape - circuito defectuoso",
        "severity": 2.0
    },
    "P1411": {
        "description": "Inyeccion aire secundario - flujo bajo",
        "severity": 1.0
    },
    "P1413": {
        "description": "Supervisor inyeccion aire secundario - se\u00f1al entrada baja",
        "severity": 1.0
    },
    "P1414": {
        "description": "Supervisor inyeccion aire secundario - se\u00f1al entrada alta",
        "severity": 1.0
    },
    "P1415": {
        "description": "Bomba Aire - circuito defectuoso",
        "severity": 1.0
    },
    "P1416": {
        "description": "Entrada aire - circuito defectuoso",
        "severity": 1.0
    },
    "P1417": {
        "description": "Entrada aire - circuito defectuoso",
        "severity": 1.0
    },
    "P1418": {
        "description": "Division aire 1 - circuito defectuoso",
        "severity": 1.0
    },
    "P1419": {
        "description": "Division aire 2 - circuito defectuoso",
        "severity": 1.0
    },
    "P1420": {
        "description": "Sensor temperatura catalizador",
        "severity": 2.0
    },
    "P1421": {
        "description": "Catalizador - defectuoso",
        "severity": 2.0
    },
    "P1422": {
        "description": "Encendido gases escape - Sensor temperatura",
        "severity": 2.0
    },
    "P1423": {
        "description": "Encendido gases escape - prueba funcional",
        "severity": 2.0
    },
    "P1424": {
        "description": "Encendido gases escape - bujia primaria",
        "severity": 2.0
    },
    "P1425": {
        "description": "Encendido gases escape - bujia secundaria",
        "severity": 2.0
    },
    "P1426": {
        "description": "Encendido gases escape - se\u00f1al MAF fuera de rango",
        "severity": 2.0
    },
    "P1427": {
        "description": "Encendido gases escape - se\u00f1al MAF cortocircuito",
        "severity": 2.0
    },
    "P1428": {
        "description": "Encendido gases escape - se\u00f1al MAF circuito abierto",
        "severity": 2.0
    },
    "P1429": {
        "description": "Bomba aire electrica - primaria",
        "severity": 2.0
    },
    "P1430": {
        "description": "Bomba aire electrica - secundaria",
        "severity": 2.0
    },
    "P1432": {
        "description": "Calentador termostato motor - circuito defectuoso",
        "severity": 1.0
    },
    "P1433": {
        "description": "Temperatura refrigerante aire acondicionado - se\u00f1al baja",
        "severity": 1.0
    },
    "P1434": {
        "description": "Temperatura refrigerante aire acondicionado - se\u00f1al alta",
        "severity": 1.0
    },
    "P1435": {
        "description": "Temperatura refrigerante aire acondicionado - rango,funcionamiento",
        "severity": 1.0
    },
    "P1436": {
        "description": "Temperatura evaporador aire acondicionado - se\u00f1al baja",
        "severity": 1.0
    },
    "P1437": {
        "description": "Temperatura evaporador aire acondicionado - se\u00f1al alta",
        "severity": 1.0
    },
    "P1438": {
        "description": "Temperatura evaporador aire acondicionado - rango,funcionamiento",
        "severity": 1.0
    },
    "P1439": {
        "description": "Interruptor temperatura - circuito defectuoso",
        "severity": 1.0
    },
    "P1440": {
        "description": "Valvula purga abierto",
        "severity": 1.0
    },
    "P1441": {
        "description": "Sistema emision evaporaciones",
        "severity": 1.0
    },
    "P1442": {
        "description": "Control emision evaporaciones",
        "severity": 1.0
    },
    "P1443": {
        "description": "Valvula control emision evaporaciones",
        "severity": 1.0
    },
    "P1444": {
        "description": "Sensor purga flujo - se\u00f1al baja",
        "severity": 1.0
    },
    "P1445": {
        "description": "Sensor purga flujo - se\u00f1al alta",
        "severity": 1.0
    },
    "P1446": {
        "description": "Solenoide evaporaciones - circuito defectuoso",
        "severity": 1.0
    },
    "P1447": {
        "description": "Valvula ELC",
        "severity": 1.0
    },
    "P1448": {
        "description": "Fallo sistema ELC",
        "severity": 1.0
    },
    "P1449": {
        "description": "Solenoide chequeo evaporaciones - circuito defectuoso",
        "severity": 1.0
    },
    "P1450": {
        "description": "Deposito combustible",
        "severity": 2.0
    },
    "P1451": {
        "description": "Valvula control emisiones evaporacion",
        "severity": 1.0
    },
    "P1452": {
        "description": "Deposito combustible",
        "severity": 2.0
    },
    "P1453": {
        "description": "Valvula presion deposito combustible",
        "severity": 2.0
    },
    "P1454": {
        "description": "Prueba sistema evaporaciones - circuito defectuoso",
        "severity": 1.0
    },
    "P1455": {
        "description": "Control emision evaporaciones",
        "severity": 2.0
    },
    "P1456": {
        "description": "Sensor temperatura deposito combustible - circuito defectuoso",
        "severity": 2.0
    },
    "P1457": {
        "description": "Imposible forzar aspiracion en deposito combustible",
        "severity": 2.0
    },
    "P1460": {
        "description": "Se\u00f1al mariposa plena carga (corte A/C) - circuito defectuoso",
        "severity": 1.0
    },
    "P1461": {
        "description": "Sensor presion aire acondicionado - tension baja",
        "severity": 1.0
    },
    "P1462": {
        "description": "Sensor presion aire acondicionado - tension alta",
        "severity": 1.0
    },
    "P1463": {
        "description": "Sensor presion aire acondicionado - presion insuficiente",
        "severity": 1.0
    },
    "P1464": {
        "description": "Peticion aire condicionado fuera limites diagnosis",
        "severity": 1.0
    },
    "P1465": {
        "description": "Rele aire acondicionado - circuito defectuoso",
        "severity": 1.0
    },
    "P1466": {
        "description": "Sensor temperatura refrigerante aire acondicionado - circuito defectuoso",
        "severity": 1.0
    },
    "P1467": {
        "description": "Sensor temperatura compresor aire acondicionado - circuito defectuoso",
        "severity": 1.0
    },
    "P1468": {
        "description": "Circuito abierto/cortocircuito SSPOD",
        "severity": 1.0
    },
    "P1469": {
        "description": "Periodo reciclaje aire acondicionado bajo",
        "severity": 1.0
    },
    "P1470": {
        "description": "Embrague compresor aire acondicionado - recorrido demasiado corto",
        "severity": 1.0
    },
    "P1471": {
        "description": "Fallo electroventilador 1 (lado conductor)",
        "severity": 1.0
    },
    "P1472": {
        "description": "Fallo electroventilador 2 (lado pasajero)",
        "severity": 1.0
    },
    "P1473": {
        "description": "Ventilador secundario alto con ventiladores apagados",
        "severity": 1.0
    },
    "P1474": {
        "description": "Ventilador - circuito defectuoso",
        "severity": 1.0
    },
    "P1475": {
        "description": "Rele ventiladores bajo - circuito defectuoso",
        "severity": 1.0
    },
    "P1476": {
        "description": "Rele ventiladores alto - circuito defectuoso",
        "severity": 1.0
    },
    "P1477": {
        "description": "Rele ventilador adicional - circuito defectuoso",
        "severity": 1.0
    },
    "P1478": {
        "description": "Ventilador refrigerante motor - fallo controlador",
        "severity": 1.0
    },
    "P1479": {
        "description": "Ventilador refrigerante motor - circuito defectuoso",
        "severity": 1.0
    },
    "P1480": {
        "description": "Ventilador secundario bajo con ventiladores bajos encendidos",
        "severity": 1.0
    },
    "P1481": {
        "description": "Ventilador lento motor con veltilador rapido encendido",
        "severity": 1.0
    },
    "P1482": {
        "description": "Bomba combustible",
        "severity": 2.0
    },
    "P1483": {
        "description": "Alimentacion ventiladores - sobrecorriente",
        "severity": 1.0
    },
    "P1484": {
        "description": "Alimentacion abierta a masa",
        "severity": 1.0
    },
    "P1485": {
        "description": "Valcula EGR - circuito defectuoso",
        "severity": 2.0
    },
    "P1486": {
        "description": "Actuador EGR - circuito defectuoso",
        "severity": 2.0
    },
    "P1487": {
        "description": "Solenoide EGR - circuito defectuoso",
        "severity": 2.0
    },
    "P1490": {
        "description": "Solenoide aire secundario - circuito defectuoso",
        "severity": 1.0
    },
    "P1491": {
        "description": "Interruptor solenoide secundario - circuito defectuoso",
        "severity": 1.0
    },
    "P1492": {
        "description": "Solenoide APLSOL - circuito defectuoso",
        "severity": 1.0
    },
    "P1493": {
        "description": "Solenoide RCNT - circuito defectuoso",
        "severity": 1.0
    },
    "P1494": {
        "description": "Solenoide SPCUT - circuito defectuoso",
        "severity": 1.0
    },
    "P1495": {
        "description": "Solenoide TCSPL - circuito defectuoso",
        "severity": 1.0
    },
    "P1500": {
        "description": "Sensor velocidad vehiculo",
        "severity": 2.0
    },
    "P1501": {
        "description": "Sensor velocidad vehiculo - fuera de limites",
        "severity": 2.0
    },
    "P1502": {
        "description": "Sensor velocidad vehiculo - interrupcion intermitente",
        "severity": 2.0
    },
    "P1503": {
        "description": "Sensor velocidad auxiliar",
        "severity": 1.0
    },
    "P1504": {
        "description": "Valvula control aire ralenti - circuito defectuoso",
        "severity": 1.0
    },
    "P1505": {
        "description": "Adaptacion control aire ralenti",
        "severity": 1.0
    },
    "P1506": {
        "description": "Valvula control aire ralenti - error sobrevelocidad",
        "severity": 1.0
    },
    "P1507": {
        "description": "Valvula control aire ralenti - error infravelocidad",
        "severity": 1.0
    },
    "P1508": {
        "description": "Solenoide subida ralenti 1 - circuito abierto",
        "severity": 1.0
    },
    "P1509": {
        "description": "Control ralenti - cortocircuito",
        "severity": 1.0
    },
    "P1510": {
        "description": "Se\u00f1al ralenti - circuito defectuoso",
        "severity": 1.0
    },
    "P1511": {
        "description": "Interruptor ralenti, mariposa electronica - circuito defectuoso",
        "severity": 1.0
    },
    "P1512": {
        "description": "Control aire colector admision (bloque 1) - cerrado",
        "severity": 1.0
    },
    "P1513": {
        "description": "Control aire colector admision (bloque 2) - cerrado",
        "severity": 1.0
    },
    "P1514": {
        "description": "Fallo controlador",
        "severity": 1.0
    },
    "P1515": {
        "description": "Corriente electrica - circuito defectuoso",
        "severity": 1.0
    },
    "P1516": {
        "description": "Solenoide aire colector admision (bloque 1) - error se\u00f1al entrada",
        "severity": 1.0
    },
    "P1517": {
        "description": "Solenoide aire colector admision (bloque 2) - error se\u00f1al entrada",
        "severity": 1.0
    },
    "P1518": {
        "description": "Control aire colector admision - abierto",
        "severity": 1.0
    },
    "P1519": {
        "description": "Control aire colector admision - cerrado",
        "severity": 1.0
    },
    "P1520": {
        "description": "Solenoide control aire colector admision - circuito defectuoso",
        "severity": 1.0
    },
    "P1521": {
        "description": "Solenoide control aire colector admision (bloque 1) - circuito defectuoso",
        "severity": 1.0
    },
    "P1522": {
        "description": "Solenoide control aire colector admision (bloque 2) - circuito defectuoso",
        "severity": 1.0
    },
    "P1523": {
        "description": "Solenoide admision variable - circuito defectuoso",
        "severity": 1.0
    },
    "P1524": {
        "description": "Solenoide admision variable",
        "severity": 1.0
    },
    "P1525": {
        "description": "Valvula aire bypass",
        "severity": 2.0
    },
    "P1526": {
        "description": "Sistema aire bypass",
        "severity": 2.0
    },
    "P1527": {
        "description": "Solenoide acelerador - circuito defectuoso",
        "severity": 1.0
    },
    "P1528": {
        "description": "Solenoide valvula mariposa auxiliar - circuito defectuoso",
        "severity": 1.0
    },
    "P1529": {
        "description": "Solenoide SCAIR - circuito defectioso",
        "severity": 1.0
    },
    "P1530": {
        "description": "Circuito aire acondicionado",
        "severity": 1.0
    },
    "P1531": {
        "description": "Movimiento pedal acelerador",
        "severity": 1.0
    },
    "P1532": {
        "description": "Circuito IMCC (Bloque B) defectuoso",
        "severity": 1.0
    },
    "P1533": {
        "description": "Circuito AAI defectuoso",
        "severity": 1.0
    },
    "P1534": {
        "description": "Interruptor inercia activado",
        "severity": 1.0
    },
    "P1535": {
        "description": "Velocidad ventilador - rango,funcionamiento",
        "severity": 1.0
    },
    "P1536": {
        "description": "Interruptor freno aparcamiento - circuito defectuoso",
        "severity": 2.0
    },
    "P1537": {
        "description": "Control aire colector admision (bloque 1) - abierto",
        "severity": 1.0
    },
    "P1538": {
        "description": "Control aire colector admision (bloque 2) - abierto",
        "severity": 1.0
    },
    "P1539": {
        "description": "Alimentacion circuito aire acondicionado - sobrecorriente",
        "severity": 1.0
    },
    "P1540": {
        "description": "Valvula aire bypass - circuito defectuoso",
        "severity": 1.0
    },
    "P1549": {
        "description": "Circuito IMCC (Bloque B) defectuoso",
        "severity": 1.0
    },
    "P1550": {
        "description": "PSPS fuera de rango",
        "severity": 1.0
    },
    "P1563": {
        "description": "Bomba inyeccion - peticion parada motor",
        "severity": 1.0
    },
    "P1564": {
        "description": "Bomba inyeccion - peticion modo combustible reducido",
        "severity": 1.0
    },
    "P1565": {
        "description": "Interruptor control velocidad - fuera de rango,alto",
        "severity": 2.0
    },
    "P1566": {
        "description": "Interruptor control velocidad - fuera de rango,bajo",
        "severity": 2.0
    },
    "P1567": {
        "description": "Salida control velocidad - continuidad",
        "severity": 2.0
    },
    "P1568": {
        "description": "Control velocidad - imposible mantener velocidad",
        "severity": 2.0
    },
    "P1571": {
        "description": "Interruptor frenos - circuito defectuoso",
        "severity": 2.0
    },
    "P1572": {
        "description": "Interruptor pedal freno - circuito defectuoso",
        "severity": 2.0
    },
    "P1573": {
        "description": "Posicion mariposa no disponible",
        "severity": 1.0
    },
    "P1574": {
        "description": "Sensor posicion mariposa - contradiccion entre sensores",
        "severity": 1.0
    },
    "P1575": {
        "description": "Posicion del pedal - fuera de rango",
        "severity": 1.0
    },
    "P1576": {
        "description": "Posicion del pedal - no disponible",
        "severity": 1.0
    },
    "P1577": {
        "description": "Posicion del pedal - contradiccion entre sensores",
        "severity": 1.0
    },
    "P1578": {
        "description": "Alimentacion ETC menor que exigida",
        "severity": 1.0
    },
    "P1579": {
        "description": "Alimentacion ETC al limite",
        "severity": 1.0
    },
    "P1580": {
        "description": "Supervisor mariposa electronica",
        "severity": 1.0
    },
    "P1581": {
        "description": "Supervisor mariposa electronica - circuito defectuoso",
        "severity": 1.0
    },
    "P1582": {
        "description": "Supervisor mariposa electronica - datos",
        "severity": 1.0
    },
    "P1583": {
        "description": "Supervisor mariposa electronica - crucero desactivado",
        "severity": 1.0
    },
    "P1584": {
        "description": "Unidad Control mariposa Detecta IPE - circuito defectuoso",
        "severity": 1.0
    },
    "P1585": {
        "description": "Unidad control mariposa - circuito defectuoso",
        "severity": 1.0
    },
    "P1586": {
        "description": "Unidad control mariposa - posicion mariposa defectuosa",
        "severity": 1.0
    },
    "P1587": {
        "description": "Unidad control mariposa modulada - circuito defectuoso",
        "severity": 1.0
    },
    "P1588": {
        "description": "Unidad control mariposa detecta perdida de retorno",
        "severity": 1.0
    },
    "P1589": {
        "description": "Unidad control mariposa no puede controlar angulo mariposa deseado",
        "severity": 1.0
    },
    "P1600": {
        "description": "Perdida alimentacion corriente auxiliar",
        "severity": 1.0
    },
    "P1601": {
        "description": "Error comunicaciones serie",
        "severity": 1.0
    },
    "P1602": {
        "description": "Modulo control inmovilizador - error comunicacion",
        "severity": 1.0
    },
    "P1603": {
        "description": "Memoria EEPROM defectuosa",
        "severity": 1.0
    },
    "P1604": {
        "description": "Codigo no registrado",
        "severity": 1.0
    },
    "P1605": {
        "description": "Fallo memoria permanente",
        "severity": 2.0
    },
    "P1606": {
        "description": "Rele control O/P - circuito defectuoso",
        "severity": 1.0
    },
    "P1607": {
        "description": "Testigo Averias O/P - circuito defectuoso",
        "severity": 1.0
    },
    "P1608": {
        "description": "Se\u00f1al control defectuosa",
        "severity": 1.0
    },
    "P1609": {
        "description": "Testigo averias - fallo controlador",
        "severity": 2.0
    },
    "P1610": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1611": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1612": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1613": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1614": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1615": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1616": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1617": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1618": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1619": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1620": {
        "description": "Codigos interactivos SBDS",
        "severity": 1.0
    },
    "P1621": {
        "description": "Memoria UCE/Codigo inmovilizador no coinciden",
        "severity": 2.0
    },
    "P1622": {
        "description": "Identificacion inmovilizador no coincide",
        "severity": 2.0
    },
    "P1623": {
        "description": "Codigo inmovilizador/identificador - fallo escritura",
        "severity": 2.0
    },
    "P1624": {
        "description": "Sistema antideslizamiento",
        "severity": 2.0
    },
    "P1625": {
        "description": "Alimentacion positivo a ventilador - circuito defectuoso",
        "severity": 1.0
    },
    "P1626": {
        "description": "Se\u00f1al activado deslizamiento no recibida",
        "severity": 1.0
    },
    "P1627": {
        "description": "Modulo control motor - tension alimentacion fuera de rango",
        "severity": 2.0
    },
    "P1628": {
        "description": "Modulo control motor - tension alimentacion",
        "severity": 2.0
    },
    "P1629": {
        "description": "Modulo control motor - regulador interno tension",
        "severity": 2.0
    },
    "P1630": {
        "description": "Modulo control motor - tension referencia interna",
        "severity": 2.0
    },
    "P1631": {
        "description": "Rele control motor/Principal alimentacion",
        "severity": 2.0
    },
    "P1632": {
        "description": "Sensor averia alternador - circuito defectuoso",
        "severity": 1.0
    },
    "P1633": {
        "description": "Tension auxiliar - demasiado baja",
        "severity": 1.0
    },
    "P1634": {
        "description": "Transmision salida datos - circuito defectuoso",
        "severity": 1.0
    },
    "P1635": {
        "description": "Valores fuera de rango",
        "severity": 1.0
    },
    "P1636": {
        "description": "Error comunicacion chip se\u00f1al inductiva",
        "severity": 1.0
    },
    "P1637": {
        "description": "Comunicacion motor-abs defectuosa",
        "severity": 2.0
    },
    "P1638": {
        "description": "Comunicacion motor-cuadro instrumentos defectuoso",
        "severity": 1.0
    },
    "P1639": {
        "description": "Identificacion vehiculo erronea o no programada",
        "severity": 1.0
    },
    "P1640": {
        "description": "Extraccion averias disponible en otro modulo",
        "severity": 1.0
    },
    "P1641": {
        "description": "Bomba combustible principal - circuito defectuoso",
        "severity": 2.0
    },
    "P1642": {
        "description": "Monitor bomba combustible - se\u00f1al entrada alta",
        "severity": 2.0
    },
    "P1643": {
        "description": "Cableado circuito red modulos",
        "severity": 1.0
    },
    "P1644": {
        "description": "Control velocidad bomba combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P1645": {
        "description": "Interruptor resistencia bomba combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P1650": {
        "description": "Interruptor presion direccion asistida - fuera limites",
        "severity": 2.0
    },
    "P1651": {
        "description": "Interruptor presion direccion asistida - se\u00f1al entrada",
        "severity": 2.0
    },
    "P1652": {
        "description": "Control Aire inyeccion desactivado",
        "severity": 1.0
    },
    "P1653": {
        "description": "Tension Alimentacion salida defectuosa",
        "severity": 1.0
    },
    "P1654": {
        "description": "Recirculacion defectuosa",
        "severity": 1.0
    },
    "P1655": {
        "description": "Arranque desactivado - circuito defectuoso",
        "severity": 1.0
    },
    "P1658": {
        "description": "Bomba inyeccion - tension alimentacion fuera rango",
        "severity": 1.0
    },
    "P1659": {
        "description": "Bomba inyeccion - tension alimentacion",
        "severity": 1.0
    },
    "P1660": {
        "description": "Se\u00f1al circuito salida alta",
        "severity": 1.0
    },
    "P1661": {
        "description": "Se\u00f1al circuito salida baja",
        "severity": 1.0
    },
    "P1662": {
        "description": "Fallo circuito IDM_EN",
        "severity": 1.0
    },
    "P1663": {
        "description": "Se\u00f1al peticion combustible - circuito defectuoso",
        "severity": 1.0
    },
    "P1664": {
        "description": "Bomba inyeccion - funcionamiento incorrecto",
        "severity": 1.0
    },
    "P1665": {
        "description": "Bomba inyeccion - comunicacion",
        "severity": 1.0
    },
    "P1666": {
        "description": "Bomba inyeccion - sincronizacion sensor posicion cigue\u00f1al",
        "severity": 1.0
    },
    "P1667": {
        "description": "Circuito Control Inyeccion defectuoso",
        "severity": 1.0
    },
    "P1668": {
        "description": "Bomba inyeccion - perdida se\u00f1al comunicacion",
        "severity": 1.0
    },
    "P1669": {
        "description": "Bomba inyeccion - supervision",
        "severity": 1.0
    },
    "P1670": {
        "description": "Se\u00f1al electronica no detectada",
        "severity": 1.0
    },
    "P1680": {
        "description": "Medicion bomba aceite - defectuosa",
        "severity": 3.0
    },
    "P1681": {
        "description": "Medicion bomba aceite - defectuosa",
        "severity": 3.0
    },
    "P1682": {
        "description": "Medicion bomba aceite - defectuosa",
        "severity": 3.0
    },
    "P1683": {
        "description": "Sensor temperatura bomba aceite - circuito defectuoso",
        "severity": 3.0
    },
    "P1684": {
        "description": "Sensor posicion bomba aceite - circuito defectuoso",
        "severity": 3.0
    },
    "P1685": {
        "description": "Motor paso a paso bomba aceite - circuito defectuoso",
        "severity": 3.0
    },
    "P1686": {
        "description": "Motor paso a paso bomba aceite - circuito defectuoso",
        "severity": 3.0
    },
    "P1687": {
        "description": "Motor paso a paso bomba aceite - circuito defectuoso",
        "severity": 3.0
    },
    "P1688": {
        "description": "Motor paso a paso bomba aceite - circuito defectuoso",
        "severity": 3.0
    },
    "P1689": {
        "description": "Solenoide control presion aceite - circuito defectuoso",
        "severity": 3.0
    },
    "P1690": {
        "description": "Solenoide entrada - circuito defectuoso",
        "severity": 3.0
    },
    "P1691": {
        "description": "Solenoide control presion turbo - circuito defectuoso",
        "severity": 3.0
    },
    "P1692": {
        "description": "Solenoide control turbo - circuito defectuoso",
        "severity": 3.0
    },
    "P1693": {
        "description": "Control carga turbo - circuito defectuoso",
        "severity": 3.0
    },
    "P1694": {
        "description": "Carga turbo - circuito defectuoso",
        "severity": 3.0
    },
    "P1695": {
        "description": "Bus de datos CAN - datos bomba inyeccion",
        "severity": 1.0
    },
    "P1700": {
        "description": "Transmision - fallo en posicion punto muerto",
        "severity": 1.0
    },
    "P1701": {
        "description": "Error reversible",
        "severity": 1.0
    },
    "P1702": {
        "description": "Circuito Transmision - interrupcion intermitente",
        "severity": 1.0
    },
    "P1703": {
        "description": "Interruptor posicion pedal freno - fuera de limites",
        "severity": 1.0
    },
    "P1704": {
        "description": "Fallo transicion estados en Transmision Digital",
        "severity": 1.0
    },
    "P1705": {
        "description": "Interruptor marchas cortas/largas - sin funcion en P/N",
        "severity": 1.0
    },
    "P1706": {
        "description": "Velocidad vehiculo alta en aparcamiento",
        "severity": 2.0
    },
    "P1707": {
        "description": "Fallo indicador punto muerto",
        "severity": 1.0
    },
    "P1708": {
        "description": "Interruptor aire acondicionado - circuito defectuoso",
        "severity": 1.0
    },
    "P1709": {
        "description": "Interruptor posicion estacionamiento/punto muerto - fuera limites",
        "severity": 1.0
    },
    "P1711": {
        "description": "sensor TFT fuera de rango",
        "severity": 1.0
    },
    "P1712": {
        "description": "Sistema reduccion par transmision - error se\u00f1al",
        "severity": 1.0
    },
    "P1713": {
        "description": "Sensor TFT - Valor muy bajo",
        "severity": 1.0
    },
    "P1714": {
        "description": "Se\u00f1al inductiva SSA defectuosa",
        "severity": 1.0
    },
    "P1715": {
        "description": "Se\u00f1al inductiva SSB defectuosa",
        "severity": 1.0
    },
    "P1716": {
        "description": "Se\u00f1al Inductiva SSC defectuosa",
        "severity": 1.0
    },
    "P1717": {
        "description": "Se\u00f1al inductiva SSD defectuosa",
        "severity": 1.0
    },
    "P1718": {
        "description": "Sensor TFT - valor muy alto",
        "severity": 1.0
    },
    "P1720": {
        "description": "Medidor velocidad vehiculo - circuito defectuoso",
        "severity": 1.0
    },
    "P1721": {
        "description": "Marcha 1 incorrecta",
        "severity": 2.0
    },
    "P1722": {
        "description": "Marcha 2 incorrecta",
        "severity": 2.0
    },
    "P1723": {
        "description": "Marcha 3 incorrecta",
        "severity": 2.0
    },
    "P1724": {
        "description": "Marcha 4 incorrecta",
        "severity": 2.0
    },
    "P1725": {
        "description": "Insuficiente velocidad motor incrementada durante diagnosis",
        "severity": 1.0
    },
    "P1726": {
        "description": "Insuficiente velocidad motor decrementada durante diagnosis",
        "severity": 1.0
    },
    "P1727": {
        "description": "Se\u00f1al inductiva solenoide aire acondicionado - circuito defectuoso",
        "severity": 1.0
    },
    "P1728": {
        "description": "Error transmision",
        "severity": 1.0
    },
    "P1729": {
        "description": "Error Interruptor 4x4 bajo",
        "severity": 1.0
    },
    "P1730": {
        "description": "Control marchas 2,3,5 - circuito defectuoso",
        "severity": 2.0
    },
    "P1731": {
        "description": "Cambio marchas 1-2 - circuito defectuoso",
        "severity": 2.0
    },
    "P1732": {
        "description": "Cambio marchas 2-3 - circuito defectuoso",
        "severity": 2.0
    },
    "P1733": {
        "description": "Cambio marchas 3-4 - circuito defectuoso",
        "severity": 2.0
    },
    "P1734": {
        "description": "Control marchas - circuito defectuoso",
        "severity": 2.0
    },
    "P1735": {
        "description": "Interruptor marcha primera - circuito defectuoso",
        "severity": 2.0
    },
    "P1736": {
        "description": "Interruptor marcha segunda - circuito defectuoso",
        "severity": 2.0
    },
    "P1737": {
        "description": "Solenoide bloqueo sistema",
        "severity": 2.0
    },
    "P1738": {
        "description": "Error tiempo cambio",
        "severity": 1.0
    },
    "P1739": {
        "description": "Solenoide sistema",
        "severity": 1.0
    },
    "P1740": {
        "description": "Se\u00f1al inductiva convertidor embrague rotativo - circuito defectuoso",
        "severity": 2.0
    },
    "P1741": {
        "description": "Error control convertidor embrague rotativo",
        "severity": 2.0
    },
    "P1742": {
        "description": "Fallo Solenoide converidor embrague rotativo",
        "severity": 2.0
    },
    "P1743": {
        "description": "Fallo Solenoide converidor embrague rotativo",
        "severity": 2.0
    },
    "P1744": {
        "description": "Convertidor embrague rotativo - funcionamiento",
        "severity": 2.0
    },
    "P1745": {
        "description": "Solenoide presion sistema",
        "severity": 3.0
    },
    "P1746": {
        "description": "Solenoide control presion A - circuito abierto",
        "severity": 2.0
    },
    "P1747": {
        "description": "Solenoide control presion A - cortocircuito",
        "severity": 2.0
    },
    "P1748": {
        "description": "EPC - circuito defectuoso",
        "severity": 2.0
    },
    "P1749": {
        "description": "Solenoide control presion - fallo bajo",
        "severity": 2.0
    },
    "P1751": {
        "description": "Solenoide cambio A - funcionamiento",
        "severity": 2.0
    },
    "P1754": {
        "description": "Solenoide embrague - circuito defectuoso",
        "severity": 2.0
    },
    "P1755": {
        "description": "Sensor velocidad intermedio - circuito defectuodo",
        "severity": 2.0
    },
    "P1756": {
        "description": "Solenoide cambio B - funcionamiento",
        "severity": 2.0
    },
    "P1760": {
        "description": "Solenoide control presion A - cortocircuito",
        "severity": 2.0
    },
    "P1761": {
        "description": "Solenoide cambio C - funcionamiento",
        "severity": 2.0
    },
    "P1762": {
        "description": "Fallo Margen superior",
        "severity": 2.0
    },
    "P1765": {
        "description": "Solenoide reglaje inyeccion - circuito defectuoso",
        "severity": 1.0
    },
    "P1767": {
        "description": "Convertidor embrague rotativo - circuito defectuoso",
        "severity": 2.0
    },
    "P1768": {
        "description": "modo funcionamiento normal/invierno - circuito defectuoso",
        "severity": 1.0
    },
    "P1769": {
        "description": "Fallo modulacion par transmision (AG4)",
        "severity": 1.0
    },
    "P1770": {
        "description": "Solenoide embrague - circuito defectuoso",
        "severity": 2.0
    },
    "P1775": {
        "description": "Fallo testigo averias transmision",
        "severity": 2.0
    },
    "P1776": {
        "description": "Fallo peticion retardo encendido",
        "severity": 1.0
    },
    "P1777": {
        "description": "Fallo peticion retardo encendido",
        "severity": 1.0
    },
    "P1778": {
        "description": "Transmision I/P - circuito defectuoso",
        "severity": 1.0
    },
    "P1779": {
        "description": "Circuito TCIL defectuoso",
        "severity": 1.0
    },
    "P1780": {
        "description": "Interruptor control transmision - fuera de rango",
        "severity": 1.0
    },
    "P1781": {
        "description": "Interruptor 4X4 - fuera de rango",
        "severity": 1.0
    },
    "P1782": {
        "description": "Circuito P/ES fuera de rango",
        "severity": 1.0
    },
    "P1783": {
        "description": "Condicion sobretemperatura en transmision",
        "severity": 2.0
    },
    "P1784": {
        "description": "Fallo mecanico en transmision - Primera y Atras",
        "severity": 2.0
    },
    "P1785": {
        "description": "Fallo mecanico en transmision - Primera y Segunda",
        "severity": 2.0
    },
    "P1786": {
        "description": "Error cambio marchas 3-2",
        "severity": 2.0
    },
    "P1787": {
        "description": "Error cambio marchas 2-1",
        "severity": 2.0
    },
    "P1788": {
        "description": "Solenoide control presion B - circuito abierto",
        "severity": 2.0
    },
    "P1789": {
        "description": "Solenoide control presion B - cortocircuito",
        "severity": 2.0
    },
    "P1790": {
        "description": "Transmision mecanica - circuito defectuoso",
        "severity": 2.0
    },
    "P1791": {
        "description": "Transmision electrica - circuito defectuoso",
        "severity": 2.0
    },
    "P1792": {
        "description": "Sensor presion barometrica - circuito defectuoso",
        "severity": 1.0
    },
    "P1793": {
        "description": "Volumen aire admision - circuito defectuoso",
        "severity": 1.0
    },
    "P1794": {
        "description": "Tension bateria",
        "severity": 1.0
    },
    "P1795": {
        "description": "Interruptor ralenti - circuito defectuoso",
        "severity": 1.0
    },
    "P1796": {
        "description": "Interruptor KickDown - circuito defectuoso",
        "severity": 1.0
    },
    "P1797": {
        "description": "Interruptor punto muerto - circuito defectuoso",
        "severity": 1.0
    },
    "P1798": {
        "description": "Temperatura refrigerante - circuito defectuoso",
        "severity": 2.0
    },
    "P1799": {
        "description": "Interruptor mantenido - circuito defectuoso",
        "severity": 1.0
    },
    "P1800": {
        "description": "Interruptor seguridad bloqueo embrague/transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1801": {
        "description": "Interruptor seguridad bloqueo embrague/transmision - circuito abierto",
        "severity": 1.0
    },
    "P1802": {
        "description": "Interruptor seguridad bloqueo embrague/transmision - corto a positivo",
        "severity": 1.0
    },
    "P1803": {
        "description": "Interruptor seguridad bloqueo embrague/transmision - corto a masa",
        "severity": 1.0
    },
    "P1804": {
        "description": "Indicador transmision alta 4 ruedas - circuito defectuoso",
        "severity": 1.0
    },
    "P1805": {
        "description": "Indicador transmision alta 4 ruedas - circuito abierto",
        "severity": 1.0
    },
    "P1806": {
        "description": "Indicador transmision alta 4 ruedas - corto a positivo",
        "severity": 1.0
    },
    "P1807": {
        "description": "Indicador transmision alta 4 ruedas - corto a masa",
        "severity": 1.0
    },
    "P1808": {
        "description": "Indicador transmision baja 4 ruedas - circuito defectuoso",
        "severity": 1.0
    },
    "P1809": {
        "description": "Indicador transmision baja 4 ruedas - circuito abierto",
        "severity": 1.0
    },
    "P1810": {
        "description": "Indicador transmision baja 4 ruedas - corto a positivo",
        "severity": 1.0
    },
    "P1811": {
        "description": "Indicador transmision baja 4 ruedas - corto a masa",
        "severity": 1.0
    },
    "P1812": {
        "description": "Modo seleccion transmision 4 ruedas - circuito defectuoso",
        "severity": 1.0
    },
    "P1813": {
        "description": "Modo seleccion transmision 4 ruedas - circuito abierto",
        "severity": 1.0
    },
    "P1814": {
        "description": "Modo seleccion transmision 4 ruedas - corto a positivo",
        "severity": 1.0
    },
    "P1815": {
        "description": "Modo seleccion transmision 4 ruedas - corto a masa",
        "severity": 1.0
    },
    "P1816": {
        "description": "Interruptor seguridad transmision punto muerto - circuito defectuoso",
        "severity": 1.0
    },
    "P1817": {
        "description": "Interruptor seguridad transmision punto muerto - circuito abierto",
        "severity": 1.0
    },
    "P1818": {
        "description": "Interruptor seguridad transmision punto muerto - corto a positivo",
        "severity": 1.0
    },
    "P1819": {
        "description": "Interruptor seguridad transmision punto muerto - corto a masa",
        "severity": 1.0
    },
    "P1820": {
        "description": "Rele cambio transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1821": {
        "description": "Rele cambio transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1822": {
        "description": "Rele cambio transferencia transmision - corto a positivo",
        "severity": 1.0
    },
    "P1823": {
        "description": "Rele cambio transferencia transmision - corto a masa",
        "severity": 1.0
    },
    "P1824": {
        "description": "Rele embrague transmision 4 ruedas - circuito defectuoso",
        "severity": 1.0
    },
    "P1825": {
        "description": "Rele embrague transmision 4 ruedas - circuito abierto",
        "severity": 1.0
    },
    "P1826": {
        "description": "Rele embrague transmision baja 4 ruedas - corto a positivo",
        "severity": 1.0
    },
    "P1827": {
        "description": "Rele embrague transmision baja 4 ruedas - corto a masa",
        "severity": 1.0
    },
    "P1828": {
        "description": "Rele cambio transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1829": {
        "description": "Rele cambio transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1830": {
        "description": "Rele cambio transferencia transmision - corto a positivo",
        "severity": 1.0
    },
    "P1831": {
        "description": "Rele cambio transferencia transmision - corto a masa",
        "severity": 1.0
    },
    "P1832": {
        "description": "Solenoide bloqueo diferencial transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1833": {
        "description": "Solenoide bloqueo diferencial transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1834": {
        "description": "Solenoide bloqueo diferencial transferencia transmision - corto a positivo",
        "severity": 1.0
    },
    "P1835": {
        "description": "Solenoide bloqueo diferencial transferencia transmision - corto a masa",
        "severity": 1.0
    },
    "P1836": {
        "description": "Sensor velocidad frontal transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1837": {
        "description": "Sensor velocidad lateral transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1838": {
        "description": "Motor cambio transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1839": {
        "description": "Motor cambio transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1840": {
        "description": "Motor cambio transferencia transmision - corto a positivo",
        "severity": 1.0
    },
    "P1841": {
        "description": "Motor cambio transferencia transmision - corto a masa",
        "severity": 1.0
    },
    "P1842": {
        "description": "Interruptor bloqueo diferencial transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1843": {
        "description": "Interruptor bloqueo diferencial transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1844": {
        "description": "Interruptor bloqueo diferencial transferencia transmision - corto a positivo",
        "severity": 1.0
    },
    "P1845": {
        "description": "Interruptor bloqueo diferencial transferencia transmision - corto a masa",
        "severity": 1.0
    },
    "P1846": {
        "description": "Contacto plata A transferencia transmision Transmission - circuito defectuoso",
        "severity": 1.0
    },
    "P1847": {
        "description": "Contacto plata A transferencia transmision Transmission - circuito abierto",
        "severity": 1.0
    },
    "P1848": {
        "description": "Contacto plata A transferencia transmision Transmission - corto a positivo",
        "severity": 1.0
    },
    "P1849": {
        "description": "Contacto plata A transferencia transmision Transmission - corto a masaa",
        "severity": 1.0
    },
    "P1850": {
        "description": "Contacto plata B transferencia transmision Transmission - circuito defectuoso",
        "severity": 1.0
    },
    "P1851": {
        "description": "Contacto plata B transferencia transmision Transmission - circuito abierto",
        "severity": 1.0
    },
    "P1852": {
        "description": "Contacto plata B transferencia transmision Transmission - corto a positivo",
        "severity": 1.0
    },
    "P1853": {
        "description": "Contacto plata B transferencia transmision Transmission - corto a masa",
        "severity": 1.0
    },
    "P1854": {
        "description": "Contacto plata C transferencia transmision Transmission - circuito defectuoso",
        "severity": 1.0
    },
    "P1855": {
        "description": "Contacto plata C transferencia transmision Transmission - circuito abierto",
        "severity": 1.0
    },
    "P1856": {
        "description": "Contacto plata C transferencia transmision Transmission - corto a positivo",
        "severity": 1.0
    },
    "P1857": {
        "description": "Contacto plata C transferencia transmision Transmission - corto a masa",
        "severity": 1.0
    },
    "P1858": {
        "description": "Contacto plata D transferencia transmision Transmission - circuito defectuoso",
        "severity": 1.0
    },
    "P1859": {
        "description": "Contacto plata D transferencia transmision Transmission - circuito abierto",
        "severity": 1.0
    },
    "P1860": {
        "description": "Contacto plata D transferencia transmision Transmission - corto a positivo",
        "severity": 1.0
    },
    "P1861": {
        "description": "Contacto plata D transferencia transmision Transmission - corto a masa",
        "severity": 1.0
    },
    "P1862": {
        "description": "Alimentacion contacto plata transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1863": {
        "description": "Alimentacion contacto plata transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1864": {
        "description": "Alimentacion contacto plata transferencia transmision - corto a positivo",
        "severity": 1.0
    },
    "P1865": {
        "description": "Alimentacion contacto plata transferencia transmision - corto a masa",
        "severity": 1.0
    },
    "P1866": {
        "description": "Transferencia transmision - servicio requerido",
        "severity": 1.0
    },
    "P1867": {
        "description": "Contacto plata transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1868": {
        "description": "Testigo transmision automatica 4 ruedas - circuito defectuoso",
        "severity": 1.0
    },
    "P1869": {
        "description": "Testigo transmision automatica 4 ruedas - corto a positivo",
        "severity": 1.0
    },
    "P1870": {
        "description": "Interruptor transferencia transmision mecanica 4x4 - circuito defectuoso",
        "severity": 1.0
    },
    "P1871": {
        "description": "Interruptor transferencia transmision mecanica 4x4 - corto a positivo",
        "severity": 1.0
    },
    "P1872": {
        "description": "Testigo bloqueo transmision mecanica 4 ruedas - circuito defectuoso",
        "severity": 1.0
    },
    "P1873": {
        "description": "Testigo bloqueo transmision mecanica 4 ruedas - corto a positivo",
        "severity": 1.0
    },
    "P1874": {
        "description": "Sensor alimentacion transmision automatica efecto Hall - circuito defectuoso",
        "severity": 1.0
    },
    "P1875": {
        "description": "Sensor alimentacion transmision automatica efecto Hall - corto a positivo",
        "severity": 1.0
    },
    "P1876": {
        "description": "Solenoide transferencia transmision 2 ruedas - circuito defectuoso",
        "severity": 1.0
    },
    "P1877": {
        "description": "Solenoide transferencia transmision 2 ruedas - corto a positivo",
        "severity": 1.0
    },
    "P1878": {
        "description": "Solenoide desencaje transferencia transmision - circuito defectuoso",
        "severity": 1.0
    },
    "P1879": {
        "description": "Solenoide desencaje transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1880": {
        "description": "Solenoide desencaje transferencia transmision - corto a positivo",
        "severity": 1.0
    },
    "P1881": {
        "description": "Interruptor nivel refrigerante motor - circuito defectuoso",
        "severity": 1.0
    },
    "P1882": {
        "description": "Interruptor nivel refrigerante motor - corto a masa",
        "severity": 1.0
    },
    "P1883": {
        "description": "Interruptor nivel refrigerante motor - circuito defectuoso",
        "severity": 1.0
    },
    "P1884": {
        "description": "Testigo Interruptor nivel refrigerante motor - corto a masa",
        "severity": 1.0
    },
    "P1885": {
        "description": "Solenoide desencaje transferencia transmision - corto a masa",
        "severity": 1.0
    },
    "P1886": {
        "description": "Fallo inicializacion 4x4",
        "severity": 1.0
    },
    "P1890": {
        "description": "Seleccion modo transmision 4WD - circuito defectuoso",
        "severity": 1.0
    },
    "P1891": {
        "description": "Contacto plata transferencia transmision - circuito abierto",
        "severity": 1.0
    },
    "P1900": {
        "description": "Circuito OSS - interrupcion intermitente",
        "severity": 1.0
    },
    "P1901": {
        "description": "Circuito TSS - Interrupcion intermitente",
        "severity": 1.0
    },
    "P1902": {
        "description": "Solenoide B Control presion - intermitente/corto",
        "severity": 1.0
    },
    "P1903": {
        "description": "Solenoide C Control presion - cortocircuito",
        "severity": 1.0
    },
    "P1904": {
        "description": "Solenoide C Control presion - circuito abierto",
        "severity": 1.0
    },
    "P1905": {
        "description": "Solenoide C Control presion - intermitente/corto",
        "severity": 1.0
    },
    "P1906": {
        "description": "Rele kickdown forzado circuito abierto/cortocircuito masa",
        "severity": 1.0
    },
    "P1907": {
        "description": "Rele kickdown mantenido circuito abierto/cortocircuito masa",
        "severity": 1.0
    },
    "P1908": {
        "description": "Solenoide presion aceite transmision - abierto/cortocircuito a masa",
        "severity": 2.0
    },
    "P1909": {
        "description": "Sensor temperatura aceite transmision - abierto/cortocircuito a masa",
        "severity": 2.0
    },
    "P1910": {
        "description": "Fallo salida presion VFS A baja",
        "severity": 1.0
    },
    "P1911": {
        "description": "Fallo salida presion VFS B baja",
        "severity": 1.0
    },
    "P1912": {
        "description": "Fallo salida presion VFS C baja",
        "severity": 1.0
    },
    "P1913": {
        "description": "Interruptor A presion - circuito defectuoso",
        "severity": 2.0
    },
    "P1914": {
        "description": "Interruptor cambio Automatico/Manual - circuito defectuoso",
        "severity": 1.0
    },
    "P1915": {
        "description": "Interruptor marcha atras - circuito defectuoso",
        "severity": 1.0
    },
    "P1916": {
        "description": "Sensor velocidad altura cilindro embrague - circuito defectuoso",
        "severity": 2.0
    },
    "P1917": {
        "description": "Sensor velocidad altura cilindro embrague - interrupcion intermitente",
        "severity": 2.0
    },
    "P1918": {
        "description": "Display rango transmision - circuito defectuoso",
        "severity": 1.0
    }
}
