import { useReducer, useEffect, useRef, useCallback } from 'react';
import {
    startOBD2LiveDTCs,
    startOBD2LiveDeleteDTCs,
    stopOBD2LiveData
} from './obd2-device';
import { DtcService } from '../services/dtc-info';

const initialState = {codes: {}, dtcs: [], hasErrors: false, updates: 0, completed: false};
const reducer = (currentState, newCode) => {
    if(newCode === 'RESET') return initialState;
    if(newCode === 'COMPLETED') return {...currentState, completed: true, updates: 0};
    const code = newCode.cmdID.split(' ').join('');
    console.log('Reducer ReadDTCs', currentState, newCode, code, currentState.updates);
    // if(code === '03' || code === '0101') {
    //     const  newState = {
    //         ...currentState,
    //         codes: {
    //             ...currentState.codes,
    //             [code]: newCode,
    //         },
    //         updates: currentState.completed ? 0 : currentState.updates + 1
    //     };
    //     newState.hasErrors = Object.keys(newState.codes).length > 0;
    //     return newState;
    // }
    if(code === '0101') {
        let status = {mil: false, errors: 0};
        let update = currentState.updates;
        try {
            if(newCode.cmdResult) {
                status = JSON.parse(newCode.cmdResult);
                update = currentState.completed ? 0 : update + 1;
            }
        }
        catch(error) {
            console.log('Diagnosis Error MIL', error);
        }
        return {
            ...currentState,
            status,
            updates: update,
            hasErrors: status.errors > 0
        };
    }
    else if(code === '03') {
        let dtcs = [];
        let update = currentState.updates;
        try {
            if(newCode.cmdResult) {
                dtcsCodes = JSON.parse(newCode.cmdResult);
                dtcs = dtcsCodes.map(
                    (dtc) => ({...DtcService(dtc), code: dtc})
                );
                update = currentState.completed ? 0 : update + 1;
            }
        }
        catch(error) {
            console.log('Diagnosis Reading DTCS', error);
        }
        return {
            ...currentState,
            dtcs,
            updates: update,
        };
    }
    return currentState;
};

export const useDTCData = (device, completed = () => {}, reload=true) => {
    const [data, setData] = useReducer(reducer, initialState);
    const deviceId = device && device.address;
    let subscription = useRef(null);
    const reset = useCallback(
        () => setData('RESET'),
        [],
    )
    useEffect(
        () => {
            //console.log('OBD2 subs ReadDTCs Connecting to...', device);
            if(deviceId && reload) {
                reset();
                subscription.current = startOBD2LiveDTCs(deviceId).subscribe(
                    (data) => {
                        //console.log('ReadDTCs Data', data);
                        setData(data);
                        // 03 y // 0101
                    },
                    (error) => console.log('ReadDTCs Error', error),
                    () => console.log('ReadDTCs Checking Completed')
                );
            }
            return () => {
                //console.log('OBD2 subs Stopping ReadDTCs device...', device, subscription);
                stopOBD2LiveData();
                subscription.current && subscription.current.unsubscribe();
            };
        },
        [deviceId, setData, reload, subscription]
    );
    useEffect(
        () => {
            if(data.updates > 3) {
                stopOBD2LiveData();
                subscription.current && subscription.current.unsubscribe();
                setData('COMPLETED');
                completed && completed();
            }
        },
        [data.updates]
    );
    return {data, reset};
};
