import { useReducer, useEffect, useRef } from 'react';
import {
    startOBD2LiveDiagnosis,
    stopOBD2LiveData
} from './obd2-device';
import {filter, skip} from 'rxjs/operators';

const proccessIntValue = (key, pid) => {
    if(pid && pid.cmdID === key && !pid.cmdResult.includes('NODATA')) {
        return parseInt(pid.cmdResult);
    }
    return null;
};

const reducer = (currentState, pid) => {
    if(currentState.finished) return currentState;

    const pidVelocity = currentState.pids['010D'];
    const pidRPM = currentState.pids['010C'];
    const velocity = proccessIntValue('01 0D', pid) || proccessIntValue('01 0D', pidVelocity);
    const rpm = proccessIntValue('01 0C', pid) || proccessIntValue('01 0C', pidRPM);

    //console.log('Reducer Diagnosis', currentState, pid, velocity, rpm);
    // Si no se tiene una velocidad de 0 o un RPM dentro del rango no se añaden
    // las lecturas de otros pids
    if(velocity > 0 || rpm < currentState.rpms[currentState.issue][0] || rpm > currentState.rpms[currentState.issue][1]) {
        let key = !pid ? null : pid.cmdID === '01 0C' ? '010C' : pid.cmdID === '01 0D' ? '010D' : null;
        if(!key) return currentState;
        return {
            ...currentState,
            pids: {
                ...currentState.pids,
                [key]: pid
            }
        };
    }
    // Si se cumplen las restricciones de RPM y velocidad añadimos el pid al diagnostico
    let newState = ({
        ...currentState,
        pids: {
            ...currentState.pids,
            [pid.cmdID.split(' ').join('')]: pid
        }
    });

    // Calculamos el porcentaje actual de diagnostico ejecutado
    newState.completed = Math.floor((Object.keys(newState.pids).length * 100) / 22);
    
    // Si el porcentaje es 100 guardamos esta issue y creamos una nueva
    if(newState.completed === 100) {
        newState.test.push({
            pids: newState.pids,
            rpms: newState.rpms[newState.issue]
        });
        newState.pids = {};
        newState.issue = newState.issue + 1;
        newState.completed = 0;
    }
    // si hemos alcanzado el numero de issues terminamos el proceso
    if(newState.issue >= newState.rpms.length) {
        newState.finished = true;
    }

    return newState;
};
const initialState = {
    rpms: [[2000, 3000], [3000, 4000], [4000, 5000]],
    completed: 0,
    finished: false,
    issue: 0,
    pids: {},
    test: []
};
export const useDiagnosisData = (device, reload=true) => {
    const [data, setData] = useReducer(reducer, initialState);
    const deviceId = device && device.address;
    const subscription = useRef(null);

    useEffect(
        () => {
            //console.log('OBD2 subs Diagnosis Connecting to...', device);
            if(deviceId && reload) {
                subscription.current = startOBD2LiveDiagnosis(deviceId).pipe(
                    filter(data => data.cmdResult !== null),
                    filter(data => (data.cmdID.startsWith('01') || data.cmdID === '03' || data.cmdID === '09 02')),
                    skip(2)
                ).subscribe(
                    (data) => {
                        //console.log('Diagnosis Data', data);
                        setData(data);
                    },
                    (error) => console.log('Diagnosis Error', error),
                    () => console.log('Diagnosis Checking Completed')
                );
            }
            return () => {
                //console.log('OBD2 subs Stopping Diagnosis device...', device, subscription);
                stopOBD2LiveData();
                subscription.current && subscription.current.unsubscribe();
            };
        },
        [deviceId, setData, reload]
    );
    useEffect(
        () => {
            if(data.finished) {
                stopOBD2LiveData();
                subscription.current && subscription.current.unsubscribe();
            }
        },
        [data.finished]
    );
    return data;
};
