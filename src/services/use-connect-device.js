import { useReducer, useEffect, useCallback } from 'react';
import {
    checkWithDataOBD2,
    stopOBD2LiveData
} from './obd2-device';
import { combineLatest } from 'rxjs';
import AsyncStorage from '@react-native-community/async-storage';
import { useUpdaterOBD2Device } from '../services/use-obd2-device-context';

const setDevice = (state, action) => {
    return {
        ...state,
        deviceConnected: action.payload,
        isDeviceConnecting: true
    }
};
const setDeviceError = (state, action) => {
    return {
        ...state,
        error: action.payload,
        isDeviceConnecting: false
    };
};
const setDeviceConnected = (state, action) => {
    //console.log('SetDeviceConnected', state, action);
    return {
        ...state,
        isDeviceConnecting: false,
        error: null,
        data: {
            ...state.data,
            // Payload payload: {cmdResult: "OK", cmdName: "Timeout", cmdID: "AT ST 3e"}
            [action.payload.cmdID.split(' ').join('')]: action.payload
        }
    };
};
const setData = (state, action) => {
    //console.log('setData', state, action);
    // payload payload: {cmdResult: "OK", cmdName: "Select Protocol AUTO", cmdID: "AT SP 0"}
    return {
        ...state,
        data: {
            ...state.data,
            [action.payload.cmdID.split(' ').join('')]: action.payload
        }
    };
};
ACTIONS = {
    ['SELECT_DEVICE']: setDevice,
    ['FAILED_DEVICE']: setDeviceError,
    ['DEVICE_CONNECTED']: setDeviceConnected,
    ['DEVICE_DATA']: setData
};
const reducer = (state, action) => {
    if(ACTIONS[action.type]) {
        return ACTIONS[action.type](state, action);
    }
    return state;
};
const initialState = {deviceConnected: null, isDeviceConnecting: false, data: {}, error: null};
export const useConnectDevice = (reload = true) => {
    const [{deviceConnected, isDeviceConnecting, data, error}, dispatch] = useReducer(reducer, initialState);
    const setDeviceConnected = useCallback(
        (device) => {
            dispatch({type: 'SELECT_DEVICE', payload: device});
        },
        []
    );
    const updateDevice = useUpdaterOBD2Device();
    useEffect(
        () => {
            //console.log('ASYNCSTORGE', 'Buscar dispositvo en storage');
            AsyncStorage.getItem('LAST_DEVICE').then(strDevice => {
                //console.log('ASYNCSTORGE', 'Last device used from ASYNC', strDevice);
                try {
                    const device = JSON.parse(strDevice);
                    setDeviceConnected(device);
                }
                catch(error) {
                    //console.log('Invalid device', error)
                }
            });
        },
        []
    );
    useEffect(
        () => {
            //console.log('ASYNCSTORGE', 'Guardar Dispositivo dispositvo en storage', deviceConnected);
            deviceConnected && !isDeviceConnecting && !error && AsyncStorage.setItem('LAST_DEVICE', JSON.stringify(deviceConnected));
            deviceConnected && !isDeviceConnecting && !error && //console.log('ASYNCSTORAGE', 'device saved', deviceConnected);
            updateDevice((error || isDeviceConnecting) ? null : deviceConnected);
        },
        [deviceConnected, isDeviceConnecting, error]
    );
    useEffect(
        () => {
            //console.log('OBD2 subs Connecting to...', deviceConnected);
            let subscription;
            if(deviceConnected && reload) {
                subscription = combineLatest(...checkWithDataOBD2(deviceConnected.address)).subscribe(
                    ([btStatus, obd2Status, data]) => {
                        //console.log('Data', btStatus, obd2Status, data, subscription);
                        if(btStatus.status === 'error' || obd2Status.status === 'disconected') {
                            stopOBD2LiveData();
                            subscription.unsubscribe();
                            dispatch({type: 'FAILED_DEVICE', payload: {code: 1, message: 'El dispositivo seleccionado no está disponible o no es un OBD2 válido'}});
                        }
                        else if(btStatus.status === 'connected' && obd2Status.status === 'receiving') {
                            dispatch({type: 'DEVICE_CONNECTED', payload: data});
                        }
                        else {
                            dispatch({type: 'DEVICE_DATA', payload: data});
                        }
                    },
                    (error) => console.log('Error', error),
                    () => console.log('Checking Completed')
                );
            }
            return () => {
                //console.log('OBD2 subs Stopping Checking device...', deviceConnected, subscription);
                stopOBD2LiveData();
                subscription && subscription.unsubscribe();
            };
        },
        [deviceConnected, reload]
    );
    return {deviceConnected, isDeviceConnecting, data, error, setDeviceConnected};
};
