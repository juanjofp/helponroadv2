import OBD2Client from 'react-native-obd2';
import { of, from, fromEvent } from 'rxjs';
import { catchError, tap, delay } from 'rxjs/operators';
import { DeviceEventEmitter } from 'react-native';

export const fakeDevices = [
    {name: 'OBD2-1', address: '88:18:56:68:98:EB'},
    {name: 'OBD2-2', address: '88:18:56:68:98:EC'},
    {name: 'OBD2-3', address: '88:18:56:68:98:ED'},
    {name: 'OBD2-4', address: '88:18:56:68:98:EE'},
    {name: 'OBD2-5', address: '82:18:56:68:98:ED'},
    {name: 'OBD2-6', address: '82:18:56:68:98:EE'}
];

export const getBluetoothDeviceNameList = (mocked = false) => {
    if (mocked) return of(fakeDevices);

    return from(OBD2Client.getBluetoothDeviceNameList()).pipe(
        delay(1000),
        tap(devices => console.log('Devices found', devices)),
        catchError(error => of({errorCode: 1001, message: error.message}))
    );
};

export const checkBTStatus = () => {
    OBD2Client.ready();
    return fromEvent(DeviceEventEmitter, 'obd2BluetoothStatus');
};

export const checkOBD2 = (address, mocked = false) => {
    //const now = Date.now();
    //console.log('OBD2Service startOBDLiveData init', address, OBD2Client);

    OBD2Client.ready();
    OBD2Client.setMockUpMode(mocked); // Mock OBD2
    setTimeout(() => OBD2Client.startLiveSelect(address), 100);
    //setTimeout(OBD2Client.stopLiveData, 500);
    //console.log('OBD2Service startOBDLiveData End', address, OBD2Client, Date.now());
    return [
        fromEvent(DeviceEventEmitter, 'obd2BluetoothStatus'),
        fromEvent(DeviceEventEmitter, 'obd2Status')
    ];
};

export const checkWithDataOBD2 = (address, mocked = false) => {
    //const now = Date.now();
    //console.log('OBD2Service startOBDLiveData init', address, OBD2Client);

    OBD2Client.ready();
    OBD2Client.setMockUpMode(mocked); // Mock OBD2
    setTimeout(() => OBD2Client.startLiveVINAndSensors(address), 100);
    //setTimeout(OBD2Client.stopLiveData, 500);
    //console.log('OBD2Service startOBDLiveData End', address, OBD2Client, Date.now());
    return [
        fromEvent(DeviceEventEmitter, 'obd2BluetoothStatus'),
        fromEvent(DeviceEventEmitter, 'obd2Status'),
        fromEvent(DeviceEventEmitter, 'obd2LiveData')
    ];
};

export const connectOBD2 = (address, mocked = false) => {
    //console.log('OBD2Service startOBDLiveData init', address, OBD2Client);
    OBD2Client.ready();
    OBD2Client.setMockUpMode(mocked); // Mock OBD2
    return [
        fromEvent(DeviceEventEmitter, 'obd2BluetoothStatus'),
        fromEvent(DeviceEventEmitter, 'obd2Status'),
    ];
};

export const disconnectOBD2 = () => {
    //console.log('OBD2Service stopOBDLiveData');
};

export const startOBD2LiveData = (address) => {
    //console.log('OBD2Service startOBD2LiveData');
    OBD2Client.startLiveData(address);
    return fromEvent(DeviceEventEmitter, 'obd2LiveData');
};

export const startOBD2LivePids = (address) => {
    //console.log('OBD2Service startOBD2LivePids');
    OBD2Client.startLivePids(address);
    return fromEvent(DeviceEventEmitter, 'obd2LiveData');
};

export const startOBD2LiveDTCs = (address) => {
    //console.log('OBD2Service startOBD2LiveDTCs');
    OBD2Client.startLiveDTC(address);
    return fromEvent(DeviceEventEmitter, 'obd2LiveData');
};

export const startOBD2LiveDeleteDTCs = (address) => {
    //console.log('OBD2Service startOBD2LiveDeleteDTCs');
    OBD2Client.deleteLiveDTC(address);
    return fromEvent(DeviceEventEmitter, 'obd2LiveData');
};

export const startOBD2LiveRPMAndVelocity = (address) => {
    //console.log('OBD2Service startOBD2LiveRPMAndVelocity');
    OBD2Client.startLiveRPMAndVelocity(address);
    return fromEvent(DeviceEventEmitter, 'obd2LiveData');
};

export const startOBD2LiveDiagnosis = (address) => {
    //console.log('OBD2Service startLiveDiagnosis', OBD2Client);
    OBD2Client.startLiveDiagnosis(address);
    return fromEvent(DeviceEventEmitter, 'obd2LiveData');
};

export const stopOBD2LiveData = () => {
    //console.log('OBD2Service stopOBD2LiveData>>>>>>>>>>>>');
    OBD2Client.stopLiveData();
};

export const isBluetoothEnabled = () => {
    //console.log('OBD2Service isBTEnabled');
    return OBD2Client.isBTEnabled();
};

export const enableBluetooth = () => {
    return OBD2Client.enableBluetooth();
};
