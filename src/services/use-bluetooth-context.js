import React, { createContext, useState, useEffect, useContext } from 'react';
import {
    isBluetoothEnabled,
    enableBluetooth
} from './obd2-device';

export const BTContext = createContext(false);
export const BTProvider = (props) => {
    const [isBTActived, setBTActived] = useState(false);
    useEffect(
        () => {
            const intervalId = setInterval(() => {
                isBluetoothEnabled().then(
                    (state) => {
                        //console.log('Updated Context IsBTEnabled!', state);
                        setBTActived(state);
                    }
                );
            }, 1500);
            return () => clearInterval(intervalId);
        }, []
    );

    return <BTContext.Provider value={isBTActived} {...props}/>
};

export const useBTStatus = () => {
    const isBluetoothEnabled = useContext(BTContext);
    return {isBluetoothEnabled, enableBluetooth};
};