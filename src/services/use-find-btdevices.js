import { useEffect, useReducer, useState, useCallback} from 'react';
import {
    getBluetoothDeviceNameList
} from './obd2-device';

const reducer = (currentState, newState) => ({...currentState, ...newState});
const initialState = {devices: [], isFinding: false};
export const useFindBTDevices = (isBTEnabled=false) => {
    const [{devices, isFinding}, dispatch] = useReducer(reducer, initialState);
    const [forceUpdate, setforceUpdate] = useState(Date.now());
    const findDevices = useCallback(
        () => {
            setforceUpdate(Date.now());
        },
        [setforceUpdate],
    )
    useEffect(
        () => {
            if(isBTEnabled) {
                dispatch({isFinding: true});
                getBluetoothDeviceNameList(false).subscribe(
                    (devices) => {
                        //console.log('Devices', devices);
                        dispatch({devices, isFinding: false});
                    },
                    (error) => {
                        //console.log('Error Finding Devices', error);
                    },
                    () => {}
                );
            }
            else {
                dispatch(initialState);
            }
        },
        [isBTEnabled, forceUpdate]
    );
    return {
        devices,
        isFinding,
        findDevices
    };
}

export default useFindBTDevices;