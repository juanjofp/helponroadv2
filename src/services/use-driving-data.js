import { useReducer, useEffect } from 'react';
import {
    startOBD2LiveRPMAndVelocity,
    stopOBD2LiveData
} from './obd2-device';

const reducer = (currentState, newState) => {
    //console.log('Reducer Driving', currentState, newState);
    return ({
        ...currentState,
        [newState.cmdID.split(' ').join('')]: newState
    });
};
const initialState = {};
export const useDrivingData = (device, reload=true) => {
    const [data, setData] = useReducer(reducer, initialState);
    const deviceId = device && device.address;

    useEffect(
        () => {
            //console.log('OBD2 subs Driving Connecting to...', device);
            let subscription;
            if(deviceId && reload) {
                subscription = startOBD2LiveRPMAndVelocity(deviceId).subscribe(
                    (data) => {
                        //console.log('Driving Data', data);
                        setData(data);
                    },
                    (error) => console.log('Driving Error', error),
                    () => console.log('Driving Checking Completed')
                );
            }
            return () => {
                //console.log('OBD2 subs Stopping Driving device...', device, subscription);
                stopOBD2LiveData();
                subscription && subscription.unsubscribe();
            };
        },
        [deviceId, setData, reload]
    );
    return data;
};
