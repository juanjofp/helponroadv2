import React, { createContext, useState, useContext } from 'react';

export const OBD2DeviceContext = createContext(null);
export const UpdateOBD2DeviceContext = createContext(null);

export const OBD2DeviceProvider = (props) => {
    const [obd2Device, setOBD2Device] = useState(null);
    //console.log('Current Context Device', obd2Device);
    return (
        <UpdateOBD2DeviceContext.Provider value={setOBD2Device}>
            <OBD2DeviceContext.Provider value={obd2Device} {...props}/>
        </UpdateOBD2DeviceContext.Provider>
    );
};

export const useOBD2Device = () => {
    return useContext(OBD2DeviceContext);
};

export const useUpdaterOBD2Device = () => {
    return useContext(UpdateOBD2DeviceContext);
};