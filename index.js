import {AppRegistry} from 'react-native';
import HelpOnRoad from './src';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => HelpOnRoad);
